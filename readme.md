# Setting up development environment

## Poetry

This project uses Poetry as its dependency management tool.
You can read more about Poetry on its [website](https://python-poetry.org/docs/).

TLDR; to install Poetry run

```
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -
```

## Installing dev dependencies

- Run `poetry install` to install the requirements.
- Now run `poetry shell` and you are ready to go.

## Setting up pre-commit hooks

Pre-commit has been setup to have standardization accross the codebase.
To install the hooks run `poetry run pre-commit install`.

To run the pre-commit hooks manually you can use `poetry run pre-commit run --all-files`.
