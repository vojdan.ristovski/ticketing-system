from datetime import datetime
from random import randint
from typing import List
from unittest.mock import MagicMock

import pytest
from fastapi.encoders import jsonable_encoder
from fastapi.testclient import TestClient
from pytest_postgresql import factories
from schemas.andon.component import ReadComponentSchema
from schemas.andon.enums import TeamEnum
from schemas.andon.events_config import (
    BasicProblem,
    Daily,
    Escalation,
    EventsConfig,
    MaterialsProblem,
    PlannedAction,
)
from schemas.andon.machines import ReadMachineSchema, ReadMachineTypeSchema
from schemas.andon.products import ReadLineSchema, ReadProductSchema
from schemas.andon.reasons import ReadReasonSchema
from schemas.andon.tickets import ReadTicketSchema
from schemas.andon.users import ReadRoleSchema, ReadUserSchema
from schemas.andon.zone import ReadZoneSchema
from sqlalchemy import insert

from src.config import get_config
from src.db.session import ScopedSession
from src.main import app
from src.models.tickets import (
    Component,
    Line,
    Machine,
    MachineType,
    Product,
    Reason,
    Ticket,
    TimeSeries,
    Zone,
    users_lines_association,
)
from src.models.users import Role, User, UsersTicketsAssociation
from src.worker.main import get_worker

config = get_config()

postgresql_in_docker = factories.postgresql_noproc()
postgresql = factories.postgresql("postgresql_in_docker", dbname="test")


# clients


@pytest.fixture
def user_client():
    user_client = TestClient(app)
    yield user_client


@pytest.fixture
def role_client():
    role_client = TestClient(app)
    yield role_client


@pytest.fixture
def zone_client():
    zone_client = TestClient(app)
    yield zone_client


@pytest.fixture
def line_client():
    line_client = TestClient(app)
    yield line_client


@pytest.fixture
def machine_type_client():
    machine_type_client = TestClient(app)
    yield machine_type_client


@pytest.fixture
def machine_client():
    machine_client = TestClient(app)
    yield machine_client


@pytest.fixture
def product_client():
    product_client = TestClient(app)
    yield product_client


@pytest.fixture
def component_client():
    component_client = TestClient(app)
    yield component_client


@pytest.fixture
def reason_client():
    reason_client = TestClient(app)
    yield reason_client


@pytest.fixture
def ticket_client(mock_celery_worker):
    ticket_client = TestClient(app)
    mock_celery_worker.send_task.delay()
    app.dependency_overrides[get_worker] = lambda: mock_celery_worker
    yield ticket_client


# celery mock


@pytest.fixture
def mock_celery_worker():
    celery_worker = MagicMock()
    # mock celery_worker.send_task function
    celery_worker.send_task.return_value = True

    yield celery_worker


# schemas


@pytest.fixture
def user_schemas_state(users: List[User], roles: List[Role]):
    # Populate db with test users
    with ScopedSession() as session:
        with session.begin():
            session.query(User).delete()
            session.query(Role).delete()
            session.flush()
            session.add_all(roles)
            session.add_all(users)
            session.flush()
            for user in users:
                session.refresh(user)
            user_schemas = [ReadUserSchema.from_orm(user) for user in users]

    yield user_schemas

    with ScopedSession() as session:
        with session.begin():
            session.query(User).delete()
            session.query(Role).delete()
            session.flush()


@pytest.fixture
def user_empty_schemas(users: List[User], roles: List[Role]):
    # Populate db with test users
    with ScopedSession() as session:
        with session.begin():
            session.query(User).delete()
            session.query(Role).delete()
            session.add_all(roles)
            session.add_all(users)
            session.flush()
            for user in users:
                session.refresh(user)
            user_schemas = [ReadUserSchema.from_orm(user) for user in users]
            session.query(User).delete()
            session.flush()

    yield user_schemas


@pytest.fixture
def role_schemas_state(roles: List[Role]):
    with ScopedSession() as session:
        with session.begin():
            session.query(Role).delete()
            session.add_all(roles)
            session.flush()
            for role in roles:
                session.refresh(role)
            role_schemas = [ReadRoleSchema.from_orm(role) for role in roles]

    yield role_schemas

    with ScopedSession() as session:
        with session.begin():
            session.query(Role).delete()
            session.flush()


@pytest.fixture
def role_empty_schemas(roles: List[Role]):
    with ScopedSession() as session:
        with session.begin():
            session.query(Role).delete()
            session.add_all(roles)
            session.flush()
            for role in roles:
                session.refresh(role)
            role_schemas = [ReadRoleSchema.from_orm(role) for role in roles]
            session.query(Role).delete()
            session.flush()

    yield role_schemas


@pytest.fixture
def zone_schemas_state(zones: List[Zone]):
    with ScopedSession() as session:
        with session.begin():
            session.query(Zone).delete()
            session.add_all(zones)
            session.flush()
            for zone in zones:
                session.refresh(zone)
            zone_schemas = [ReadZoneSchema.from_orm(zone) for zone in zones]

    yield zone_schemas

    with ScopedSession() as session:
        with session.begin():
            session.query(Zone).delete()
            session.flush()


@pytest.fixture
def zone_empty_schemas(zones: List[Zone]):
    with ScopedSession() as session:
        with session.begin():
            session.query(Zone).delete()
            session.add_all(zones)
            session.flush()
            for zone in zones:
                session.refresh(zone)
            zone_schemas = [ReadZoneSchema.from_orm(zone) for zone in zones]
            session.query(Zone).delete()
            session.flush()

    yield zone_schemas


@pytest.fixture
def line_schemas_state(lines: List[Line], zones: List[Zone], roles: List[Role]):
    with ScopedSession() as session:
        with session.begin():
            session.query(Zone).delete()
            session.query(Role).delete()
            session.query(TimeSeries).delete()
            session.query(Line).delete()
            session.add_all(zones)
            session.add_all(roles)
            session.add_all(lines)
            session.flush()
            line_schemas = [ReadLineSchema.from_orm(line) for line in lines]

    yield line_schemas

    with ScopedSession() as session:
        with session.begin():
            session.query(Zone).delete()
            session.query(Role).delete()
            session.query(TimeSeries).delete()
            session.query(Line).delete()
            session.flush()


@pytest.fixture
def line_empty_schemas(lines: List[Line], zones: List[Zone], roles: List[Role]):
    with ScopedSession() as session:
        with session.begin():
            session.query(Zone).delete()
            session.query(Role).delete()
            session.query(TimeSeries).delete()
            session.query(Line).delete()
            session.add_all(zones)
            session.add_all(roles)
            session.add_all(lines)
            session.flush()
            for line in lines:
                session.refresh(line)
            line_schemas = [ReadLineSchema.from_orm(line) for line in lines]
            session.query(Line).delete()
            session.flush()

    yield line_schemas


@pytest.fixture
def user_line_association_schema(
    lines: List[Line], zones: List[Zone], roles: List[Role], users: List[User]
):
    with ScopedSession() as session:
        with session.begin():
            session.query(Zone).delete()
            session.query(Role).delete()
            session.query(User).delete()
            session.query(TimeSeries).delete()
            session.query(Line).delete()
            session.add_all(zones)
            session.add_all(roles)
            session.add_all(users)
            session.add_all(lines)
            session.flush()
            for x in range(11):
                statement = insert(users_lines_association).values(
                    user_id=users[x].id_, line_id=lines[x].id_
                )
                session.execute(statement)
            session.flush()
            line_schemas = [ReadLineSchema.from_orm(line) for line in lines]

    yield line_schemas

    with ScopedSession() as session:
        with session.begin():
            session.query(users_lines_association).delete()
            session.query(Zone).delete()
            session.query(Role).delete()
            session.query(User).delete()
            session.query(TimeSeries).delete()
            session.query(Line).delete()


@pytest.fixture
def machine_type_schemas_state(machine_types: List[MachineType]):
    with ScopedSession() as session:
        with session.begin():
            session.query(MachineType).delete()
            session.add_all(machine_types)
            session.flush()
            for machine_type in machine_types:
                session.refresh(machine_type)
            machine_type_schemas = [
                ReadMachineTypeSchema.from_orm(machine_type)
                for machine_type in machine_types
            ]

    yield machine_type_schemas

    with ScopedSession() as session:
        with session.begin():
            session.query(MachineType).delete()
            session.flush()


@pytest.fixture
def machine_type_empty_schemas(machine_types: List[MachineType]):
    with ScopedSession() as session:
        with session.begin():
            session.query(MachineType).delete()
            session.add_all(machine_types)
            session.flush()
            for machine_type in machine_types:
                session.refresh(machine_type)
            machine_type_schemas = [
                ReadMachineTypeSchema.from_orm(machine_type)
                for machine_type in machine_types
            ]
            session.query(MachineType).delete()
            session.flush()

    yield machine_type_schemas


@pytest.fixture
def machine_schemas_state(
    machines: List[Machine],
    machine_types: List[MachineType],
    lines: List[Line],
    zones: List[Zone],
    time_series: List[TimeSeries],
):
    with ScopedSession() as session:
        with session.begin():
            session.query(TimeSeries).delete()
            session.query(Line).delete()
            session.query(Zone).delete()
            session.query(MachineType).delete()
            session.query(Machine).delete()
            session.add_all(zones)
            session.add_all(lines)
            session.add_all(time_series)
            session.add_all(machine_types)
            session.add_all(machines)
            session.flush()
            for machine in machines:
                session.refresh(machine)
            machine_schemas = [
                ReadMachineSchema.from_orm(machine) for machine in machines
            ]

    yield machine_schemas

    with ScopedSession() as session:
        with session.begin():
            session.query(TimeSeries).delete()
            session.query(Line).delete()
            session.query(Zone).delete()
            session.query(MachineType).delete()
            session.query(Machine).delete()
            session.flush()


@pytest.fixture
def machine_empty_schemas(
    machines: List[Machine],
    machine_types: List[MachineType],
    lines: List[Line],
    zones: List[Zone],
    time_series: List[TimeSeries],
):
    with ScopedSession() as session:
        with session.begin():
            session.query(TimeSeries).delete()
            session.query(Line).delete()
            session.query(Zone).delete()
            session.query(MachineType).delete()
            session.query(Machine).delete()
            session.add_all(zones)
            session.add_all(lines)
            session.add_all(time_series)
            session.add_all(machine_types)
            session.add_all(machines)
            session.flush()
            for machine in machines:
                session.refresh(machine)
            machine_schemas = [
                ReadMachineSchema.from_orm(machine) for machine in machines
            ]
            session.query(Machine).delete()
            session.flush()

    yield machine_schemas


@pytest.fixture
def product_schemas_state(
    products: List[Product],
    lines: List[Line],
    zones: List[Zone],
    time_series: List[TimeSeries],
):
    with ScopedSession() as session:
        with session.begin():
            session.query(TimeSeries).delete()
            session.query(Line).delete()
            session.query(Zone).delete()
            session.query(Product).delete()
            session.add_all(zones)
            session.add_all(lines)
            session.add_all(time_series)
            session.add_all(products)
            session.flush()
            for product in products:
                session.refresh(product)
            product_schemas = [
                ReadProductSchema.from_orm(product) for product in products
            ]

    yield product_schemas

    with ScopedSession() as session:
        with session.begin():
            session.query(TimeSeries).delete()
            session.query(Line).delete()
            session.query(Zone).delete()
            session.query(Product).delete()
            session.flush()


@pytest.fixture
def product_empty_schemas(
    products: List[Product],
    lines: List[Line],
    zones: List[Zone],
    time_series: List[TimeSeries],
):
    with ScopedSession() as session:
        with session.begin():
            session.query(TimeSeries).delete()
            session.query(Line).delete()
            session.query(Zone).delete()
            session.query(Product).delete()
            session.add_all(zones)
            session.add_all(lines)
            session.add_all(time_series)
            session.add_all(products)
            session.flush()
            for product in products:
                session.refresh(product)
            product_schemas = [
                ReadProductSchema.from_orm(product) for product in products
            ]
            session.query(Product).delete()
            session.flush()

    yield product_schemas


@pytest.fixture
def component_schemas_state(components: List[Component]):
    with ScopedSession() as session:
        with session.begin():
            session.query(Component).delete()
            session.add_all(components)
            session.flush()
            for component in components:
                session.refresh(component)
            component_schemas = [
                ReadComponentSchema.from_orm(component) for component in components
            ]

    yield component_schemas

    with ScopedSession() as session:
        with session.begin():
            session.query(Component).delete()
            session.flush()


@pytest.fixture
def component_empty_schemas(components: List[Component]):
    with ScopedSession() as session:
        with session.begin():
            session.query(Component).delete()
            session.add_all(components)
            session.flush()
            for component in components:
                session.refresh(component)
            component_schemas = [
                ReadComponentSchema.from_orm(component) for component in components
            ]
            session.query(Component).delete()
            session.flush()

    yield component_schemas


@pytest.fixture
def reason_schemas_state(reasons: List[Reason]):
    with ScopedSession() as session:
        with session.begin():
            session.query(Reason).delete()
            session.add_all(reasons)
            session.flush()
            for reason in reasons:
                session.refresh(reason)
            reason_schemas = [ReadReasonSchema.from_orm(reason) for reason in reasons]

    yield reason_schemas

    with ScopedSession() as session:
        with session.begin():
            session.query(Reason).delete()
            session.flush()


@pytest.fixture
def reason_empty_schemas(reasons: List[Reason]):
    with ScopedSession() as session:
        with session.begin():
            session.query(Reason).delete()
            session.add_all(reasons)
            session.flush()
            for reason in reasons:
                session.refresh(reason)
            reason_schemas = [ReadReasonSchema.from_orm(reason) for reason in reasons]
            session.query(Reason).delete()
            session.flush()

    yield reason_schemas


@pytest.fixture
def ticket_schemas_state(
    tickets: List[Ticket],
    reasons: List[Reason],
    components: List[Component],
    roles: List[Role],
    users: List[User],
    products: List[Product],
    lines: List[Line],
    time_series: List[TimeSeries],
    zones: List[Zone],
    machine_types: List[MachineType],
    machines: List[Machine],
):
    with ScopedSession() as session:
        with session.begin():
            session.query(Component).delete()
            session.query(Reason).delete()
            session.query(TimeSeries).delete()
            session.query(Line).delete()
            session.query(Zone).delete()
            session.query(Product).delete()
            session.query(MachineType).delete()
            session.query(Machine).delete()
            session.query(User).delete()
            session.query(Role).delete()
            session.query(Ticket).delete()
            session.query(UsersTicketsAssociation).delete()
            session.add_all(components)
            session.add_all(reasons)
            session.add_all(zones)
            session.add_all(lines)
            session.add_all(time_series)
            session.add_all(machine_types)
            session.add_all(machines)
            session.add_all(products)
            session.add_all(roles)
            session.add_all(users)
            session.add_all(tickets)
            session.add_all(
                [
                    UsersTicketsAssociation(
                        user_id=users[x].id_,
                        ticket_id=tickets[x].id_,
                    )
                    for x in range(11)
                ]
            )
            session.flush()
            for ticket in tickets:
                session.refresh(ticket)
            ticket_schemas = [ReadTicketSchema.from_orm(ticket) for ticket in tickets]

    yield ticket_schemas

    with ScopedSession() as session:
        with session.begin():
            session.query(Reason).delete()
            session.query(Component).delete()
            session.query(TimeSeries).delete()
            session.query(Line).delete()
            session.query(Zone).delete()
            session.query(MachineType).delete()
            session.query(Machine).delete()
            session.query(Product).delete()
            session.query(User).delete()
            session.query(Role).delete()
            session.query(Ticket).delete()
            session.query(UsersTicketsAssociation).delete()
            session.flush()


@pytest.fixture(scope="function")
def ticket_empty_schemas(
    tickets: List[Ticket],
    reasons: List[Reason],
    components: List[Component],
    roles: List[Role],
    users: List[User],
    products: List[Product],
    lines: List[Line],
    zones: List[Zone],
    machine_types: List[MachineType],
    machines: List[Machine],
):
    with ScopedSession() as session:
        with session.begin():
            session.query(Reason).delete()
            session.query(Component).delete()
            session.query(TimeSeries).delete()
            session.query(Line).delete()
            session.query(Zone).delete()
            session.query(Machine).delete()
            session.query(MachineType).delete()
            session.query(Product).delete()
            session.query(User).delete()
            session.query(Role).delete()
            session.query(Ticket).delete()
            session.query(UsersTicketsAssociation).delete()
            session.add_all(reasons)
            session.add_all(components)
            session.add_all(zones)
            session.add_all(lines)
            session.add_all(machine_types)
            session.add_all(machines)
            session.add_all(products)
            session.add_all(roles)
            session.add_all(users)
            session.add_all(tickets)
            session.add_all(
                [
                    UsersTicketsAssociation(
                        user_id=users[x].id_,
                        ticket_id=tickets[x].id_,
                    )
                    for x in range(11)
                ]
            )
            session.flush()
            for ticket in tickets:
                session.refresh(ticket)
            ticket_schemas = [ReadTicketSchema.from_orm(ticket) for ticket in tickets]
            session.query(Ticket).delete()
            session.flush()

    yield ticket_schemas


# atributes


@pytest.fixture
def roles():
    return [Role(id_=x, name=f"test_role{x}") for x in range(3)]


@pytest.fixture
def users(roles):
    return [
        User(
            id_=x,
            created_at=datetime.now(),
            updated_at=datetime.now(),
            deleted=False,
            first_name="Test",
            last_name="User",
            email=f"test+{x}@example.com",
            username=f"testuser{x}",
            password="password",
            is_admin=False,
            phone_number=f"3897{x:07}",
            card_id=f"{x}",
            # work_hours_from=datetime.time(8),
            # work_hours_to=datetime.time(16),
            work_days=[0, 1, 2, 3, 4],
            team=TeamEnum.A,
            roles=roles,
        )
        for x in range(11)
    ]


@pytest.fixture
def reasons():
    return [
        Reason(
            id_=x,
            created_at=datetime.now(),
            updated_at=datetime.now(),
            deleted=False,
            reason=f"test_reason{x}",
        )
        for x in range(0, 11)
    ]


@pytest.fixture
def components():
    return [
        Component(
            id_=x,
            created_at=datetime.now(),
            updated_at=datetime.now(),
            deleted=False,
            component_number=f"test_component{x}",
            description="test_desc",
        )
        for x in range(0, 11)
    ]


@pytest.fixture
def zones():
    return [
        Zone(
            id_=x,
            created_at=datetime.now(),
            updated_at=datetime.now(),
            deleted=False,
            name=f"test_zone{x}",
        )
        for x in range(11)
    ]


@pytest.fixture
def basic_problems():
    return BasicProblem(
        initial_escalation=Escalation(role_id=1, seconds_delay=0),
        escalation_1=Escalation(role_id=1, seconds_delay=0),
        escalation_2=Escalation(role_id=1, seconds_delay=0),
        escalation_3=Escalation(role_id=1, seconds_delay=0),
        escalation_4=Escalation(role_id=1, seconds_delay=0),
        escalation_5=Escalation(role_id=1, seconds_delay=0),
        forwarded_escalation=Escalation(role_id=1, seconds_delay=0),
        closed_escalation=Escalation(role_id=1, seconds_delay=0),
        daily=Daily(role_id=1, hour=0, minute=0),
    )


@pytest.fixture
def materials_problems():
    return MaterialsProblem(
        initial_escalation=Escalation(role_id=1, seconds_delay=0),
        escalation_1=Escalation(role_id=1, seconds_delay=0),
        escalation_2=Escalation(role_id=1, seconds_delay=0),
        escalation_3=Escalation(role_id=1, seconds_delay=0),
        escalation_4=Escalation(role_id=1, seconds_delay=0),
        escalation_5=Escalation(role_id=1, seconds_delay=0),
        confirmation_escalation=Escalation(role_id=1, seconds_delay=0),
        material_unavailable_escalation=Escalation(role_id=1, seconds_delay=0),
        daily=Daily(role_id=1, hour=0, minute=0),
    )


@pytest.fixture
def planned_actions():
    return PlannedAction(
        short_action_role_id=1, long_action_role_id=1, short_action_length_seconds=1800
    )


@pytest.fixture
def events_configs(basic_problems, materials_problems, planned_actions):
    return EventsConfig(
        MACHINE_PROBLEM=basic_problems,
        QUALITY_PROBLEM=basic_problems,
        MAINTENANCE_PROBLEM=basic_problems,
        MATERIALS_PROBLEM=materials_problems,
        PLANNED_ACTION=planned_actions,
    )


@pytest.fixture
def lines(zones, events_configs):
    return [
        Line(
            id_=x,
            created_at=datetime.now(),
            updated_at=datetime.now(),
            deleted=False,
            name="test_line",
            zone_id=zones[x].id_,
            password="password",
            event_config=jsonable_encoder(events_configs),
        )
        for x in range(11)
    ]


@pytest.fixture
def time_series(lines):
    return [
        TimeSeries(
            id_=x,
            created_at=datetime.now(),
            updated_at=datetime.now(),
            deleted=False,
            line_id=lines[x].id_,
            status=randint(0, 8),
            timestamp=datetime.now(),
        )
        for x in range(11)
    ]


@pytest.fixture
def machine_types():
    return [
        MachineType(
            id_=x,
            name=f"test_type{x}",
        )
        for x in range(11)
    ]


@pytest.fixture
def machines(machine_types, lines):
    return [
        Machine(
            id_=x,
            created_at=datetime.now(),
            updated_at=datetime.now(),
            deleted=False,
            name=f"test_machine{x}",
            description="test_desc",
            sku="test_sku",
            machine_type_id=machine_types[x].id_,
            order=x,
            line_id=lines[x].id_,
        )
        for x in range(11)
    ]


@pytest.fixture
def products(lines):
    return [
        Product(
            id_=x,
            created_at=datetime.now(),
            updated_at=datetime.now(),
            deleted=False,
            name=f"test_product{x}",
            lines=lines,
        )
        for x in range(11)
    ]


@pytest.fixture
def tickets(components, reasons, users, products, lines, machines):
    return [
        Ticket(
            id_=x,
            created_at=datetime.now(),
            updated_at=datetime.now(),
            deleted=False,
            status=x,
            corrective_action="test_action",
            issue_description="test_issue",
            component_id=components[x].component_number,
            reason_id=reasons[x].id_,
            created_by_id=users[x].id_,
            product_id=products[x].id_,
            line_id=lines[x].id_,
            datetime_from=datetime.now(),
            datetime_to=datetime.now(),
            approved_by_id=users[x].id_,
            priority=randint(0, 6),
            ticket_type=randint(0, 4),
            machines=[machines[x]],
        )
        for x in range(11)
    ]
