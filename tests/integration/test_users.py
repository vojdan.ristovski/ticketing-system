from typing import List

from schemas.andon.tickets import ReadTicketSchema
from schemas.andon.users import CreateUserSchema, ReadUserSchema, UpdateUserSchema
from starlette.testclient import TestClient

from src.models.users import Role


def test_list_users_empty_db(user_client: TestClient):
    # Get response from the service
    response = user_client.get("/api/v1/users/", params={"page": 0, "page_size": 10})
    assert "content" in response.json()
    assert "total" in response.json()
    assert response.json()["total"] == 0
    users = response.json()["content"]
    # Assert that the service returns the correct data
    assert response.status_code == 200
    assert len(users) == 0


def test_list_users(user_client: TestClient, user_schemas_state: List[ReadUserSchema]):
    # Get response from the service
    response = user_client.get("/api/v1/users/", params={"page": 0, "page_size": 11})
    assert "content" in response.json()
    assert "total" in response.json()
    users_response: List[dict] = response.json()["content"]
    # Assert that the service returns the correct data
    assert response.status_code == 200
    assert len(users_response) == len(user_schemas_state)
    users_response.sort(key=lambda x: x["id_"])
    user_schemas_state.sort(key=lambda x: x.id_)
    for user, test_user in zip(users_response, user_schemas_state):
        assert ReadUserSchema(**user).json(
            exclude={"created_at", "updated_at", "roles"}
        ) == test_user.json(exclude={"created_at", "updated_at", "roles"})


def test_list_users_5_per_page(
    user_client: TestClient, user_schemas_state: List[ReadUserSchema]
):
    # Get response from the service
    response = user_client.get("/api/v1/users/", params={"page": 0, "page_size": 5})
    assert "content" in response.json()
    assert "total" in response.json()
    users_response: List[dict] = response.json()["content"]
    # Assert that the service returns the correct data
    assert response.status_code == 200
    assert len(users_response) == 5
    users_response.sort(key=lambda x: x["id_"])
    user_schemas_state.sort(key=lambda x: x.id_)
    for user, test_user in zip(users_response, user_schemas_state):
        assert ReadUserSchema(**user).json(
            exclude={"created_at", "updated_at", "roles"}
        ) == test_user.json(exclude={"created_at", "updated_at", "roles"})


def test_create_user(user_client: TestClient, user_empty_schemas: List[ReadUserSchema]):
    assert user_empty_schemas[0].roles is not None
    user_schema = CreateUserSchema(
        **user_empty_schemas[0].dict(exclude={"roles"}),
        password="password",
        roles=[role.id_ for role in user_empty_schemas[0].roles],
    )
    response = user_client.post("/api/v1/users/", data=user_schema.json())
    assert response.status_code == 201
    user_response: List[dict] = response.json()
    assert ReadUserSchema(**user_response).json(
        exclude={"id_", "created_at", "updated_at", "roles"}
    ) == user_empty_schemas[0].json(exclude={"id_", "created_at", "updated_at", "roles"})


def test_create_user_invalid_schema(user_client: TestClient):
    response = user_client.post("/api/v1/users/", json={"first_name": "John"})
    assert response.status_code == 422


def test_create_user_already_exists(
    user_client: TestClient, user_schemas_state: List[ReadUserSchema]
):
    user_schema = CreateUserSchema(
        **user_schemas_state[0].dict(exclude={"roles"}), password="password"
    )
    response = user_client.post("/api/v1/users/", data=user_schema.json())
    assert response.status_code == 400


def test_create_user_role_not_found(
    user_client: TestClient, user_empty_schemas: List[ReadUserSchema]
):
    user_schema = CreateUserSchema(
        **user_empty_schemas[0].dict(exclude={"roles"}),
        password="password",
        roles=[666],
    )
    response = user_client.post("/api/v1/users/", data=user_schema.json())
    assert response.status_code == 404


def test_get_user(user_client: TestClient, user_schemas_state: List[ReadUserSchema]):
    response = user_client.get(f"/api/v1/users/{user_schemas_state[0].id_}")
    assert response.status_code == 200
    user_schema: List[dict] = response.json()
    assert ReadUserSchema(**user_schema).json() == user_schemas_state[0].json()


def test_get_user_not_found(user_client: TestClient):
    response = user_client.get("/api/v1/users/666")
    assert response.status_code == 404


def test_get_user_invalid_id(user_client: TestClient):
    response = user_client.get("/api/v1/users/invalid_id")
    assert response.status_code == 422


def test_delete_user(user_client: TestClient, user_schemas_state: List[ReadUserSchema]):
    response = user_client.delete(f"/api/v1/users/{user_schemas_state[0].id_}")
    assert response.status_code == 202
    response = user_client.get(f"/api/v1/users/{user_schemas_state[0].id_}")
    assert response.status_code == 404


def test_delete_user_id_not_found(user_client: TestClient):
    response = user_client.delete("/api/v1/users/666")
    assert response.status_code == 404


def test_delete_user_invalid_id(user_client: TestClient):
    response = user_client.delete("/api/v1/users/invalid_id")
    assert response.status_code == 422


def test_update_user(user_client: TestClient, user_schemas_state: List[ReadUserSchema]):
    assert user_schemas_state[0].roles is not None
    user_schema = UpdateUserSchema(
        **user_schemas_state[0].dict(exclude={"first_name", "roles"}),
        first_name="John",
        roles=[role.id_ for role in user_schemas_state[0].roles],
    )
    response = user_client.patch(
        f"/api/v1/users/{user_schemas_state[0].id_}", data=user_schema.json()
    )
    assert response.status_code == 200
    user_response: List[dict] = response.json()
    assert ReadUserSchema(**user_response).json(
        exclude={"id_", "created_at", "updated_at", "deleted_at", "deleted", "roles"}
    ) == user_schema.json(exclude={"roles"})


def test_update_user_invalid_id(user_client: TestClient):
    response = user_client.patch("/api/v1/users/invalid_id", json={"first_name": "John"})
    assert response.status_code == 422


def test_update_user_id_not_found(user_client: TestClient):
    response = user_client.patch("/api/v1/users/666", json={"first_name": "John"})
    assert response.status_code == 404


def test_update_user_role_not_found(
    user_client: TestClient, user_schemas_state: List[ReadUserSchema]
):
    user_schema = UpdateUserSchema(
        **user_schemas_state[0].dict(exclude={"first_name", "roles"}),
        first_name="John",
        roles=[666],
    )
    response = user_client.patch(
        f"/api/v1/users/{user_schemas_state[0].id_}", data=user_schema.json()
    )
    assert response.status_code == 404


def test_get_user_by_email(
    user_client: TestClient, user_schemas_state: List[ReadUserSchema]
):
    response = user_client.get(f"/api/v1/users/e/{user_schemas_state[0].email}")
    assert response.status_code == 200
    user_schema: List[dict] = response.json()
    assert ReadUserSchema(**user_schema).json() == user_schemas_state[0].json()


def test_user_get_by_email_not_found(user_client: TestClient):
    response = user_client.get("/api/v1/users/e/duci.brz@gmail.com")
    assert response.status_code == 404


def test_user_get_by_email_invalid_email(user_client: TestClient):
    response = user_client.get("/api/v1/users/e/invalid_email")
    assert response.status_code == 422


def test_get_user_by_username(
    user_client: TestClient, user_schemas_state: List[ReadUserSchema]
):
    response = user_client.get(f"/api/v1/users/u/{user_schemas_state[0].username}")
    assert response.status_code == 200
    user_schema: List[dict] = response.json()
    assert ReadUserSchema(**user_schema).json() == user_schemas_state[0].json()


def test_user_get_by_username_not_found(user_client: TestClient):
    response = user_client.get("/api/v1/users/u/1")
    assert response.status_code == 404


def test_get_user_by_role(
    user_client: TestClient, user_schemas_state: List[ReadUserSchema], roles: List[Role]
):
    for role in roles:
        response = user_client.get(f"/api/v1/users/r/{role.id_}")
        assert response.status_code == 200
        users_response: List[dict] = response.json()
        users_response.sort(key=lambda x: x["id_"])
        user_schemas_state.sort(key=lambda x: x.id_)
        for user, test_user in zip(users_response, user_schemas_state):
            assert ReadUserSchema(**user).json() == test_user.json()


def test_user_get_by_role_not_found(user_client: TestClient):
    response = user_client.get("/api/v1/users/r/5")
    assert response.status_code == 404


def test_user_get_by_role_invalid_role(user_client: TestClient):
    response = user_client.get("/api/v1/users/r/invalid_id")
    assert response.status_code == 422


def test_get_user_by_card_id(
    user_client: TestClient, user_schemas_state: List[ReadUserSchema]
):
    response = user_client.get(f"/api/v1/users/c/{user_schemas_state[0].card_id}")
    assert response.status_code == 200
    user_schema: List[dict] = response.json()
    assert ReadUserSchema(**user_schema).json() == user_schemas_state[0].json()


def test_get_user_by_card_id_not_found(user_client: TestClient):
    response = user_client.get("/api/v1/users/c/1")
    assert response.status_code == 404


def test_get_tickets_by_user_id(
    user_client: TestClient, ticket_schemas_state: List[ReadTicketSchema]
):
    user = ticket_schemas_state[0].created_by.id_
    response = user_client.get(
        f"/api/v1/users/{user}/tickets", params={"page": 0, "page_size": 10}
    )
    assert "content" in response.json()
    assert "total" in response.json()
    assert response.status_code == 200
    users_response: List[dict] = response.json()["content"]
    # Assert that the service returns the correct data
    assert len(users_response) == 1
    for user, test_user in zip(users_response, ticket_schemas_state):
        assert ReadTicketSchema(**user).json(
            exclude={
                "created_at",
                "updated_at",
                "machines",
                "zone",
                "line",
                "product",
                "created_by",
                "handled_by",
            }
        ) == ReadTicketSchema.from_orm(test_user).json(
            exclude={
                "created_at",
                "updated_at",
                "machines",
                "zone",
                "line",
                "product",
                "created_by",
                "handled_by",
            }
        )


def test_get_tickets_by_user_id_invalid_id(user_client: TestClient):
    response = user_client.get(
        "/api/v1/users/invalid_id/tickets", params={"page": 0, "page_size": 10}
    )
    assert response.status_code == 422


def test_get_tickets_by_user_id_not_found(user_client: TestClient):
    response = user_client.get(
        "/api/v1/users/666/tickets", params={"page": 0, "page_size": 10}
    )
    assert response.status_code == 404
