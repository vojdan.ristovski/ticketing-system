from typing import List

from schemas.andon.products import (
    CreateProductSchema,
    ReadProductSchema,
    UpdateProductSchema,
)
from starlette.testclient import TestClient


def test_list_products_empty_db(product_client: TestClient):
    response = product_client.get(
        "/api/v1/products/", params={"page": 0, "page_size": 10}
    )
    assert "content" in response.json()
    assert "total" in response.json()
    assert response.json()["total"] == 0
    products = response.json()["content"]
    assert response.status_code == 200
    assert len(products) == 0


def test_list_products(
    product_client: TestClient, product_schemas_state: List[ReadProductSchema]
):
    # Get response from the service
    response = product_client.get(
        "/api/v1/products/", params={"page": 0, "page_size": 11}
    )
    assert "content" in response.json()
    assert "total" in response.json()
    products_response: List[dict] = response.json()["content"]
    # Assert that the service returns the correct data
    assert response.status_code == 200
    assert len(products_response) == len(product_schemas_state)
    for product, test_product in zip(products_response, product_schemas_state):
        assert ReadProductSchema(**product).json(
            exclude={"created_at", "updated_at"}
        ) == ReadProductSchema.from_orm(test_product).json(
            exclude={"created_at", "updated_at"}
        )


def test_create_product(
    product_client: TestClient, product_empty_schemas: List[ReadProductSchema]
):
    product_schema = CreateProductSchema(
        **product_empty_schemas[0].dict(),
        line_ids=list(x.id_ for x in product_empty_schemas[0].lines),
    )
    response = product_client.post(
        "/api/v1/products/", data=product_schema.json(exclude={"id"})
    )
    product_response: List[dict] = response.json()
    assert response.status_code == 201
    assert ReadProductSchema(**product_response).json(
        exclude={"id_", "created_at", "updated_at", "lines"}
    ) == product_empty_schemas[0].json(
        exclude={"id_", "created_at", "updated_at", "lines"}
    )


def test_create_product_invalid_schema(product_client: TestClient):
    response = product_client.post("/api/v1/products/", json={"reason": "test"})
    assert response.status_code == 422


def test_create_product_line_not_found(
    product_client: TestClient, product_schemas_state: List[ReadProductSchema]
):
    product_schema = CreateProductSchema(
        **product_schemas_state[0].dict(), line_ids=list(x for x in range(10, 20))
    )
    response = product_client.post("/api/v1/products/", data=product_schema.json())
    assert response.status_code == 400


def test_get_product(
    product_client: TestClient, product_schemas_state: List[ReadProductSchema]
):
    response = product_client.get(f"/api/v1/products/{product_schemas_state[0].id_}")
    assert response.status_code == 200
    product_schema: List[dict] = response.json()
    assert ReadProductSchema(**product_schema).json() == product_schemas_state[0].json()


def test_get_product_id_not_found(product_client: TestClient):
    response = product_client.get("/api/v1/products/15")
    assert response.status_code == 404


def test_get_product_invalid_id(product_client: TestClient):
    response = product_client.get("/api/v1/products/invalid_id")
    assert response.status_code == 422


def test_delete_product(
    product_client: TestClient, product_schemas_state: List[ReadProductSchema]
):
    response = product_client.delete(f"/api/v1/products/{product_schemas_state[0].id_}")
    assert response.status_code == 202
    response = product_client.get(f"/api/v1/products/{product_schemas_state[0].id_}")
    assert response.status_code == 404


def test_delete_product_id_not_found(product_client: TestClient):
    response = product_client.delete("/api/v1/products/66")
    assert response.status_code == 404


def test_delete_product_invalid_id(product_client: TestClient):
    response = product_client.delete("/api/v1/products/invalid_id")
    assert response.status_code == 422


def test_update_product(
    product_client: TestClient, product_schemas_state: List[ReadProductSchema]
):
    product_schema = UpdateProductSchema(**product_schemas_state[0].dict())
    product_schema.name = "update"
    response = product_client.patch(
        f"/api/v1/products/{product_schemas_state[0].id_}", data=product_schema.json()
    )
    assert response.status_code == 200
    product_response: List[dict] = response.json()
    assert ReadProductSchema(**product_response).json(
        include={"name"}
    ) == product_schema.json(include={"name"})


def test_update_product_id_not_found(product_client: TestClient):
    response = product_client.patch("/api/v1/products/666", json={"name": "John"})
    assert response.status_code == 404


def test_update_product_invalid_id(product_client: TestClient):
    response = product_client.patch("/api/v1/products/invalid_id", json={"name": "John"})
    assert response.status_code == 422
