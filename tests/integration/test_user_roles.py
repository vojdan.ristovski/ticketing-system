from typing import List

from schemas.andon.users import CreateRoleSchema, ReadRoleSchema
from starlette.testclient import TestClient


def test_list_user_roles_empty_db(role_client: TestClient):
    response = role_client.get(
        "/api/v1/users/roles/", params={"page": 0, "page_size": 10}
    )
    assert "content" in response.json()
    assert "total" in response.json()
    user_roles = response.json()["content"]
    assert response.status_code == 200
    assert len(user_roles) == 0


def test_list_user_roles(
    role_client: TestClient, role_schemas_state: List[ReadRoleSchema]
):
    # Get response from the service
    response = role_client.get("/api/v1/users/roles/")
    assert "content" in response.json()
    assert "total" in response.json()
    roles_response: List[dict] = response.json()["content"]
    # Assert that the service returns the correct data
    assert response.status_code == 200
    assert len(roles_response) == len(role_schemas_state)
    for role, test_role in zip(roles_response, role_schemas_state):
        assert ReadRoleSchema(**role).json(
            exclude={"created_at", "updated_at"}
        ) == ReadRoleSchema.from_orm(test_role).json(
            exclude={"created_at", "updated_at"}
        )


def test_create_user_role(
    role_client: TestClient, role_empty_schemas: List[ReadRoleSchema]
):
    role_schema = CreateRoleSchema(**role_empty_schemas[0].dict())
    response = role_client.post(
        "/api/v1/users/roles", data=role_schema.json(exclude={"id"})
    )
    role_response: List[dict] = response.json()
    assert response.status_code == 200
    assert ReadRoleSchema(**role_response).json(
        exclude={"id_", "created_at", "updated_at"}
    ) == role_empty_schemas[0].json(exclude={"id_", "created_at", "updated_at"})


def test_create_user_role_invalid_schema(role_client: TestClient):
    response = role_client.post("/api/v1/users/roles", json={"some": "data"})
    assert response.status_code == 422


def test_create_user_role_already_exists(
    role_client: TestClient, role_schemas_state: List[ReadRoleSchema]
):
    role_schema = CreateRoleSchema(**role_schemas_state[0].dict())
    response = role_client.post("/api/v1/users/roles", data=role_schema.json())
    assert response.status_code == 409


def test_get_user_role(
    role_client: TestClient, role_schemas_state: List[ReadRoleSchema]
):
    response = role_client.get(f"/api/v1/users/roles/{role_schemas_state[0].id_}")
    assert response.status_code == 200
    role_schema: List[dict] = response.json()
    assert ReadRoleSchema(**role_schema).json() == role_schemas_state[0].json()


def test_get_user_role_not_found(role_client: TestClient):
    response = role_client.get("/api/v1/users/roles/5")
    assert response.status_code == 404


def test_get_user_role_invalid_id(role_client: TestClient):
    response = role_client.get("/api/v1/users/roles/invalid_id")
    assert response.status_code == 422
