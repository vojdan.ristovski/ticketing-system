from typing import List

from schemas.andon.machines import (
    CreateMachineSchema,
    ReadMachineSchema,
    UpdateMachineSchema,
)
from schemas.andon.tickets import ReadTicketSchema
from starlette.testclient import TestClient


def test_list_machine_empty_db(machine_client: TestClient):
    response = machine_client.get(
        "/api/v1/machines/", params={"page": 0, "page_size": 10, "order_by": ["name"]}
    )
    assert "content" in response.json()
    assert "total" in response.json()
    machine = response.json()["content"]
    assert response.status_code == 200
    assert len(machine) == 0


def test_list_machine(
    machine_client: TestClient, machine_schemas_state: List[ReadMachineSchema]
):
    # Get response from the service
    response = machine_client.get(
        "/api/v1/machines/", params={"page": 0, "page_size": 11, "order_by": "name"}
    )
    assert "content" in response.json()
    assert "total" in response.json()
    machine_response: List[dict] = response.json()["content"]
    # Assert that the service returns the correct data
    assert response.status_code == 200
    assert len(machine_response) == len(machine_schemas_state)
    machine_schemas_state = sorted(machine_schemas_state, key=lambda x: x.name)
    for machine, test_machine in zip(machine_response, machine_schemas_state):
        assert ReadMachineSchema(**machine).json(
            exclude={"created_at", "updated_at", "machine_type", "line"}
        ) == ReadMachineSchema.from_orm(test_machine).json(
            exclude={"created_at", "updated_at", "machine_type", "line"}
        )


def test_create_machine(
    machine_client: TestClient, machine_empty_schemas: List[ReadMachineSchema]
):
    assert machine_empty_schemas[0].machine_type is not None
    assert machine_empty_schemas[0].line is not None
    machine_schema = CreateMachineSchema(
        **machine_empty_schemas[0].dict(),
        machine_type_id=machine_empty_schemas[0].machine_type.id_,
        line_id=machine_empty_schemas[0].line.id_,
    )
    response = machine_client.post(
        "/api/v1/machines/", data=machine_schema.json(exclude={"id"})
    )
    assert response.status_code == 201
    machine_response: List[dict] = response.json()
    assert ReadMachineSchema(**machine_response).json(
        exclude={"id_", "created_at", "updated_at"}
    ) == machine_empty_schemas[0].json(exclude={"id_", "created_at", "updated_at"})


def test_create_machine_invalid_schema(machine_client: TestClient):
    response = machine_client.post("/api/v1/machines/", json={"some": "data"})
    assert response.status_code == 422


def test_create_machine_already_exists(
    machine_client: TestClient, machine_schemas_state: List[ReadMachineSchema]
):
    assert machine_schemas_state[0].machine_type is not None
    assert machine_schemas_state[0].line is not None
    machine_schema = CreateMachineSchema(
        **machine_schemas_state[0].dict(),
        machine_type_id=machine_schemas_state[0].machine_type.id_,
        line_id=machine_schemas_state[0].line.id_,
    )
    response = machine_client.post("/api/v1/machines/", data=machine_schema.json())
    assert response.status_code == 400


def test_create_machine_type_not_found(
    machine_client: TestClient, machine_empty_schemas: List[ReadMachineSchema]
):
    machine_schema = CreateMachineSchema(
        **machine_empty_schemas[0].dict(),
        machine_type_id=666,
        line_id=machine_empty_schemas[0].line.id_,
    )
    response = machine_client.post(
        "/api/v1/machines/", data=machine_schema.json(exclude={"id_"})
    )
    assert response.status_code == 404


def test_create_machine_line_not_found(
    machine_client: TestClient, machine_empty_schemas: List[ReadMachineSchema]
):
    assert machine_empty_schemas[0].machine_type is not None
    machine_schema = CreateMachineSchema(
        **machine_empty_schemas[0].dict(),
        machine_type_id=machine_empty_schemas[0].machine_type.id_,
        line_id=666,
    )
    response = machine_client.post(
        "/api/v1/machines/", data=machine_schema.json(exclude={"id_"})
    )
    assert response.status_code == 404


def test_get_machine(
    machine_client: TestClient, machine_schemas_state: List[ReadMachineSchema]
):
    response = machine_client.get(f"/api/v1/machines/{machine_schemas_state[0].id_}")
    assert response.status_code == 200
    machine_schema: List[dict] = response.json()
    assert ReadMachineSchema(**machine_schema).json() == machine_schemas_state[0].json()


def test_get_machine_not_found(machine_client: TestClient):
    response = machine_client.get("/api/v1/machines/666")
    assert response.status_code == 404


def test_get_machine_invalid_id(machine_client: TestClient):
    response = machine_client.get("/api/v1/machines/invalid_id")
    assert response.status_code == 422


def test_delete_machine(
    machine_client: TestClient, machine_schemas_state: List[ReadMachineSchema]
):
    response = machine_client.delete(f"/api/v1/machines/{machine_schemas_state[0].id_}")
    assert response.status_code == 202
    response = machine_client.get(f"/api/v1/machines/{machine_schemas_state[0].id_}")
    assert response.status_code == 404


def test_delete_machine_id_not_found(machine_client: TestClient):
    response = machine_client.delete("/api/v1/machines/666")
    assert response.status_code == 404


def test_delete_machine_invalid_id(machine_client: TestClient):
    response = machine_client.delete("/api/v1/machines/invalid_id")
    assert response.status_code == 422


def test_update_machine(
    machine_client: TestClient, machine_schemas_state: List[ReadMachineSchema]
):
    machine_schema = UpdateMachineSchema(**machine_schemas_state[0].dict())
    machine_schema.name = "update"
    machine_schema.machine_type_id = 0
    response = machine_client.patch(
        f"/api/v1/machines/{machine_schemas_state[0].id_}", data=machine_schema.json()
    )
    assert response.status_code == 200
    machine_response: List[dict] = response.json()
    assert ReadMachineSchema(**machine_response).json(
        include={"name", "order", "description", "sku"}
    ) == machine_schema.json(include={"name", "order", "description", "sku"})


def test_update_machine_already_exists(
    machine_client: TestClient, machine_schemas_state: List[ReadMachineSchema]
):
    machine_schema = UpdateMachineSchema(**machine_schemas_state[0].dict())
    machine_schema.name = "update"
    response = machine_client.patch(
        f"/api/v1/machines/{machine_schemas_state[0].id_}", data=machine_schema.json()
    )
    assert response.status_code == 400


def test_update_machine_id_not_found(machine_client: TestClient):
    response = machine_client.patch("/api/v1/machines/666", json={"name": "update"})
    assert response.status_code == 404


def test_update_machine_invalid_id(machine_client: TestClient):
    response = machine_client.patch(
        "/api/v1/machines/invalid_id", json={"name": "update"}
    )
    assert response.status_code == 422


def test_update_machine_type_not_found(
    machine_client: TestClient, machine_schemas_state: List[ReadMachineSchema]
):
    machine_schema = UpdateMachineSchema(**machine_schemas_state[0].dict())
    machine_schema.name = "update"
    machine_schema.machine_type_id = 666
    response = machine_client.patch(
        f"/api/v1/machines/{machine_schemas_state[0].id_}", data=machine_schema.json()
    )
    assert response.status_code == 404


def test_get_tickets_by_machine_id(
    machine_client: TestClient, ticket_schemas_state: List[ReadTicketSchema]
):
    response = machine_client.get(
        "/api/v1/machines/0/tickets",
        params={
            "page": 0,
            "page_size": 10,
            "status": ticket_schemas_state[0].status.value,
        },
    )
    assert "content" in response.json()
    assert "total" in response.json()
    tickets_response: List[dict] = response.json()["content"]

    # Assert that the service returns the correct data
    assert response.status_code == 200
    assert len(tickets_response) == 1
    tickets_response.sort(key=lambda x: x["id_"])
    ticket_schemas_state.sort(key=lambda x: x.id_)
    for ticket, test_ticket in zip(tickets_response, ticket_schemas_state):
        assert ReadTicketSchema(**ticket).json(
            include={"id_"}
        ) == ReadTicketSchema.from_orm(test_ticket).json(include={"id_"})


def test_get_tickets_by_machine_id_invalid_id(machine_client: TestClient):
    response = machine_client.get(
        "/api/v1/machines/invalid_id/tickets", params={"page": 0, "page_size": 10}
    )
    assert response.status_code == 422


def test_get_tickets_by_machine_id_not_found(machine_client: TestClient):
    response = machine_client.get(
        "/api/v1/machines/666/tickets", params={"page": 0, "page_size": 10}
    )
    assert response.status_code == 404
