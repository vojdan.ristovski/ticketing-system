from datetime import datetime
from typing import List

import pytest
from pydantic import ValidationError
from schemas.andon.enums import StatusEnum, TicketTypeEnum
from schemas.andon.tickets import (
    CreateTicketSchema,
    ReadTicketSchema,
    UpdateTicketSchema,
)
from starlette.testclient import TestClient

from src.models.users import User


def test_list_tickets_empty_db(ticket_client: TestClient):
    response = ticket_client.get("api/v1/tickets/", params={"page": 0, "page_size": 10})
    assert "content" in response.json()
    assert "total" in response.json()
    assert response.json()["total"] == 0
    tickets = response.json()["content"]
    assert response.status_code == 200
    assert len(tickets) == 0


def test_list_tickets(
    ticket_client: TestClient, ticket_schemas_state: List[ReadTicketSchema]
):
    # Get response from the service
    response = ticket_client.get("api/v1/tickets/", params={"page": 0, "page_size": 11})
    assert "content" in response.json()
    assert "total" in response.json()
    tickets_response: List[dict] = response.json()["content"]
    # Assert that the service returns the correct data
    assert response.status_code == 200
    assert len(tickets_response) == len(ticket_schemas_state)
    for ticket, test_ticket in zip(tickets_response, ticket_schemas_state):
        assert ReadTicketSchema(**ticket).json(
            exclude={
                "created_at",
                "updated_at",
                "machines",
                "zone",
                "line",
                "product",
                "created_by",
                "handled_by",
            }
        ) == ReadTicketSchema.from_orm(test_ticket).json(
            exclude={
                "created_at",
                "updated_at",
                "machines",
                "zone",
                "line",
                "product",
                "created_by",
                "handled_by",
            }
        )


def test_create_ticket(
    ticket_client: TestClient, ticket_empty_schemas: List[ReadTicketSchema]
):
    ticket_schema = CreateTicketSchema(
        **ticket_empty_schemas[0].dict(exclude={"status", "ticket_type"}),
        machine_ids=[x.id_ for x in ticket_empty_schemas[0].machines],
        created_by_card_id=ticket_empty_schemas[0].created_by.card_id,
        line_id=ticket_empty_schemas[0].line.id_,
        status=StatusEnum.OPEN,
        ticket_type=TicketTypeEnum.MAINTENANCE_PROBLEM,
    )
    response = ticket_client.post("api/v1/tickets/", data=ticket_schema.json())
    print(f"Response: {response.json()}")
    assert response.status_code == 201
    ticket_response: List[dict] = response.json()
    ticket_empty_schemas.sort(key=lambda x: x.id_)
    assert ReadTicketSchema(**ticket_response).json(
        include={"id"}
    ) == ticket_empty_schemas[0].json(include={"id"})


def test_create_ticket_user_not_found(
    ticket_client: TestClient, ticket_empty_schemas: List[ReadTicketSchema]
):
    ticket_schema = CreateTicketSchema(
        **ticket_empty_schemas[0].dict(exclude={"status", "ticket_type"}),
        machine_ids=[ticket_empty_schemas[0].machines[0].id_],
        created_by_card_id=666,
        line_id=ticket_empty_schemas[0].line.id_,
        status=StatusEnum.OPEN,
        ticket_type=TicketTypeEnum.MAINTENANCE_PROBLEM,
    )
    response = ticket_client.post(
        "api/v1/tickets/", data=ticket_schema.json(exclude={"id_"})
    )
    assert response.status_code == 404


def test_create_ticket_invalid_schema(ticket_client: TestClient):
    response = ticket_client.post(
        "/api/v1/tickets/",
        json={"Duci": "Brz", "ticket_type": TicketTypeEnum.PLANNED_ACTION},
    )
    assert response.status_code == 422


def test_create_ticket_planned_action_no_time_frame(
    ticket_schemas_state: List[ReadTicketSchema],
):
    with pytest.raises(ValidationError):
        CreateTicketSchema(
            **ticket_schemas_state[0].dict(
                exclude={"ticket_type", "datetime_from", "datetime_to"}
            ),
            machine_ids=[x.id_ for x in ticket_schemas_state[0].machines],
            created_by_card_id=ticket_schemas_state[0].created_by.card_id,
            line_id=ticket_schemas_state[0].line.id_,
            ticket_type=TicketTypeEnum.PLANNED_ACTION,
        )


def test_create_ticket_planned_action_no_extra_fields(
    ticket_schemas_state: List[ReadTicketSchema],
):
    with pytest.raises(ValidationError):
        CreateTicketSchema(
            **ticket_schemas_state[0].dict(exclude={"ticket_type"}),
            machine_ids=[x.id_ for x in ticket_schemas_state[0].machines],
            created_by_card_id=ticket_schemas_state[0].created_by.card_id,
            line_id=ticket_schemas_state[0].line.id_,
            ticket_type=TicketTypeEnum.PLANNED_ACTION,
        )


def test_create_ticket_material_problem_has_no_machines(
    ticket_schemas_state: List[ReadTicketSchema],
):
    with pytest.raises(ValidationError):
        CreateTicketSchema(
            **ticket_schemas_state[0].dict(exclude={"ticket_type"}),
            machine_ids=[x.id_ for x in ticket_schemas_state[0].machines],
            created_by_card_id=ticket_schemas_state[0].created_by.card_id,
            line_id=ticket_schemas_state[0].line.id_,
            ticket_type=TicketTypeEnum.MATERIALS_PROBLEM,
        )


def test_create_ticket_material_problem_has_no_reason_and_component(
    ticket_schemas_state: List[ReadTicketSchema],
):
    with pytest.raises(ValidationError):
        CreateTicketSchema(
            **ticket_schemas_state[0].dict(exclude={"ticket_type", "reason_id"}),
            machine_ids=[x.id_ for x in ticket_schemas_state[0].machines],
            created_by_card_id=ticket_schemas_state[0].created_by.card_id,
            line_id=ticket_schemas_state[0].line.id_,
            ticket_type=TicketTypeEnum.MATERIALS_PROBLEM,
        )
    with pytest.raises(ValidationError):
        CreateTicketSchema(
            **ticket_schemas_state[0].dict(exclude={"ticket_type", "component_id"}),
            machine_ids=[x.id_ for x in ticket_schemas_state[0].machines],
            created_by_card_id=ticket_schemas_state[0].created_by.card_id,
            line_id=ticket_schemas_state[0].line.id_,
            ticket_type=TicketTypeEnum.MATERIALS_PROBLEM,
        )


def test_create_ticket_machine_problems_have_machines(
    ticket_schemas_state: List[ReadTicketSchema],
):
    with pytest.raises(ValidationError):
        CreateTicketSchema(
            **ticket_schemas_state[0].dict(exclude={"ticket_type", "machines"}),
            created_by_card_id=ticket_schemas_state[0].created_by.card_id,
            line_id=ticket_schemas_state[0].line.id_,
            ticket_type=TicketTypeEnum.MACHINE_PROBLEM,
        )
    with pytest.raises(ValidationError):
        CreateTicketSchema(
            **ticket_schemas_state[0].dict(exclude={"ticket_type", "machines"}),
            created_by_card_id=ticket_schemas_state[0].created_by.card_id,
            line_id=ticket_schemas_state[0].line.id_,
            ticket_type=TicketTypeEnum.QUALITY_PROBLEM,
        )


def test_create_ticket_single_machine_problems(
    ticket_schemas_state: List[ReadTicketSchema],
):
    with pytest.raises(ValidationError):
        CreateTicketSchema(
            **ticket_schemas_state[0].dict(exclude={"ticket_type"}),
            machine_ids=[],
            created_by_card_id=ticket_schemas_state[0].created_by.card_id,
            line_id=ticket_schemas_state[0].line.id_,
            ticket_type=TicketTypeEnum.QUALITY_PROBLEM,
        )
    with pytest.raises(ValidationError):
        CreateTicketSchema(
            **ticket_schemas_state[0].dict(exclude={"ticket_type"}),
            machine_ids=[],
            created_by_card_id=ticket_schemas_state[0].created_by.card_id,
            line_id=ticket_schemas_state[0].line.id_,
            ticket_type=TicketTypeEnum.MACHINE_PROBLEM,
        )


def test_get_ticket(
    ticket_client: TestClient, ticket_schemas_state: List[ReadTicketSchema]
):
    response = ticket_client.get(f"/api/v1/tickets/{ticket_schemas_state[0].id_}")
    assert response.status_code == 200
    ticket_schema: List[dict] = response.json()
    assert ReadTicketSchema(**ticket_schema).json(
        exclude={
            "created_at",
            "updated_at",
            "machines",
            "zone",
            "line",
            "product",
            "created_by",
            "handled_by",
        }
    ) == ticket_schemas_state[0].json(
        exclude={
            "created_at",
            "updated_at",
            "machines",
            "zone",
            "line",
            "product",
            "created_by",
            "handled_by",
        }
    )


def test_get_ticket_id_not_found(ticket_client: TestClient):
    response = ticket_client.get("/api/v1/tickets/666")
    assert response.status_code == 404


def test_get_ticket_invalid_id(ticket_client: TestClient):
    response = ticket_client.get("/api/v1/tickets/invalid_id")
    assert response.status_code == 422


def test_update_ticket(
    ticket_client: TestClient, ticket_schemas_state: List[ReadTicketSchema]
):
    ticket_schema = UpdateTicketSchema(**ticket_schemas_state[0].dict())
    ticket_schema.corrective_action = "update"
    ticket_schema.approved_by_id = ticket_schemas_state[0].created_by.id_
    response = ticket_client.patch(
        f"/api/v1/tickets/{ticket_schemas_state[0].id_}",
        data=ticket_schema.json(exclude={"issue_description", "corrective_action"}),
    )
    assert response.status_code == 200
    ticket_response: List[dict] = response.json()
    assert ReadTicketSchema(**ticket_response).json(
        include={
            "reason_id",
            "status",
            "priority",
            "ticket_type",
            "component_id",
            "product_id",
            "datetime_from",
            "datetime_to",
        }
    ) == ticket_schema.json(
        exclude={
            "approved_by_id",
            "line_id",
            "corrective_action",
            "issue_description",
        }
    )


def test_update_ticket_invalid_id(ticket_client: TestClient):
    response = ticket_client.patch(
        "/api/v1/tickets/invalid_id", json={"corrective_action": "update"}
    )
    assert response.status_code == 422


def test_close_ticket(
    ticket_client: TestClient, ticket_schemas_state: List[ReadTicketSchema]
):
    response = ticket_client.post(
        f"/api/v1/tickets/{ticket_schemas_state[0].id_}/close",
        json={
            "card_id": ticket_schemas_state[0].handled_by[0].user_id,
            "corrective_action": None,
            "issue_description": None,
        },
    )
    assert response.status_code == 200
    ticket_response: List[dict] = response.json()
    ticket_response_schema = ReadTicketSchema(**ticket_response)
    assert ticket_response_schema.status == StatusEnum.CLOSED


def test_close_ticket_forbidden(
    ticket_client: TestClient, ticket_schemas_state: List[ReadTicketSchema]
):
    response = ticket_client.post(
        f"/api/v1/tickets/{ticket_schemas_state[0].id_}/close",
        json={
            "card_id": 5,
            "corrective_action": None,
            "issue_description": None,
        },
    )
    assert response.status_code == 403


def test_close_ticket_card_id_not_found(
    ticket_client: TestClient, ticket_schemas_state: List[ReadTicketSchema]
):
    response = ticket_client.post(
        f"/api/v1/tickets/{ticket_schemas_state[0].id_}/close",
        json={
            "card_id": 666,
            "corrective_action": None,
            "issue_description": None,
        },
    )
    assert response.status_code == 404


def test_close_ticket_id_not_found(ticket_client: TestClient):
    response = ticket_client.post(
        "/api/v1/tickets/666/close",
        json={
            "card_id": 1,
            "corrective_action": None,
            "issue_description": None,
        },
    )
    assert response.status_code == 404


def test_accept_ticket(
    ticket_client: TestClient, ticket_schemas_state: List[ReadTicketSchema]
):
    response = ticket_client.post(
        f"/api/v1/tickets/{ticket_schemas_state[0].id_}/accept",
        json={"card_id": 5},
    )
    assert response.status_code == 200
    ticket_response: List[dict] = response.json()
    ticket_response_schema = ReadTicketSchema(**ticket_response)
    assert ticket_response_schema.status == StatusEnum.PENDING


def test_accept_ticket_user_not_found(
    ticket_client: TestClient, ticket_schemas_state: List[ReadTicketSchema]
):
    response = ticket_client.post(
        f"/api/v1/tickets/{ticket_schemas_state[0].id_}/accept",
        json={"card_id": 666},
    )
    assert response.status_code == 404


def test_accept_ticket_not_found(ticket_client: TestClient):
    response = ticket_client.post(
        "/api/v1/tickets/666/accept",
        json={"card_id": 1},
    )
    assert response.status_code == 404


def test_accept_ticket_indvaid_ticket_id(ticket_client: TestClient):
    response = ticket_client.post(
        "/api/v1/tickets/invalid_id/accept",
        json={"card_id": 1},
    )
    assert response.status_code == 422


def test_forward_ticket_to_on_call_engineer(
    ticket_client: TestClient, ticket_schemas_state: List[ReadTicketSchema]
):
    from_card_id = ticket_schemas_state[0].handled_by[0].user.card_id
    response = ticket_client.post(
        f"/api/v1/tickets/{ticket_schemas_state[0].id_}/forward_to_on_call_engineer",
        json={"from_card_id": from_card_id},
    )
    assert response.status_code == 200
    ticket_response: List[dict] = response.json()
    ticket_response_schema = ReadTicketSchema(**ticket_response)
    assert ticket_response_schema.status == StatusEnum.FORWARDED


def test_forward_ticket_to_on_call_engineer_not_allowed(
    ticket_client: TestClient, ticket_schemas_state: List[ReadTicketSchema]
):
    response = ticket_client.post(
        f"/api/v1/tickets/{ticket_schemas_state[0].id_}/forward_to_on_call_engineer",
        json={"from_card_id": 5},
    )
    assert response.status_code == 403


def test_forward_ticket_to_on_call_engineer_user_not_found(
    ticket_client: TestClient, ticket_schemas_state: List[ReadTicketSchema]
):
    response = ticket_client.post(
        f"/api/v1/tickets/{ticket_schemas_state[0].id_}/forward_to_on_call_engineer",
        json={"from_card_id": 666},
    )
    assert response.status_code == 404


def test_forward_ticket_to_on_call_engineer_ticket_not_found(ticket_client: TestClient):
    response = ticket_client.post(
        "/api/v1/tickets/666/forward_to_on_call_engineer",
        json={"from_card_id": 0},
    )
    assert response.status_code == 404


def test_forward_ticket_to_on_call_engineer_invalid_ticket_id(ticket_client: TestClient):
    response = ticket_client.post(
        "/api/v1/tickets/invalid_id/forward_to_on_call_engineer",
        json={"from_card_id": 0},
    )
    assert response.status_code == 422


def test_forward_ticket(
    ticket_client: TestClient,
    ticket_schemas_state: List[ReadTicketSchema],
    users: List[User],
):
    from_card_id = ticket_schemas_state[0].handled_by[0].user.card_id
    to_card_id: str = ""
    for user in users:
        for h in ticket_schemas_state[0].handled_by:
            if user.id_ != h.user_id and user.card_id is not None:
                to_card_id = user.card_id
                break
        if to_card_id:
            break
    response = ticket_client.post(
        f"/api/v1/tickets/{ticket_schemas_state[0].id_}/forward",
        json={"from_card_id": from_card_id, "to_card_id": to_card_id},
    )
    assert response.status_code == 200
    ticket_response: List[dict] = response.json()
    ticket_response_schema = ReadTicketSchema(**ticket_response)
    assert ticket_response_schema.status == StatusEnum.FORWARDED


def test_forward_ticket_not_allowed(
    ticket_client: TestClient, ticket_schemas_state: List[ReadTicketSchema]
):
    response = ticket_client.post(
        f"/api/v1/tickets/{ticket_schemas_state[0].id_}/forward",
        json={"from_card_id": 5, "to_card_id": 6},
    )
    assert response.status_code == 403


def test_forward_ticket_user_not_found(
    ticket_client: TestClient, ticket_schemas_state: List[ReadTicketSchema]
):
    response = ticket_client.post(
        f"/api/v1/tickets/{ticket_schemas_state[0].id_}/forward",
        json={"from_card_id": 666, "to_card_id": 6},
    )
    assert response.status_code == 404


def test_forward_ticket_not_found(ticket_client: TestClient):
    response = ticket_client.post(
        "/api/v1/tickets/666/forward",
        json={"from_card_id": 0, "to_card_id": 6},
    )
    assert response.status_code == 404


def test_forward_ticket_invalid_id(ticket_client: TestClient):
    response = ticket_client.post(
        "/api/v1/tickets/invalid_id/forward",
        json={"from_card_id": 0, "to_card_id": 6},
    )
    assert response.status_code == 422


def test_prioritize_ticket(
    ticket_client: TestClient, ticket_schemas_state: List[ReadTicketSchema]
):
    response = ticket_client.post(
        f"/api/v1/tickets/{ticket_schemas_state[0].id_}/prioritize",
        json={"card_id": ticket_schemas_state[0].handled_by[0].user_id},
    )
    assert response.status_code == 200
    ticket_response: List[dict] = response.json()
    ticket_response_schema = ReadTicketSchema(**ticket_response)
    assert ticket_response_schema.status == StatusEnum.PRIORITY


def test_prioritize_ticket_user_not_found(
    ticket_client: TestClient, ticket_schemas_state: List[ReadTicketSchema]
):
    response = ticket_client.post(
        f"/api/v1/tickets/{ticket_schemas_state[0].id_}/prioritize",
        json={"card_id": 666},
    )
    assert response.status_code == 404


def test_prioritize_ticket_invalid_id(ticket_client: TestClient):
    response = ticket_client.post(
        "/api/v1/tickets/invalid_id/prioritize",
        json={"card_id": 1},
    )
    assert response.status_code == 422


def test_pause_ticket(
    ticket_client: TestClient, ticket_schemas_state: List[ReadTicketSchema]
):
    response = ticket_client.post(
        f"/api/v1/tickets/{ticket_schemas_state[0].id_}/pause",
        json={"card_id": ticket_schemas_state[0].handled_by[0].user_id},
    )
    assert response.status_code == 200
    ticket_response: List[dict] = response.json()
    ticket_response_schema = ReadTicketSchema(**ticket_response)
    assert ticket_response_schema.status == StatusEnum.ON_HOLD


def test_pause_ticket_user_not_found(
    ticket_client: TestClient, ticket_schemas_state: List[ReadTicketSchema]
):
    response = ticket_client.post(
        f"/api/v1/tickets/{ticket_schemas_state[0].id_}/pause",
        json={"card_id": 666},
    )
    assert response.status_code == 404


def test_pause_ticket_invalid_id(ticket_client: TestClient):
    response = ticket_client.post(
        "/api/v1/tickets/invalid_id/pause",
        json={"card_id": 1},
    )
    assert response.status_code == 422


def test_material_not_available_ticket(
    ticket_client: TestClient, ticket_schemas_state: List[ReadTicketSchema]
):
    response = ticket_client.post(
        f"/api/v1/tickets/{ticket_schemas_state[0].id_}/material_not_available",
        json={"card_id": ticket_schemas_state[0].handled_by[0].user_id},
    )
    assert response.status_code == 200
    ticket_response: List[dict] = response.json()
    ticket_response_schema = ReadTicketSchema(**ticket_response)
    assert ticket_response_schema.status == StatusEnum.MATERIAL_NOT_AVAILABLE


def test_material_not_available_ticket_user_not_found(
    ticket_client: TestClient, ticket_schemas_state: List[ReadTicketSchema]
):
    response = ticket_client.post(
        f"/api/v1/tickets/{ticket_schemas_state[0].id_}/material_not_available",
        json={"card_id": 666},
    )
    assert response.status_code == 404


def test_material_not_available_ticket_invalid_id(ticket_client: TestClient):
    response = ticket_client.post(
        "/api/v1/tickets/invalid_id/material_not_available",
        json={"card_id": 1},
    )
    assert response.status_code == 422


def test_approve_planned_action_ticket(
    ticket_client: TestClient, ticket_schemas_state: List[ReadTicketSchema]
):
    response = ticket_client.post(
        f"/api/v1/tickets/{ticket_schemas_state[0].id_}/approve_planned_action",
        json={
            "card_id": ticket_schemas_state[0].handled_by[0].user_id,
            "datetime_from": str(datetime.now()),
            "datetime_to": str(datetime.now()),
        },
    )
    assert response.status_code == 200
    ticket_response: List[dict] = response.json()
    ticket_response_schema = ReadTicketSchema(**ticket_response)
    assert ticket_response_schema.status == StatusEnum.APPROVED


def test_approve_planned_action_ticket_user_not_found(
    ticket_client: TestClient, ticket_schemas_state: List[ReadTicketSchema]
):
    response = ticket_client.post(
        f"/api/v1/tickets/{ticket_schemas_state[0].id_}/approve_planned_action",
        json={
            "card_id": 666,
            "datetime_from": str(datetime.now()),
            "datetime_to": str(datetime.now()),
        },
    )
    assert response.status_code == 404


def test_approve_planned_action_ticket_invalid_id(ticket_client: TestClient):
    response = ticket_client.post(
        "/api/v1/tickets/invalid_id/approve_planned_action",
        json={
            "card_id": 1,
            "datetime_from": str(datetime.now()),
            "datetime_to": str(datetime.now()),
        },
    )
    assert response.status_code == 422


def test_reject_planned_action_ticket(
    ticket_client: TestClient, ticket_schemas_state: List[ReadTicketSchema]
):
    response = ticket_client.post(
        f"/api/v1/tickets/{ticket_schemas_state[0].id_}/reject_planned_action",
        json={"card_id": ticket_schemas_state[0].handled_by[0].user_id},
    )
    assert response.status_code == 200
    ticket_response: List[dict] = response.json()
    ticket_response_schema = ReadTicketSchema(**ticket_response)
    assert ticket_response_schema.status == StatusEnum.REJECTED


def test_reject_planned_action_ticket_user_not_found(
    ticket_client: TestClient, ticket_schemas_state: List[ReadTicketSchema]
):
    response = ticket_client.post(
        f"/api/v1/tickets/{ticket_schemas_state[0].id_}/reject_planned_action",
        json={"card_id": 666},
    )
    assert response.status_code == 404


def test_reject_planned_action_ticket_invalid_id(ticket_client: TestClient):
    response = ticket_client.post(
        "/api/v1/tickets/invalid_id/reject_planned_action",
        json={"card_id": 1},
    )
    assert response.status_code == 422
