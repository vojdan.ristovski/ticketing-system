from typing import List

from schemas.andon.zone import CreateZoneSchema, ReadZoneSchema, UpdateZoneSchema
from starlette.testclient import TestClient


def test_list_zones_empty_db(zone_client: TestClient):
    response = zone_client.get("/api/v1/zones/", params={"page": 0, "page_size": 10})
    assert "content" in response.json()
    assert "total" in response.json()
    assert response.json()["total"] == 0
    zones = response.json()["content"]
    assert response.status_code == 200
    assert len(zones) == 0


def test_list_zones(zone_client: TestClient, zone_schemas_state: List[ReadZoneSchema]):
    # Get response from the service
    response = zone_client.get("/api/v1/zones/", params={"page": 0, "page_size": 11})
    assert "content" in response.json()
    assert "total" in response.json()
    zones_response: List[dict] = response.json()["content"]
    # Assert that the service returns the correct data
    assert response.status_code == 200
    assert len(zones_response) == len(zone_schemas_state)
    for zone, test_zone in zip(zones_response, zone_schemas_state):
        assert ReadZoneSchema(**zone).json(
            exclude={"created_at", "updated_at"}
        ) == ReadZoneSchema.from_orm(test_zone).json(
            exclude={"created_at", "updated_at"}
        )


def test_create_zone(zone_client: TestClient, zone_empty_schemas: List[ReadZoneSchema]):
    zone_schema = CreateZoneSchema(**zone_empty_schemas[0].dict())
    response = zone_client.post("/api/v1/zones/", data=zone_schema.json(exclude={"id"}))
    zone_response: List[dict] = response.json()
    assert response.status_code == 201
    assert ReadZoneSchema(**zone_response).json(
        exclude={"id_", "created_at", "updated_at"}
    ) == zone_empty_schemas[0].json(exclude={"id_", "created_at", "updated_at"})


def test_create_zone_invalid_schema(zone_client: TestClient):
    response = zone_client.post("/api/v1/zones/", json={"reason": "test"})
    assert response.status_code == 422


def test_create_zone_already_exists(
    zone_client: TestClient, zone_schemas_state: List[ReadZoneSchema]
):
    zone_schema = CreateZoneSchema(**zone_schemas_state[0].dict())
    response = zone_client.post("/api/v1/zones/", data=zone_schema.json())
    assert response.status_code == 409


def test_get_zone(zone_client: TestClient, zone_schemas_state: List[ReadZoneSchema]):
    response = zone_client.get(f"/api/v1/zones/{zone_schemas_state[0].id_}")
    assert response.status_code == 200
    zone_schema: List[dict] = response.json()
    assert ReadZoneSchema(**zone_schema).json() == zone_schemas_state[0].json()


def test_get_zone_id_not_found(zone_client: TestClient):
    response = zone_client.get("/api/v1/zones/15")
    assert response.status_code == 404


def test_get_zone_invalid_id(zone_client: TestClient):
    response = zone_client.get("/api/v1/zones/invalid_id")
    assert response.status_code == 422


def test_delete_zone(zone_client: TestClient, zone_schemas_state: List[ReadZoneSchema]):
    response = zone_client.delete(f"/api/v1/zones/{zone_schemas_state[0].id_}")
    assert response.status_code == 202
    response = zone_client.get(f"/api/v1/zones/{zone_schemas_state[0].id_}")
    assert response.status_code == 404


def test_delete_zone_id_not_found(zone_client: TestClient):
    response = zone_client.delete("/api/v1/zones/66")
    assert response.status_code == 404


def test_delete_zone_invalid_id(zone_client: TestClient):
    response = zone_client.delete("/api/v1/zones/invalid_id")
    assert response.status_code == 422


def test_update_zone(zone_client: TestClient, zone_schemas_state: List[ReadZoneSchema]):
    zone_schema = UpdateZoneSchema(**zone_schemas_state[0].dict())
    zone_schema.name = "update"
    response = zone_client.patch(
        f"/api/v1/zones/{zone_schemas_state[0].id_}", data=zone_schema.json()
    )
    assert response.status_code == 200
    zone_response: List[dict] = response.json()
    assert (
        ReadZoneSchema(**zone_response).json(
            exclude={"id_", "created_at", "updated_at", "deleted_at", "deleted"}
        )
        == zone_schema.json()
    )


def test_update_zone_id_not_found(zone_client: TestClient):
    response = zone_client.patch("/api/v1/zones/666", json={"name": "John"})
    assert response.status_code == 404


def test_update_zone_invalid_id(zone_client: TestClient):
    response = zone_client.patch("/api/v1/zones/invalid_id", json={"name": "John"})
    assert response.status_code == 422
