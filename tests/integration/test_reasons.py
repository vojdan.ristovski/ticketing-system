from typing import List

from schemas.andon.reasons import (
    CreateReasonSchema,
    ReadReasonSchema,
    UpdateReasonSchema,
)
from starlette.testclient import TestClient


def test_list_reasons_empty_db(reason_client: TestClient):
    response = reason_client.get("/api/v1/reasons/", params={"page": 0, "page_size": 10})
    assert "content" in response.json()
    assert "total" in response.json()
    assert response.json()["total"] == 0
    reasons = response.json()["content"]
    assert response.status_code == 200
    assert len(reasons) == 0


def test_list_reasons(
    reason_client: TestClient, reason_schemas_state: List[ReadReasonSchema]
):
    # Get response from the service
    response = reason_client.get("/api/v1/reasons/", params={"page": 0, "page_size": 11})
    assert "content" in response.json()
    assert "total" in response.json()
    reasons_response: List[dict] = response.json()["content"]
    # Assert that the service returns the correct data
    assert response.status_code == 200
    assert len(reasons_response) == len(reason_schemas_state)
    for reason, test_reason in zip(reasons_response, reason_schemas_state):
        assert ReadReasonSchema(**reason).json(
            exclude={"created_at", "updated_at"}
        ) == ReadReasonSchema.from_orm(test_reason).json(
            exclude={"created_at", "updated_at"}
        )


def test_create_reason(
    reason_client: TestClient, reason_empty_schemas: List[ReadReasonSchema]
):
    reason_schema = CreateReasonSchema(**reason_empty_schemas[0].dict())
    response = reason_client.post(
        "/api/v1/reasons/", data=reason_schema.json(exclude={"id"})
    )
    reason_response: List[dict] = response.json()
    assert response.status_code == 201
    assert ReadReasonSchema(**reason_response).json(
        exclude={"id_", "created_at", "updated_at"}
    ) == reason_empty_schemas[0].json(exclude={"id_", "created_at", "updated_at"})


def test_create_reason_invalid_schema(reason_client: TestClient):
    response = reason_client.post("/api/v1/reasons/", json={"some": "test"})
    assert response.status_code == 422


def test_create_reason_already_exists(
    reason_client: TestClient, reason_schemas_state: List[ReadReasonSchema]
):
    reason_schema = CreateReasonSchema(**reason_schemas_state[0].dict())
    response = reason_client.post("/api/v1/reasons/", data=reason_schema.json())
    assert response.status_code == 400


def test_get_reason(
    reason_client: TestClient, reason_schemas_state: List[ReadReasonSchema]
):
    response = reason_client.get(f"/api/v1/reasons/{reason_schemas_state[0].id_}")
    assert response.status_code == 200
    reason_schema: List[dict] = response.json()
    assert ReadReasonSchema(**reason_schema).json() == reason_schemas_state[0].json()


def test_get_reason_id_not_found(reason_client: TestClient):
    response = reason_client.get("/api/v1/reasons/15")
    assert response.status_code == 404


def test_get_reason_invalid_id(reason_client: TestClient):
    response = reason_client.get("/api/v1/reasons/invalid_id")
    assert response.status_code == 422


def test_delete_reason(
    reason_client: TestClient, reason_schemas_state: List[ReadReasonSchema]
):
    response = reason_client.delete(f"/api/v1/reasons/{reason_schemas_state[0].id_}")
    assert response.status_code == 202
    response = reason_client.get(f"/api/v1/reasons/{reason_schemas_state[0].id_}")
    assert response.status_code == 404


def test_delete_reason_id_not_found(reason_client: TestClient):
    response = reason_client.delete("/api/v1/reasons/66")
    assert response.status_code == 404


def test_delete_reason_invalid_id(reason_client: TestClient):
    response = reason_client.delete("/api/v1/reasons/invalid_id")
    assert response.status_code == 422


def test_update_reason(
    reason_client: TestClient, reason_schemas_state: List[ReadReasonSchema]
):
    reason_schema = UpdateReasonSchema(**reason_schemas_state[0].dict())
    reason_schema.reason = "update"
    response = reason_client.patch(
        f"/api/v1/reasons/{reason_schemas_state[0].id_}", data=reason_schema.json()
    )
    assert response.status_code == 202
    reason_response: List[dict] = response.json()
    assert (
        ReadReasonSchema(**reason_response).json(
            exclude={"id_", "created_at", "updated_at", "deleted_at", "deleted"}
        )
        == reason_schema.json()
    )


def test_update_reason_id_not_found(reason_client: TestClient):
    response = reason_client.patch("/api/v1/reasons/666", json={"reason": "test"})
    assert response.status_code == 404


def test_update_reason_invalid_id(reason_client: TestClient):
    response = reason_client.patch("/api/v1/reasons/invalid_id", json={"name": "John"})
    assert response.status_code == 422
