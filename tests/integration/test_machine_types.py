from typing import List

from schemas.andon.machines import (
    CreateMachineTypeSchema,
    ReadMachineTypeSchema,
    UpdateMachineTypeSchema,
)
from starlette.testclient import TestClient


def test_list_machine_types_empty_db(machine_type_client: TestClient):
    response = machine_type_client.get(
        "/api/v1/machines/types", params={"page": 0, "page_size": 10}
    )
    assert "content" in response.json()
    assert "total" in response.json()
    machine_types = response.json()["content"]
    assert response.status_code == 200
    assert len(machine_types) == 0


def test_list_machine_types(
    machine_type_client: TestClient,
    machine_type_schemas_state: List[ReadMachineTypeSchema],
):
    # Get response from the service
    response = machine_type_client.get(
        "/api/v1/machines/types", params={"page": 0, "page_size": 11}
    )
    assert "content" in response.json()
    assert "total" in response.json()
    machine_types_response: List[dict] = response.json()["content"]
    # Assert that the service returns the correct data
    assert response.status_code == 200
    assert len(machine_types_response) == len(machine_type_schemas_state)
    for machine_type, test_machine_type in zip(
        machine_types_response, machine_type_schemas_state
    ):
        assert ReadMachineTypeSchema(**machine_type).json(
            exclude={"created_at", "updated_at"}
        ) == ReadMachineTypeSchema.from_orm(test_machine_type).json(
            exclude={"created_at", "updated_at"}
        )


def test_create_machine_type(
    machine_type_client: TestClient,
    machine_type_empty_schemas: List[ReadMachineTypeSchema],
):
    machine_type_schema = CreateMachineTypeSchema(**machine_type_empty_schemas[0].dict())
    response = machine_type_client.post(
        "/api/v1/machines/types", data=machine_type_schema.json(exclude={"id"})
    )
    assert response.status_code == 201
    machine_type_response: List[dict] = response.json()
    assert ReadMachineTypeSchema(**machine_type_response).json(
        exclude={"id_", "created_at", "updated_at"}
    ) == machine_type_empty_schemas[0].json(exclude={"id_", "created_at", "updated_at"})


def test_create_machine_type_invalid_schema(machine_type_client: TestClient):
    response = machine_type_client.post("/api/v1/machines/types", json={"some": "data"})
    assert response.status_code == 422


def test_create_machine_type_already_exists(
    machine_type_client: TestClient,
    machine_type_schemas_state: List[ReadMachineTypeSchema],
):
    machine_type_schema = CreateMachineTypeSchema(**machine_type_schemas_state[0].dict())
    response = machine_type_client.post(
        "/api/v1/machines/types", data=machine_type_schema.json()
    )
    assert response.status_code == 400


def test_get_machine_type(
    machine_type_client: TestClient,
    machine_type_schemas_state: List[ReadMachineTypeSchema],
):
    response = machine_type_client.get(
        f"/api/v1/machines/types/{machine_type_schemas_state[0].id_}"
    )
    assert response.status_code == 200
    machine_type_schema: List[dict] = response.json()
    assert (
        ReadMachineTypeSchema(**machine_type_schema).json()
        == machine_type_schemas_state[0].json()
    )


def test_get_machine_type_not_found(machine_type_client: TestClient):
    response = machine_type_client.get("/api/v1/machines/types/5")
    assert response.status_code == 404


def test_get_machine_type_invalid_id(machine_type_client: TestClient):
    response = machine_type_client.get("/api/v1/machines/types/invalid_id")
    assert response.status_code == 422


def test_update_machine_type(
    machine_type_client: TestClient,
    machine_type_schemas_state: List[ReadMachineTypeSchema],
):
    machine_type_schema = UpdateMachineTypeSchema(**machine_type_schemas_state[0].dict())
    machine_type_schema.name = "update"
    response = machine_type_client.patch(
        f"/api/v1/machines/types/{machine_type_schemas_state[0].id_}",
        data=machine_type_schema.json(),
    )
    assert response.status_code == 201
    machine_type_response: List[dict] = response.json()
    assert (
        ReadMachineTypeSchema(**machine_type_response).json(exclude={"id_"})
        == machine_type_schema.json()
    )


def test_update_machine_type_already_exists(
    machine_type_client: TestClient,
    machine_type_schemas_state: List[ReadMachineTypeSchema],
):
    machine_type_schema = UpdateMachineTypeSchema(**machine_type_schemas_state[0].dict())
    machine_type_schema.name = "test_type1"
    response = machine_type_client.patch(
        f"/api/v1/machines/types/{machine_type_schemas_state[0].id_}",
        data=machine_type_schema.json(),
    )
    assert response.status_code == 409


def test_update_machine_type_id_not_found(machine_type_client: TestClient):
    response = machine_type_client.patch(
        "/api/v1/machines/types/666", json={"name": "update"}
    )
    assert response.status_code == 404


def test_update_machine_type_invalid_id(machine_client: TestClient):
    response = machine_client.patch(
        "/api/v1/machines/types/invalid_id", json={"name": "update"}
    )
    assert response.status_code == 422
