from typing import List

from schemas.andon.component import CreateComponentSchema, ReadComponentSchema
from starlette.testclient import TestClient


def test_list_components_empty_db(component_client: TestClient):
    response = component_client.get(
        "/api/v1/components/", params={"page": 0, "page_size": 10}
    )
    assert "content" in response.json()
    assert "total" in response.json()
    assert response.json()["total"] == 0
    assert response.status_code == 200
    components = response.json()["content"]
    assert len(components) == 0


def test_list_components(
    component_client: TestClient, component_schemas_state: List[ReadComponentSchema]
):
    # Get response from the service
    response = component_client.get(
        "/api/v1/components/", params={"page": 0, "page_size": 11}
    )
    assert "content" in response.json()
    assert "total" in response.json()
    components_response: List[dict] = response.json()["content"]
    # Assert that the service returns the correct data
    assert response.status_code == 200
    assert len(components_response) == len(component_schemas_state)
    for component, test_component in zip(components_response, component_schemas_state):
        assert ReadComponentSchema(**component).json(
            exclude={"created_at", "updated_at"}
        ) == ReadComponentSchema.from_orm(test_component).json(
            exclude={"created_at", "updated_at"}
        )


def test_create_component(
    component_client: TestClient, component_empty_schemas: List[ReadComponentSchema]
):
    component_schema = CreateComponentSchema(**component_empty_schemas[0].dict())
    response = component_client.post(
        "/api/v1/components/", data=component_schema.json(exclude={"id"})
    )
    assert response.status_code == 201
    component_response: List[dict] = response.json()
    assert ReadComponentSchema(**component_response).json(
        exclude={"id_", "created_at", "updated_at"}
    ) == component_empty_schemas[0].json(exclude={"id_", "created_at", "updated_at"})


def test_create_component_invalid_schema(component_client: TestClient):
    response = component_client.post("/api/v1/components/", json={"reason": "test"})
    assert response.status_code == 422


def test_create_component_already_exists(
    component_client: TestClient, component_schemas_state: List[ReadComponentSchema]
):
    component_schema = CreateComponentSchema(**component_schemas_state[0].dict())
    response = component_client.post("/api/v1/components/", data=component_schema.json())
    assert response.status_code == 400


def test_get_component(
    component_client: TestClient, component_schemas_state: List[ReadComponentSchema]
):
    response = component_client.get(
        f"/api/v1/components/{component_schemas_state[0].id_}"
    )
    assert response.status_code == 200
    component_schema: List[dict] = response.json()
    assert (
        ReadComponentSchema(**component_schema).json()
        == component_schemas_state[0].json()
    )


def test_get_component_id_not_found(component_client: TestClient):
    response = component_client.get("/api/v1/components/666")
    assert response.status_code == 404


def test_get_component_invalid_id(component_client: TestClient):
    response = component_client.get("/api/v1/components/invalid_id")
    assert response.status_code == 422


def test_delete_component(
    component_client: TestClient, component_schemas_state: List[ReadComponentSchema]
):
    response = component_client.delete(
        f"/api/v1/components/{component_schemas_state[0].id_}"
    )
    assert response.status_code == 202
    response = component_client.get(
        f"/api/v1/components/{component_schemas_state[0].id_}"
    )
    assert response.status_code == 404


def test_delete_component_id_not_found(component_client: TestClient):
    response = component_client.delete("/api/v1/components/666")
    assert response.status_code == 404


def test_delete_component_invalid_id(component_client: TestClient):
    response = component_client.delete("/api/v1/components/invalid_id")
    assert response.status_code == 422
