from typing import List

import pytest
from pydantic import ValidationError
from schemas.andon.events_config import (
    BasicProblem,
    Daily,
    Escalation,
    EventsConfig,
    MaterialsProblem,
    PlannedAction,
)
from schemas.andon.lines import CreateLineSchema, ReadLineSchema, UpdateLineSchema
from schemas.andon.machines import ReadMachineSchema
from schemas.andon.products import ReadProductSchema
from schemas.andon.tickets import ReadTicketSchema
from schemas.andon.users import ReadUserSchema
from starlette.testclient import TestClient

from src.models.users import User


def test_list_lines_empty_db(line_client: TestClient):
    response = line_client.get(
        "/api/v1/lines/", params={"page": 0, "page_size": 11, "order_by": "name"}
    )
    assert "content" in response.json()
    assert "total" in response.json()
    assert response.json()["total"] == 0
    assert response.status_code == 200
    lines = response.json()["content"]
    assert len(lines) == 0


def test_list_lines(line_client: TestClient, line_schemas_state: List[ReadLineSchema]):
    # Get response from the service
    response = line_client.get(
        "/api/v1/lines/", params={"page": 0, "page_size": 11, "order_by": "name"}
    )
    assert "content" in response.json()
    assert "total" in response.json()
    assert response.status_code == 200
    lines_response: List[dict] = response.json()["content"]
    # Assert that the service returns the correct data
    assert len(lines_response) == len(line_schemas_state)
    for line, test_line in zip(lines_response, line_schemas_state):
        assert ReadLineSchema(**line).json(
            exclude={"created_at", "updated_at"}
        ) == ReadLineSchema.from_orm(test_line).json(
            exclude={"created_at", "updated_at"}
        )


def test_create_line(line_client: TestClient, line_empty_schemas: List[ReadLineSchema]):
    line_schema = CreateLineSchema(**line_empty_schemas[0].dict())
    response = line_client.post("/api/v1/lines/", data=line_schema.json(exclude={"id"}))
    line_response: List[dict] = response.json()
    assert response.status_code == 201
    assert ReadLineSchema(**line_response).json(
        exclude={"id_", "created_at", "updated_at"}
    ) == line_empty_schemas[0].json(exclude={"id_", "created_at", "updated_at"})


def test_create_line_invalid_schema(line_client: TestClient):
    response = line_client.post("/api/v1/lines/", json={"reason": "test"})
    assert response.status_code == 422


def test_create_line_already_exists(
    line_client: TestClient, line_schemas_state: List[ReadLineSchema]
):
    line_schema = CreateLineSchema(**line_schemas_state[0].dict())
    response = line_client.post("/api/v1/lines/", data=line_schema.json())
    assert response.status_code == 409


def test_create_line_zone_not_found(
    line_client: TestClient, line_schemas_state: List[ReadLineSchema]
):
    line_schema = CreateLineSchema(
        **line_schemas_state[0].dict(exclude={"zone_id"}), zone_id=666
    )
    response = line_client.post("/api/v1/lines/", data=line_schema.json())
    assert response.status_code == 409


def test_create_line_role_not_found(
    line_client: TestClient, line_schemas_state: List[ReadLineSchema]
):
    basic_problems = BasicProblem(
        initial_escalation=Escalation(role_id=1, seconds_delay=0),
        escalation_1=Escalation(role_id=1, seconds_delay=0),
        escalation_2=Escalation(role_id=1, seconds_delay=0),
        escalation_3=Escalation(role_id=1, seconds_delay=0),
        escalation_4=Escalation(role_id=1, seconds_delay=0),
        escalation_5=Escalation(role_id=1, seconds_delay=0),
        forwarded_escalation=Escalation(role_id=1, seconds_delay=0),
        closed_escalation=Escalation(role_id=1, seconds_delay=0),
        daily=Daily(role_id=1, hour=0, minute=0),
    )
    materials_problems = MaterialsProblem(
        initial_escalation=Escalation(role_id=1, seconds_delay=0),
        escalation_1=Escalation(role_id=1, seconds_delay=0),
        escalation_2=Escalation(role_id=1, seconds_delay=0),
        escalation_3=Escalation(role_id=1, seconds_delay=0),
        escalation_4=Escalation(role_id=1, seconds_delay=0),
        escalation_5=Escalation(role_id=1, seconds_delay=0),
        confirmation_escalation=Escalation(role_id=1, seconds_delay=0),
        material_unavailable_escalation=Escalation(role_id=1, seconds_delay=0),
        daily=Daily(role_id=1, hour=0, minute=0),
    )
    event_config = EventsConfig(
        MACHINE_PROBLEM=basic_problems,
        QUALITY_PROBLEM=basic_problems,
        MAINTENANCE_PROBLEM=basic_problems,
        MATERIALS_PROBLEM=materials_problems,
        PLANNED_ACTION=PlannedAction(
            short_action_role_id=666,
            long_action_role_id=666,
            short_action_length_seconds=1800,
        ),
    )
    line_schema = CreateLineSchema(
        **line_schemas_state[0].dict(exclude={"event_config"}), event_config=event_config
    )
    response = line_client.post("/api/v1/lines/", data=line_schema.json())
    assert response.status_code == 409


def test_create_line_event_config_invalid_schema(
    line_schemas_state: List[ReadLineSchema],
):
    with pytest.raises(ValidationError):
        CreateLineSchema(
            **line_schemas_state[0].dict(exclude={"event_config"}),
            event_config=EventsConfig(
                MACHINE_PROBLEM=0,
                QUALITY_PROBLEM=0,
                MAINTENANCE_PROBLEM=0,
                MATERIALS_PROBLEM=0,
                PLANNED_ACTION=0,
            ),
        )


def test_get_line(line_client: TestClient, line_schemas_state: List[ReadLineSchema]):
    response = line_client.get(f"/api/v1/lines/{line_schemas_state[0].id_}")
    assert response.status_code == 200
    line_schema: List[dict] = response.json()
    assert ReadLineSchema(**line_schema).json() == line_schemas_state[0].json()


def test_get_line_id_not_found(line_client: TestClient):
    response = line_client.get("/api/v1/lines/15")
    assert response.status_code == 404


def test_get_line_invalid_id(line_client: TestClient):
    response = line_client.get("/api/v1/lines/invalid_id")
    assert response.status_code == 422


def test_delete_line(line_client: TestClient, line_schemas_state: List[ReadLineSchema]):
    response = line_client.delete(f"/api/v1/lines/{line_schemas_state[0].id_}")
    assert response.status_code == 202
    response = line_client.get(f"/api/v1/lines/{line_schemas_state[0].id_}")
    assert response.status_code == 404


def test_delete_line_id_not_found(line_client: TestClient):
    response = line_client.delete("/api/v1/lines/666")
    assert response.status_code == 404


def test_delete_line_invalid_id(line_client: TestClient):
    response = line_client.delete("/api/v1/lines/invalid_id")
    assert response.status_code == 422


def test_delete_line_machines_on_line(
    line_client: TestClient, machine_schemas_state: List[ReadMachineSchema]
):
    line = machine_schemas_state[0].line
    response = line_client.delete(f"/api/v1/lines/{line.id_}")
    assert response.status_code == 409


def test_update_line(line_client: TestClient, line_schemas_state: List[ReadLineSchema]):
    line_schema = UpdateLineSchema(**line_schemas_state[0].dict())
    line_schema.name = "update"
    response = line_client.patch(
        f"/api/v1/lines/{line_schemas_state[0].id_}", data=line_schema.json()
    )
    assert response.status_code == 200
    line_response: List[dict] = response.json()
    assert (
        ReadLineSchema(**line_response).json(
            exclude={
                "id_",
                "created_at",
                "updated_at",
                "deleted_at",
                "deleted",
                "zone_id",
            }
        )
        == line_schema.json()
    )


def test_update_line_id_not_found(
    line_client: TestClient, line_schemas_state: List[ReadLineSchema]
):
    line_schema = UpdateLineSchema(**line_schemas_state[0].dict())
    line_schema.name = "update"
    response = line_client.patch("/api/v1/lines/666", data=line_schema.json())
    assert response.status_code == 404


def test_update_line_invalid_id(line_client: TestClient):
    response = line_client.patch("/api/v1/lines/invalid_id", json={"name": "John"})
    assert response.status_code == 422


def test_get_machines_by_line_id(
    line_client: TestClient, machine_schemas_state: List[ReadMachineSchema]
):
    line = machine_schemas_state[0].line
    response = line_client.get(
        f"/api/v1/lines/{line.id_}/machines",
        params={"page": 0, "page_size": 11, "order_by": "order"},
    )
    assert "content" in response.json()
    assert "total" in response.json()
    machines_response: List[dict] = response.json()["content"]
    # Assert that the service returns the correct data
    assert response.status_code == 200
    assert len(machines_response) == 1
    for machine, test_machine in zip(machines_response, machine_schemas_state):
        assert (
            ReadMachineSchema(**machine).json()
            == ReadMachineSchema.from_orm(test_machine).json()
        )


def test_get_machines_by_line_id_invalid_id(line_client: TestClient):
    response = line_client.get(
        "/api/v1/lines/invalid_id/machines",
        params={"page": 0, "page_size": 11, "order_by": "order"},
    )
    assert response.status_code == 422


def test_get_machines_by_line_id_not_found(line_client: TestClient):
    response = line_client.get(
        "/api/v1/lines/666/machines",
        params={"page": 0, "page_size": 11, "order_by": "order"},
    )
    assert response.status_code == 404


def test_get_products_by_line_id(
    line_client: TestClient, product_schemas_state: List[ReadProductSchema]
):
    response = line_client.get(
        f"/api/v1/lines/{product_schemas_state[0].lines[0].id_}/products",
        params={"page": 0, "page_size": 11, "order_by": "name"},
    )
    assert "content" in response.json()
    assert "total" in response.json()
    products_response: List[dict] = response.json()["content"]
    # Assert that the service returns the correct data
    assert response.status_code == 200
    assert len(products_response) == len(product_schemas_state)
    product_schemas_state = sorted(product_schemas_state, key=lambda x: x.name)
    for product, test_product in zip(products_response, product_schemas_state):
        assert ReadProductSchema(**product).json(
            exclude={"lines"}
        ) == ReadProductSchema.from_orm(test_product).json(exclude={"lines"})


def test_get_products_by_line_id_invalid_id(line_client: TestClient):
    response = line_client.get(
        "/api/v1/lines/invalid_id/products",
        params={"page": 0, "page_size": 11, "order_by": "name"},
    )
    assert response.status_code == 422


def test_get_products_by_line_id_not_found(line_client: TestClient):
    response = line_client.get(
        "/api/v1/lines/666/products",
        params={"page": 0, "page_size": 11, "order_by": "name"},
    )
    assert response.status_code == 404


def test_get_tickets_by_line_id(
    line_client: TestClient, ticket_schemas_state: List[ReadTicketSchema]
):
    exclusion = {
        "created_at",
        "updated_at",
        "machines",
        "zone",
        "line",
        "product",
        "created_by",
        "handled_by",
    }
    response = line_client.get(
        f"/api/v1/lines/{ticket_schemas_state[0].line.id_}/tickets",
        params={
            "page": 0,
            "page_size": 11,
            "status": ticket_schemas_state[0].status.value,
            "type": ticket_schemas_state[0].ticket_type.value,
            "reason_id": ticket_schemas_state[0].reason_id,
        },
    )
    assert "content" in response.json()
    assert "total" in response.json()
    assert response.status_code == 200
    tickets_response: List[dict] = response.json()["content"]
    # Assert that the service returns the correct data
    assert len(tickets_response) == 1
    for ticket, test_ticket in zip(tickets_response, ticket_schemas_state):
        assert ReadTicketSchema(**ticket).json(
            exclude=exclusion
        ) == ReadTicketSchema.from_orm(test_ticket).json(exclude=exclusion)


def test_get_tickets_by_line_id_no_status_and_type(
    line_client: TestClient, ticket_schemas_state: List[ReadTicketSchema]
):
    line = ticket_schemas_state[0].line
    exclude = {
        "created_at",
        "updated_at",
        "machines",
        "zone",
        "line",
        "product",
        "created_by",
        "handled_by",
    }
    response = line_client.get(
        f"/api/v1/lines/{line.id_}/tickets",
        params={
            "page": 0,
            "page_size": 11,
        },
    )
    assert "content" in response.json()
    assert "total" in response.json()
    assert response.status_code == 200
    tickets_response: List[dict] = response.json()["content"]
    # Assert that the service returns the correct data
    assert len(tickets_response) == 1
    for ticket, test_ticket in zip(tickets_response, ticket_schemas_state):
        assert ReadTicketSchema(**ticket).json(
            exclude=exclude
        ) == ReadTicketSchema.from_orm(test_ticket).json(exclude=exclude)


def test_get_tickets_by_line_id_invalid_id(line_client: TestClient):
    response = line_client.get(
        "/api/v1/lines/invalid_id/tickets", params={"page": 0, "page_size": 11}
    )
    assert response.status_code == 422


def test_get_tickets_by_line_id_not_found(line_client: TestClient):
    response = line_client.get(
        "/api/v1/lines/666/tickets", params={"page": 0, "page_size": 11}
    )
    assert response.status_code == 404


def test_get_users_by_line_id(
    line_client: TestClient,
    user_line_association_schema: List[ReadLineSchema],
    users: List[User],
):
    line_id = user_line_association_schema[0].id_
    response = line_client.get(
        f"/api/v1/lines/{line_id}/users", params={"page": 0, "page_size": 11}
    )
    assert "content" in response.json()
    assert "total" in response.json()
    assert response.status_code == 200
    assert users[0].id_ in [x["id_"] for x in response.json()["content"]]


def test_get_users_by_line_id_invalid_id(line_client: TestClient):
    response = line_client.get(
        "/api/v1/lines/invalid_id/users", params={"page": 0, "page_size": 11}
    )
    assert response.status_code == 422


def test_get_users_by_line_id_not_found(line_client: TestClient):
    response = line_client.get(
        "/api/v1/lines/666/users", params={"page": 0, "page_size": 11}
    )
    assert response.status_code == 404


def test_get_users_by_line_id_and_role_id(
    line_client: TestClient,
    user_line_association_schema: List[ReadLineSchema],
    users: List[User],
):
    line_id = user_line_association_schema[0].id_
    role_id = users[0].roles[0].id_
    response = line_client.get(f"/api/v1/lines/{line_id}/users/{role_id}")
    assert response.status_code == 200
    for user in users:
        if role_id in user.roles:
            assert ReadUserSchema(**user).json(exclude={"roles"}) in response.json()


def test_get_users_by_line_id_and_role_id_invalid_line_id(line_client: TestClient):
    response = line_client.get("/api/v1/lines/invalid_id/users/1")
    assert response.status_code == 422


def test_get_users_by_line_id_and_role_id_invalid_role_id(line_client: TestClient):
    response = line_client.get("/api/v1/lines/1/users/invalid_id")
    assert response.status_code == 422


def test_get_users_by_line_id_and_role_id_line_not_found(line_client: TestClient):
    response = line_client.get("/api/v1/lines/666/users/1")
    assert response.status_code == 404


def test_get_users_by_line_id_and_role_id_role_not_found(
    line_client: TestClient, line_schemas_state: List[ReadLineSchema]
):
    response = line_client.get(f"/api/v1/lines/{line_schemas_state[0].id_}/users/666")
    assert response.status_code == 409


def test_add_users_to_line(
    line_client: TestClient,
    user_schemas_state: List[ReadUserSchema],
    line_schemas_state: List[ReadLineSchema],
):
    user_ids = [x.id_ for x in user_schemas_state]
    response = line_client.put(
        f"/api/v1/lines/{line_schemas_state[0].id_}/add_users", json=user_ids
    )
    print("Response: ", response.json())
    assert response.status_code == 200
    line_response: List[dict] = response.json()
    assert ReadLineSchema(**line_response).json(
        exclude={"id_", "created_at", "updated_at"}
    ) == line_schemas_state[0].json(exclude={"id_", "created_at", "updated_at"})


def test_add_users_to_line_user_not_found(
    line_client: TestClient, line_schemas_state: List[ReadLineSchema]
):
    response = line_client.put(
        f"/api/v1/lines/{line_schemas_state[0].id_}/add_users", json=[666]
    )
    assert response.status_code == 404


def test_add_users_to_line_line_not_found(line_client: TestClient):
    response = line_client.put("/api/v1/lines/666/add_users", json=[666])
    assert response.status_code == 404


def test_set_users_to_line(
    line_client: TestClient,
    user_schemas_state: List[ReadUserSchema],
    line_schemas_state: List[ReadLineSchema],
):
    user_ids = [x.id_ for x in user_schemas_state]
    response = line_client.post(
        f"/api/v1/lines/{line_schemas_state[0].id_}/set_users", json=user_ids
    )
    assert response.status_code == 200
    line_response: List[dict] = response.json()
    assert ReadLineSchema(**line_response).json(
        exclude={"id_", "created_at", "updated_at"}
    ) == line_schemas_state[0].json(exclude={"id_", "created_at", "updated_at"})


def test_set_users_to_line_user_not_found(
    line_client: TestClient, line_schemas_state: List[ReadLineSchema]
):
    response = line_client.post(
        f"/api/v1/lines/{line_schemas_state[0].id_}/set_users", json=[666]
    )
    assert response.status_code == 404


def test_set_users_to_line_line_not_found(line_client: TestClient):
    response = line_client.post("/api/v1/lines/666/set_users", json=[666])
    assert response.status_code == 404
