from pymongo import MongoClient

from src.config import get_config

settings = get_config()

_client = MongoClient(settings.mongo_url)


def get_mongo_client() -> MongoClient:
    return _client


def get_mongo_db_name() -> str:
    return settings.mongo_database_name
