from typing import Generator

from sqlalchemy import create_engine
from sqlalchemy.engine import Engine
from sqlalchemy.orm import scoped_session, sessionmaker

from src.config import get_config

engine: Engine = create_engine(get_config().database_url, pool_pre_ping=True)
ScopedSession = scoped_session(
    sessionmaker(
        autoflush=False,
        bind=engine,
        autocommit=True,
        expire_on_commit=False,
    )
)


def get_session() -> Generator[scoped_session, None, None]:
    yield ScopedSession
