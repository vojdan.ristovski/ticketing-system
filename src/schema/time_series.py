from datetime import datetime

from schemas.andon.base import BaseReadSchema
from schemas.andon.enums import SeriesStatusEnum


class ReadTimeSeriesSchema(BaseReadSchema):
    line_id: int
    status: SeriesStatusEnum
    timestamp: datetime

    class Config:
        orm_mode = True
