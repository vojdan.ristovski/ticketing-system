import enum
from datetime import datetime
from typing import Optional

from pydantic import BaseModel, Field
from schemas.andon.users import ReadUserSchema


class LineStatusEnum(enum.Enum):
    ACTIVE = "active"
    INACTIVE = "inactive"


class BaseStagingSchema(BaseModel):
    method: str
    document: dict
    timestamp: datetime = Field(default_factory=datetime.utcnow)
    user: Optional[ReadUserSchema]


class LineStatus(BaseModel):
    method: str
    line_id: int
    timestamp: datetime = Field(default_factory=datetime.utcnow)
    status: LineStatusEnum

    class Config:
        use_enum_values = True
