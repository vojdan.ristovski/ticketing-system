from datetime import datetime
from typing import Any, List, Optional

from fastapi import Query
from schemas.andon.enums import PriorityEnum, StatusEnum, TicketTypeEnum


class QueryTicketSchema:
    def __init__(
        self,
        reason_id: Optional[int] = None,
        status: Optional[List[StatusEnum]] = Query(None),
        ticket_type: Optional[List[TicketTypeEnum]] = Query(None),
        priority: Optional[List[PriorityEnum]] = Query(None),
        corrective_action: Optional[str] = None,
        issue_description: Optional[str] = None,
        component_id: Optional[str] = None,
        product_id: Optional[int] = None,
        datetime_from: Optional[datetime] = None,
        datetime_to: Optional[datetime] = None,
        approved_by: Optional[int] = None,
        line_id: Optional[int] = None,
    ):
        self.reason_id = reason_id
        self.status = status
        self.ticket_type = ticket_type
        self.priority = priority
        self.corrective_action = corrective_action
        self.issue_description = issue_description
        self.component_id = component_id
        self.product_id = product_id
        self.datetime_from = datetime_from
        self.datetime_to = datetime_to
        self.approved_by = approved_by
        self.line_id = line_id

    def dict(self) -> dict[str, Any]:
        d = {
            "reason_id": self.reason_id,
            "status": self.status,
            "ticket_type": self.ticket_type,
            "priority": self.priority,
            "corrective_action": self.corrective_action,
            "issue_description": self.issue_description,
            "component_id": self.component_id,
            "product_id": self.product_id,
            "datetime_from": self.datetime_from,
            "datetime_to": self.datetime_to,
            "approved_by": self.approved_by,
            "line_id": self.line_id,
        }
        return {k: v for k, v in d.items() if v is not None}
