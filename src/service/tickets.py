from asyncio.log import logger
from datetime import datetime
from enum import Enum
from typing import Any, List, Optional, Tuple

import fastapi
from celery import Celery
from fastapi import Depends, HTTPException, status
from pymongo import MongoClient
from pymongo.collection import Collection
from schemas.andon.base import Page
from schemas.andon.enums import StatusEnum, TicketTypeEnum
from schemas.andon.tickets import (
    CreateTicketSchema,
    ReadMachineSchema,
    ReadTicketSchema,
    UpdateTicketSchema,
)
from schemas.andon.users import ReadUserSchema
from sqlalchemy.exc import DatabaseError, IntegrityError
from sqlalchemy.orm import Session

from src.config import get_config
from src.const import (
    TRIGGER_APPROVED_TICKET_TASKNAME,
    TRIGGER_CLOSED_TICKET_TASKNAME,
    TRIGGER_EXPORT_TICKETS,
    TRIGGER_FORWARDED_TICKET_TASKNAME,
    TRIGGER_MATERIAL_NOT_AVAILABLE_TASKNAME,
    TRIGGER_NEW_TICKET_TASKNAME,
    TRIGGER_PRIORITIZED_TICKET_TASKNAME,
)
from src.db.mongo import get_mongo_client
from src.db.session import ScopedSession
from src.models.enums import OrderDirection
from src.models.tickets import Ticket, TimeSeries
from src.repository.lines import LineRepository, get_line_repository
from src.repository.machines import MachineRepository, get_machine_repository
from src.repository.tickets import TicketRepository, get_ticket_repository
from src.repository.time_series import TimeSeriesRepository, get_time_series_repository
from src.repository.users import UserRepository, get_user_repository
from src.schema.tickets import QueryTicketSchema
from src.schema.warehouse import BaseStagingSchema
from src.service.base import BaseService
from src.worker.main import get_worker


class TicketService(
    BaseService[
        Ticket,
        CreateTicketSchema,
        UpdateTicketSchema,
        ReadTicketSchema,
        TicketRepository,
    ]
):
    _model = Ticket
    _create_schema = CreateTicketSchema  # type: ignore
    _update_schema = UpdateTicketSchema  # type: ignore
    _return_schema = ReadTicketSchema  # type: ignore
    _collection = Collection

    def __init__(
        self,
        ticket_repository: TicketRepository,
        machine_repository: MachineRepository,
        user_repository: UserRepository,
        time_series_repository: TimeSeriesRepository,
        line_repository: LineRepository,
        worker: Celery,
        client: MongoClient,
    ):
        super().__init__(ticket_repository)
        self.machine_repository = machine_repository
        self.user_repository = user_repository
        self._worker = worker
        self.time_series_repository = time_series_repository
        self.line_repository = line_repository

        mongodb = client[get_config().mongo_database_name]
        collection = mongodb["tickets"]
        self._collection = collection

    def list_entries(
        self,
        page_number,
        page_size,
        order_by: Optional[List[Enum]] = None,
        query: Optional[QueryTicketSchema] = None,
    ) -> Page[ReadTicketSchema]:
        if order_by is None:
            order_by = []
        try:
            order_by_value = [item.value for item in order_by]
            if query:
                where: List[Tuple[str, Any]] = [(k, v) for k, v in query.dict().items()]
            else:
                where = []

            with ScopedSession() as active_session:
                with active_session.begin():
                    entries = self._repository.find_many_by_attrs(
                        page_number,
                        page_size,
                        *where,
                        session=active_session,
                        order_by=order_by_value,
                    )
                    q: List[Tuple[str, any]] = [
                        (k, v) for k, v in query.dict().items()
                    ]  # type: ignore

                    total = self._repository.count_by_attrs(*q, session=active_session)
                    return Page(
                        content=[
                            self._return_schema.from_orm(entry) for entry in entries
                        ],
                        total=total,
                    )
        except DatabaseError as e:
            logger.debug("Error listing entries %s:", type(self._model), exc_info=e)
            raise HTTPException(
                status_code=fastapi.status.HTTP_400_BAD_REQUEST,
                detail=f"There was an error fetching {type(self._model)}.",
            )

    def export_to_csv(self, user_id: int) -> None:
        with ScopedSession() as active_session:
            with active_session.begin():
                user = UserRepository().find_one_by_id(
                    id_=user_id, session=active_session
                )
                if not user:
                    raise HTTPException(
                        status_code=status.HTTP_404_NOT_FOUND,
                        detail=f"User with id={user_id} does not exist!",
                    )
                if not user.email:
                    raise HTTPException(
                        status_code=status.HTTP_400_BAD_REQUEST,
                        detail=f"User with id={user_id} does not have an email address!",
                    )
                self._worker.send_task(TRIGGER_EXPORT_TICKETS, [user.email])

    def create(self, ticket_in: CreateTicketSchema) -> ReadTicketSchema:
        try:
            ticket_object = Ticket(
                **ticket_in.dict(
                    exclude_unset=True,
                    exclude={"machine_ids", "created_by_id", "created_by_card_id"},
                )
            )
            with ScopedSession() as active_session:
                with active_session.begin():
                    created_by = self.user_repository.find_one_by_attr(
                        "card_id", ticket_in.created_by_card_id, active_session
                    )
                    if created_by is None:
                        raise HTTPException(
                            status_code=fastapi.status.HTTP_404_NOT_FOUND,
                            detail=(
                                "User with card_id="
                                f"{ticket_in.created_by_card_id} not found"
                            ),
                        )
                    created_by_id = created_by.id_
                    ticket_object.created_by_id = created_by_id
                    ticket_object.created_by = created_by
                    ticket: Ticket = self._repository.insert_one(
                        ticket_object, session=active_session
                    )
                    if not ticket.id_ or created_by_id is None:
                        raise HTTPException(
                            status_code=fastapi.status.HTTP_409_CONFLICT,
                            detail="There was an error creating ticket",
                        )
                    if ticket_in.machine_ids:
                        for machine_id in ticket_in.machine_ids:
                            self.machine_repository.insert_ticket_on_machine(
                                ticket.id_, machine_id, active_session
                            )
                    self._repository.insert_handled_by_on_ticket(
                        user_id=created_by_id,
                        ticket_id=ticket.id_,
                        session=active_session,
                    )
                    ticket_schema = self._return_schema.from_orm(ticket)
                    machines = self.machine_repository.get_machines_by_ticket_id(
                        ticket.id_, active_session
                    )
                    for machine in machines:
                        ticket_schema.machines.append(
                            ReadMachineSchema.from_orm(machine)
                        )
                    time_series_object = TimeSeries(
                        line_id=ticket_in.line_id,
                        status=ticket_in.priority.name,
                    )
                    self.time_series_repository.insert_one(
                        time_series_object, session=active_session
                    )
                    self._worker.send_task(TRIGGER_NEW_TICKET_TASKNAME, [ticket_schema])
                    self._collection.insert_one(
                        BaseStagingSchema(
                            method="create_ticket",
                            document=ticket_schema.dict(),
                            user=ReadUserSchema.from_orm(created_by),
                        ).dict()
                    )
                    return ticket_schema
        except IntegrityError as e:
            raise HTTPException(
                status_code=fastapi.status.HTTP_409_CONFLICT,
                detail=e.orig.diag.message_detail,
            )

    def _update_ticket(
        self,
        ticket_id: int,
        ticket_in: UpdateTicketSchema,
        active_session: Session,
    ):
        """
        Creates a ticket from the database and returns the schema object
        """
        ticket_dict = ticket_in.dict(exclude_unset=True)
        persisted_by_id = self._repository.find_one_by_id(
            ticket_id, session=active_session
        )
        if not persisted_by_id:
            raise HTTPException(
                status_code=fastapi.status.HTTP_404_NOT_FOUND,
                detail=f"Ticket with id={ticket_id} is not found",
            )
        ticket = self._repository.update_one_by_id(
            ticket_id, ticket_dict, session=active_session
        )
        ticket_schema = self._return_schema.from_orm(ticket)
        return ticket_schema

    def update(
        self,
        ticket_id: int,
        ticket_in: UpdateTicketSchema,
        card_id: str = Optional[None],
    ) -> ReadTicketSchema:
        try:
            with ScopedSession() as active_session:
                with active_session.begin():
                    ticket_schema = None
                    if ticket_in.corrective_action or ticket_in.issue_description:
                        if not card_id:
                            raise HTTPException(
                                status_code=fastapi.status.HTTP_400_BAD_REQUEST,
                                detail="""
                                Card id is required to update ticket's
                                corrective action or issue description
                                """,
                            )
                        user = self.user_repository.find_one_by_attr(
                            "card_id", card_id, session=active_session
                        )
                        if not user or user.id_ is None:
                            raise HTTPException(
                                status_code=fastapi.status.HTTP_404_NOT_FOUND,
                                detail=f"User with card id={card_id} not found",
                            )
                        self._repository.insert_handled_by_on_ticket(
                            user.id_, ticket_id, active_session
                        )
                        ticket_schema = self._update_ticket(
                            ticket_id, ticket_in, active_session
                        )
                        self._collection.insert_one(
                            BaseStagingSchema(
                                method="update_ticket",
                                document=ticket_schema.dict(),
                                user=ReadUserSchema.from_orm(user),
                            ).dict()
                        )
                    else:
                        ticket_schema = self._update_ticket(
                            ticket_id, ticket_in, active_session
                        )
                        self._collection.insert_one(
                            BaseStagingSchema(
                                method="update_ticket",
                                document=ticket_schema.dict(),
                            ).dict()
                        )
                    return ticket_schema
        except IntegrityError as e:
            raise HTTPException(
                status_code=fastapi.status.HTTP_404_NOT_FOUND,
                detail=str(e.orig.diag.message_detail),
            )

    def close(
        self,
        ticket_id: int,
        card_id: str,
        corrective_action: Optional[str] = "",
        issue_description: Optional[str] = "",
    ) -> ReadTicketSchema:
        """
        Closes the ticket

        params:
            ticket_id: id of the ticket to close
            user_id: id of the user that closed the ticket
            corrective_action: the corrective action that was taken
        """
        try:
            with ScopedSession() as active_session:
                with active_session.begin():
                    persisted_ticket = self._repository.find_one_by_id(
                        ticket_id, session=active_session
                    )
                    if not persisted_ticket:
                        raise HTTPException(
                            status_code=fastapi.status.HTTP_404_NOT_FOUND,
                            detail=f"Ticket with id={ticket_id} not found",
                        )
                    user = self.user_repository.find_one_by_attr(
                        "card_id", card_id, session=active_session
                    )
                    if user is None:
                        raise HTTPException(
                            status_code=fastapi.status.HTTP_404_NOT_FOUND,
                            detail=f"User with card id={card_id} not found",
                        )
                    # if the admin role is with id=1, then
                    # if 1 not in r.id_ for r in user.roles
                    if "ADMIN" not in (r.name for r in user.roles) and user.id_ not in (
                        u.user.id_ for u in persisted_ticket.handled_by
                    ):
                        raise HTTPException(
                            status_code=fastapi.status.HTTP_403_FORBIDDEN,
                            detail=(
                                f"User with id={user.id_}"
                                "is not allowed to close this ticket"
                            ),
                        )

                    ticket = self._repository.update_one_by_id(
                        ticket_id,
                        {
                            "status": StatusEnum.CLOSED,
                            "corrective_action": corrective_action,
                            "issue_description": issue_description,
                            "closed_by_id": user.id_,
                            "closed_at": datetime.now(),
                        },
                        session=active_session,
                    )
                    ticket_schema = self._return_schema.from_orm(ticket)
                    self._collection.insert_one(
                        BaseStagingSchema(
                            method="close_ticket",
                            document=ticket_schema.dict(),
                            user=ReadUserSchema.from_orm(user),
                        ).dict()
                    )
                    return ticket_schema
        except IntegrityError as e:
            raise HTTPException(
                status_code=fastapi.status.HTTP_404_NOT_FOUND,
                detail=str(e.orig.diag.message_detail),
            )

    def accept(self, ticket_id: int, card_id: str) -> ReadTicketSchema:
        """
        Accepts the ticket

        params:
            ticket_id: id of the ticket to accept
            user_id: id of the user that accepted the ticket
        """
        try:
            with ScopedSession() as active_session:
                with active_session.begin():
                    persisted_ticket = self._repository.find_one_by_id(
                        ticket_id, session=active_session
                    )
                    if not persisted_ticket:
                        raise HTTPException(
                            status_code=fastapi.status.HTTP_404_NOT_FOUND,
                            detail=f"Ticket with id={ticket_id} is not found",
                        )
                    user = self.user_repository.find_one_by_attr(
                        "card_id", card_id, session=active_session
                    )
                    if not user or user.id_ is None:
                        raise HTTPException(
                            status_code=fastapi.status.HTTP_404_NOT_FOUND,
                            detail=f"User with card id={card_id} not found",
                        )
                    self._repository.insert_handled_by_on_ticket(
                        user.id_, ticket_id, active_session
                    )
                    ticket = self._repository.update_one_by_id(
                        ticket_id,
                        {
                            "status": StatusEnum.PENDING,
                        },
                        session=active_session,
                    )
                    ticket_schema = self._return_schema.from_orm(ticket)
                    self._collection.insert_one(
                        BaseStagingSchema(
                            method="accept_ticket",
                            document=ticket_schema.dict(),
                            user=ReadUserSchema.from_orm(user),
                        ).dict()
                    )
                    return ticket_schema
        except IntegrityError as e:
            raise HTTPException(
                status_code=fastapi.status.HTTP_404_NOT_FOUND,
                detail=str(e.orig.diag.message_detail),
            )

    def forward(
        self, ticket_id: int, from_card_id: str, to_card_id: str
    ) -> ReadTicketSchema:
        """
        Forwards the ticket to the next user

        params:
            ticket_id: id of the ticket to forward
            from_user_id: str of the user's card id that forwarded the ticket
            to_user_id: str of the user's card id that the ticket is forwarded to
        """
        try:
            with ScopedSession() as active_session:
                with active_session.begin():
                    persisted_ticket = self._repository.find_one_by_id(
                        ticket_id, session=active_session
                    )
                    if not persisted_ticket:
                        raise HTTPException(
                            status_code=fastapi.status.HTTP_404_NOT_FOUND,
                            detail=f"Ticket with id={ticket_id} not found",
                        )
                    from_user = self.user_repository.find_one_by_attr(
                        "card_id", from_card_id, session=active_session
                    )
                    if not from_user or from_user.id_ is None:
                        raise HTTPException(
                            status_code=fastapi.status.HTTP_404_NOT_FOUND,
                            detail=f"User with card id={from_card_id} not found",
                        )
                    to_user = self.user_repository.find_one_by_attr(
                        "card_id", to_card_id, session=active_session
                    )
                    if not to_user or to_user.id_ is None:
                        raise HTTPException(
                            status_code=fastapi.status.HTTP_404_NOT_FOUND,
                            detail=f"User with card id={to_card_id} not found",
                        )
                    if from_user.id_ not in (
                        u.user.id_ for u in persisted_ticket.handled_by
                    ):
                        raise HTTPException(
                            status_code=fastapi.status.HTTP_403_FORBIDDEN,
                            detail=(
                                f"User with id={from_user.id_}"
                                "is not allowed to forward this ticket"
                            ),
                        )

                    ticket = self._repository.update_one_by_id(
                        ticket_id,
                        {
                            "status": StatusEnum.FORWARDED,
                        },
                        session=active_session,
                    )
                    self._repository.update_handled_by_on_ticket(
                        from_user.id_,
                        ticket_id,
                        {"forwarded_on": datetime.now()},
                        active_session,
                    )
                    self._repository.insert_forwarded_to_on_ticket(
                        from_user_id=from_user.id_,
                        to_user_id=to_user.id_,
                        ticket_id=ticket_id,
                        session=active_session,
                    )
                    ticket_schema = self._return_schema.from_orm(ticket)
                    self._worker.send_task(
                        TRIGGER_FORWARDED_TICKET_TASKNAME,
                        [ticket_schema, from_user, to_user],
                    )
                    self._collection.insert_one(
                        BaseStagingSchema(
                            method="forward_ticket",
                            document=ticket_schema.dict(),
                            user=ReadUserSchema.from_orm(from_user),
                        ).dict()
                    )
                    return ticket_schema
        except IntegrityError as e:
            raise HTTPException(
                status_code=fastapi.status.HTTP_404_NOT_FOUND,
                detail=str(e.orig.diag.message_detail),
            )

    def forward_to_on_call_engineer(
        self, ticket_id: int, from_card_id: str
    ) -> ReadTicketSchema:
        """
        Forwards the ticket to the on call engineer

        params:
            ticket_id: id of the ticket to forward
            from_user_id: str of the user's card id that forwarded the ticket
        """
        try:
            with ScopedSession() as active_session:
                with active_session.begin():
                    persisted_ticket = self._repository.find_one_by_id(
                        ticket_id, session=active_session
                    )
                    if not persisted_ticket:
                        raise HTTPException(
                            status_code=fastapi.status.HTTP_404_NOT_FOUND,
                            detail=f"Ticket with id={ticket_id} not found",
                        )
                    from_user = self.user_repository.find_one_by_attr(
                        "card_id", from_card_id, session=active_session
                    )
                    if not from_user:
                        raise HTTPException(
                            status_code=fastapi.status.HTTP_404_NOT_FOUND,
                            detail=f"User with card id={from_card_id} not found",
                        )

                    if from_user.id_ not in (
                        u.user.id_ for u in persisted_ticket.handled_by
                    ):
                        raise HTTPException(
                            status_code=fastapi.status.HTTP_403_FORBIDDEN,
                            detail=(
                                f"User with id={from_user.id_}"
                                "is not allowed to forward this ticket"
                            ),
                        )

                    ticket = self._repository.update_one_by_id(
                        ticket_id,
                        {
                            "status": StatusEnum.FORWARDED,
                        },
                        session=active_session,
                    )
                    ticket_schema = self._return_schema.from_orm(ticket)
                    self._collection.insert_one(
                        BaseStagingSchema(
                            method="foward_ticket_to_on_call_engineer",
                            document=ticket_schema.dict(),
                            user=ReadUserSchema.from_orm(from_user),
                        ).dict()
                    )
                    return ticket_schema
        except IntegrityError as e:
            raise HTTPException(
                status_code=fastapi.status.HTTP_404_NOT_FOUND,
                detail=str(e.orig.diag.message_detail),
            )

    def prioritize(self, ticket_id: int, card_id: str) -> ReadTicketSchema:
        """
        Sets the ticket status as prioritized

        params:
            ticket_id: id of the ticket to forward
            user_id: id of the user that forwarded the ticket
        """
        try:
            with ScopedSession() as active_session:
                with active_session.begin():
                    user = self.user_repository.find_one_by_attr(
                        "card_id", card_id, session=active_session
                    )
                    if not user:
                        raise HTTPException(
                            status_code=fastapi.status.HTTP_404_NOT_FOUND,
                            detail=f"User with card_id={card_id} is not found",
                        )

                    ticket_schema = self._update_ticket(
                        ticket_id,
                        UpdateTicketSchema(status=StatusEnum.PRIORITY),
                        active_session,
                    )
                    self._worker.send_task(
                        TRIGGER_PRIORITIZED_TICKET_TASKNAME, [ticket_schema]
                    )
                    self._collection.insert_one(
                        BaseStagingSchema(
                            method="prioritize_ticket",
                            document=ticket_schema.dict(),
                            user=ReadUserSchema.from_orm(user),
                        ).dict()
                    )
                    return ticket_schema
        except IntegrityError as e:
            raise HTTPException(
                status_code=fastapi.status.HTTP_404_NOT_FOUND,
                detail=str(e.orig.diag.message_detail),
            )

    def pause(self, ticket_id: int, card_id: str) -> ReadTicketSchema:
        """
        Sets the ticket status as paused

        params:
            ticket_id: id of the ticket to forward
            user_id: id of the user that forwarded the ticket
        """
        try:
            with ScopedSession() as active_session:
                with active_session.begin():
                    user = self.user_repository.find_one_by_attr(
                        "card_id", card_id, session=active_session
                    )
                    if not user:
                        raise HTTPException(
                            status_code=fastapi.status.HTTP_404_NOT_FOUND,
                            detail=f"User with card_id={card_id} is not found",
                        )

                    ticket_schema = self._update_ticket(
                        ticket_id,
                        UpdateTicketSchema(status=StatusEnum.ON_HOLD),
                        active_session,
                    )

                    self._collection.insert_one(
                        BaseStagingSchema(
                            method="pause_ticket",
                            document=ticket_schema.dict(),
                            user=ReadUserSchema.from_orm(user),
                        ).dict()
                    )
                    return ticket_schema
        except IntegrityError as e:
            raise HTTPException(
                status_code=fastapi.status.HTTP_404_NOT_FOUND,
                detail=str(e.orig.diag.message_detail),
            )

    def material_not_available(self, ticket_id: int, card_id: str) -> ReadTicketSchema:
        """
        Sets the ticket status as material not available

        params:
            ticket_id: id of the ticket to forward
            user_id: id of the user that forwarded the ticket
        """
        try:
            with ScopedSession() as active_session:
                with active_session.begin():
                    user = self.user_repository.find_one_by_attr(
                        "card_id", card_id, session=active_session
                    )
                    if not user:
                        raise HTTPException(
                            status_code=fastapi.status.HTTP_404_NOT_FOUND,
                            detail=f"User with card_id={card_id} is not found",
                        )

                    ticket_schema = self._update_ticket(
                        ticket_id,
                        UpdateTicketSchema(status=StatusEnum.MATERIAL_NOT_AVAILABLE),
                        active_session,
                    )
                    self._worker.send_task(
                        TRIGGER_MATERIAL_NOT_AVAILABLE_TASKNAME, [ticket_schema]
                    )
                    self._collection.insert_one(
                        BaseStagingSchema(
                            method="material_not_available",
                            document=ticket_schema.dict(),
                            user=ReadUserSchema.from_orm(user),
                        ).dict()
                    )
                    return ticket_schema
        except IntegrityError as e:
            raise HTTPException(
                status_code=fastapi.status.HTTP_404_NOT_FOUND,
                detail=str(e.orig.diag.message_detail),
            )

    def approve_planned_action(
        self,
        ticket_id: int,
        card_id: str,
        datetime_from: datetime,
        datetime_to: datetime,
    ) -> ReadTicketSchema:
        """
        Sets the planned action as approved

        params:
            ticket_id: id of the ticket to forward
            user_id: id of the user that forwarded the ticket
        """
        try:
            with ScopedSession() as active_session:
                with active_session.begin():
                    user = self.user_repository.find_one_by_attr(
                        "card_id", card_id, session=active_session
                    )
                    if not user:
                        raise HTTPException(
                            status_code=fastapi.status.HTTP_404_NOT_FOUND,
                            detail=f"User with card_id={card_id} is not found",
                        )
                    ticket_schema = self._update_ticket(
                        ticket_id,
                        UpdateTicketSchema(
                            status=StatusEnum.APPROVED,
                            datetime_from=datetime_from,
                            datetime_to=datetime_to,
                            approved_by_id=user.id_,
                        ),
                        active_session,
                    )
                    self._worker.send_task(
                        TRIGGER_APPROVED_TICKET_TASKNAME, [ticket_schema]
                    )
                    self._collection.insert_one(
                        BaseStagingSchema(
                            method="approve_planned_action",
                            document=ticket_schema.dict(),
                            user=ReadUserSchema.from_orm(user),
                        ).dict()
                    )
                    return ticket_schema
        except IntegrityError as e:
            raise HTTPException(
                status_code=fastapi.status.HTTP_404_NOT_FOUND,
                detail=str(e.orig.diag.message_detail),
            )

    def reject_planned_action(self, ticket_id: int, card_id: str) -> ReadTicketSchema:
        """
        Sets the planned action as declined

        params:
            ticket_id: id of the ticket to forward
            user_id: id of the user that forwarded the ticket
        """
        try:
            with ScopedSession() as active_session:
                with active_session.begin():
                    user = self.user_repository.find_one_by_attr(
                        "card_id", card_id, session=active_session
                    )
                    if not user:
                        raise HTTPException(
                            status_code=fastapi.status.HTTP_404_NOT_FOUND,
                            detail=f"User with card_id={card_id} is not found",
                        )

                    ticket_schema = self._update_ticket(
                        ticket_id,
                        UpdateTicketSchema(status=StatusEnum.REJECTED),
                        active_session,
                    )
                    self._worker.send_task(
                        TRIGGER_CLOSED_TICKET_TASKNAME, [ticket_schema]
                    )
                    self._collection.insert_one(
                        BaseStagingSchema(
                            method="reject_planned_action",
                            document=ticket_schema.dict(),
                            user=ReadUserSchema.from_orm(user),
                        ).dict()
                    )
                    return ticket_schema
        except IntegrityError as e:
            raise HTTPException(
                status_code=fastapi.status.HTTP_404_NOT_FOUND,
                detail=str(e.orig.diag.message_detail),
            )

    def get_by_line_id(
        self,
        line_id: int,
        page: int = 0,
        page_size: int = 10,
        status: Optional[List[StatusEnum]] = None,
        ticket_type: Optional[List[TicketTypeEnum]] = None,
        reason_id: Optional[int] = None,
        order_by: Optional[List[Enum]] = None,
        order: OrderDirection = None,
        start_date: Optional[datetime] = datetime.min,
        end_date: Optional[datetime] = datetime.max,
    ) -> Page[ReadTicketSchema]:
        status = status or []
        ticket_type = ticket_type or []
        order = order or OrderDirection.ASCENDING
        order_by = order_by or ["id_"]
        try:
            order_by_value = [item.value for item in order_by]
            with ScopedSession() as active_session:
                with active_session.begin():
                    query: List[Tuple[str, Any]] = [
                        ("line_id", line_id),
                    ]
                    if status:
                        query.append(("status", status))
                    if ticket_type:
                        query.append(("ticket_type", ticket_type))
                    if reason_id:
                        query.append(("reason_id", reason_id))
                    entries = self._repository.find_many_by_attrs(
                        page,
                        page_size,
                        *query,
                        session=active_session,
                        order_by=order_by_value,
                        order=order,
                        start_date=start_date,
                        end_date=end_date,
                    )
                    if not entries:
                        line = self.line_repository.find_one_by_id(
                            id_=line_id, session=active_session
                        )
                        if not line:
                            raise HTTPException(
                                status_code=fastapi.status.HTTP_404_NOT_FOUND,
                                detail=f"Line with id={line_id} does not exist",
                            )
                        # When no tickets found return empty array
                        return Page(content=[], total=0)
                    total = self._repository.count_by_attrs(
                        *query,
                        session=active_session,
                        start_date=start_date,
                        end_date=end_date,
                    )
                    return Page(
                        content=[
                            self._return_schema.from_orm(entry) for entry in entries
                        ],
                        total=total,
                    )
        except DatabaseError as e:
            logger.debug("Error listing entries %s:", type(self._model), exc_info=e)
            raise HTTPException(
                status_code=fastapi.status.HTTP_400_BAD_REQUEST,
                detail=f"There was an error fetching {type(self._model)}.",
            )

    def find_by_machine_id(
        self,
        machine_id: int,
        page: int = 0,
        page_size: int = 10,
        status: Optional[List[StatusEnum]] = None,
        order_by: Optional[List[Enum]] = None,
        order: OrderDirection = OrderDirection.ASCENDING,
    ) -> Page[ReadTicketSchema]:
        if status is None:
            status = []
        try:
            with ScopedSession() as active_session:
                order_by_value = [item.value for item in order_by]
                with active_session.begin():
                    entries = self._repository.find_by_machine_id(
                        machine_id,
                        page=page,
                        page_size=page_size,
                        session=active_session,
                        status=status,
                        order_by=order_by_value,
                        order=order,
                    )
                    if not entries:
                        machine = self.machine_repository.find_one_by_id(
                            id_=machine_id, session=active_session
                        )
                        if not machine:
                            raise HTTPException(
                                status_code=fastapi.status.HTTP_404_NOT_FOUND,
                                detail=f"Machine with id={machine_id} does not exist",
                            )
                        # When no tickets found return empty array
                        return Page(content=[], total=0)
                    return Page(
                        content=[
                            self._return_schema.from_orm(entry) for entry in entries
                        ],
                        total=self._repository.count_by_machine_id(
                            machine_id, session=active_session
                        ),
                    )
        except DatabaseError as e:
            logger.debug("Error listing entries %s:", type(self._model), exc_info=e)
            raise HTTPException(
                status_code=fastapi.status.HTTP_400_BAD_REQUEST,
                detail=f"There was an error fetching {type(self._model)}.",
            )

    def find_by_user_id(
        self,
        user_id: int,
        page: int = 0,
        page_size: int = 10,
    ) -> Page[ReadTicketSchema]:
        try:
            with ScopedSession() as active_session:
                with active_session.begin():
                    entries = self._repository.find_by_user_id(
                        user_id,
                        page=page,
                        page_size=page_size,
                        session=active_session,
                    )
                    if not entries:
                        user = self.user_repository.find_one_by_id(
                            id_=user_id, session=active_session
                        )
                        if not user:
                            raise HTTPException(
                                status_code=fastapi.status.HTTP_404_NOT_FOUND,
                                detail=f"User with id={user_id} does not exist",
                            )
                        raise HTTPException(
                            status_code=fastapi.status.HTTP_404_NOT_FOUND,
                            detail=f"No tickets found for user with id={user_id}",
                        )
                    return Page(
                        content=[
                            self._return_schema.from_orm(entry) for entry in entries
                        ],
                        total=self._repository.count_by_user_id(
                            user_id, session=active_session
                        ),
                    )
        except DatabaseError as e:
            logger.debug("Error listing entries %s:", type(self._model), exc_info=e)
            raise HTTPException(
                status_code=fastapi.status.HTTP_400_BAD_REQUEST,
                detail=f"There was an error fetching {type(self._model)}.",
            )


def get_ticket_service(
    ticket_repository: TicketRepository = Depends(get_ticket_repository),
    machine_repository: MachineRepository = Depends(get_machine_repository),
    user_repository: UserRepository = Depends(get_user_repository),
    time_series_repository: TimeSeriesRepository = Depends(get_time_series_repository),
    line_repository: LineRepository = Depends(get_line_repository),
    worker: Celery = Depends(get_worker),
    client: MongoClient = Depends(get_mongo_client),
) -> TicketService:
    return TicketService(
        ticket_repository=ticket_repository,
        machine_repository=machine_repository,
        user_repository=user_repository,
        time_series_repository=time_series_repository,
        line_repository=line_repository,
        worker=worker,
        client=client,
    )
