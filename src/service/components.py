import logging

from fastapi import Depends, HTTPException, status
from pymongo import MongoClient
from pymongo.collection import Collection
from schemas.andon.component import (
    CreateComponentSchema,
    ReadComponentSchema,
    UpdateComponentSchema,
)
from sqlalchemy.exc import IntegrityError

from src.config import get_config
from src.db.mongo import get_mongo_client
from src.db.session import ScopedSession
from src.models.tickets import Component
from src.repository.components import ComponentRepository, get_component_repository
from src.schema.warehouse import BaseStagingSchema
from src.service.base import BaseService

logger = logging.getLogger(__name__)


class ComponentService(
    BaseService[
        Component,
        CreateComponentSchema,
        UpdateComponentSchema,
        ReadComponentSchema,
        ComponentRepository,
    ]
):
    _model = Component
    _create_schema = CreateComponentSchema  # type: ignore
    _update_schema = UpdateComponentSchema  # type: ignore
    _return_schema = ReadComponentSchema  # type: ignore
    _collection = Collection

    def __init__(self, component_repository: ComponentRepository, client: MongoClient):
        super().__init__(component_repository)
        mongodb = client[get_config().mongo_database_name]
        collection = mongodb["components"]
        self._collection = collection

    def create(self, component_in: CreateComponentSchema) -> ReadComponentSchema:
        try:
            with ScopedSession() as active_session:
                with active_session.begin():
                    component = self._repository.insert_one(
                        Component(**component_in.dict(exclude_unset=True)),
                        session=active_session,
                    )
                    component_schema = self._return_schema.from_orm(component)
                    self._collection.insert_one(
                        BaseStagingSchema(
                            method="create_component", document=component_schema.dict()
                        ).dict()
                    )
                    return component_schema
        except IntegrityError as e:
            logger.error(e)
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="Component with this name already exists",
            )


def get_component_service(
    component_repository: ComponentRepository = Depends(get_component_repository),
    client: MongoClient = Depends(get_mongo_client),
):
    return ComponentService(component_repository, client)
