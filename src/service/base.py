import abc
import logging
from enum import Enum
from typing import Generic, List, Optional, Type, TypeVar

from fastapi import HTTPException
from pydantic.main import BaseModel
from schemas.andon.base import Page
from sqlalchemy.exc import DatabaseError, IntegrityError
from starlette import status

from src.db.session import ScopedSession
from src.models.mixins import BaseDbModel
from src.repository.base import BaseRepository

ModelType = TypeVar("ModelType", bound=BaseDbModel)
CreateSchemaType = TypeVar("CreateSchemaType", bound=BaseModel)
UpdateSchemaType = TypeVar("UpdateSchemaType", bound=BaseModel)
ReturnSchemaType = TypeVar("ReturnSchemaType", bound=BaseModel)
RepositoryType = TypeVar("RepositoryType", bound=BaseRepository)

logger = logging.getLogger(__name__)


class BaseService(
    Generic[
        ModelType, CreateSchemaType, UpdateSchemaType, ReturnSchemaType, RepositoryType
    ]
):
    def __init__(self, repository: RepositoryType):
        self._repository = repository

    @property
    @abc.abstractmethod
    def _model(self) -> Type[ModelType]:
        ...

    @property
    @abc.abstractmethod
    def _create_schema(self) -> Type[CreateSchemaType]:
        ...

    @property
    @abc.abstractmethod
    def _update_schema(self) -> Type[UpdateSchemaType]:
        ...

    @property
    @abc.abstractmethod
    def _return_schema(self) -> Type[ReturnSchemaType]:
        ...

    def get_by_id(self, id_: int) -> ReturnSchemaType:
        try:
            with ScopedSession() as active_session:
                with active_session.begin():
                    entry = self._repository.find_one_by_id(id_, session=active_session)
                    if entry is None:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Entity with {id_=} does not exist",
                        )
                    return self._return_schema.from_orm(entry)
        except IntegrityError as e:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail=f"There was an error getting {self._model.__tablename__} "
                f"with id={id_} is not found. "
                f"Detail: {e.orig.diag.message_detail}",
            )

    def get_count(self) -> int:
        try:
            with ScopedSession() as active_session:
                with active_session.begin():
                    return self._repository.count(active_session)

        except DatabaseError as e:
            logger.debug("Error counting %s:", type(self._model), exc_info=e)
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail=f"There was an error counting {type(self._model)}.",
            )

    def list_entries(
        self, page_number, page_size, order_by: Optional[List[Enum]] = None
    ) -> Page[ReturnSchemaType]:
        if order_by is None:
            order_by = []
        try:
            order_by_value = [item.value for item in order_by]
            with ScopedSession() as active_session:
                with active_session.begin():
                    entries = self._repository.find_many(
                        page_number,
                        page_size,
                        session=active_session,
                        order_by=order_by_value,
                    )
                    total = self._repository.count(session=active_session)
                    return Page(
                        content=[
                            self._return_schema.from_orm(entry) for entry in entries
                        ],
                        total=total,
                    )
        except DatabaseError as e:
            logger.debug("Error listing entries %s:", type(self._model), exc_info=e)
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail=f"There was an error fetching {type(self._model)}.",
            )

    def delete(self, id_: int) -> None:
        try:
            with ScopedSession() as active_session:
                with active_session.begin():
                    entry = self._repository.find_one_by_id(id_, session=active_session)
                    if entry is None:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Entity with {id_=} does not exist",
                        )
                    self._repository.delete_one_by_id(id_, active_session)

        except DatabaseError as e:
            logger.debug("Error deleting %s:", type(self._model), exc_info=e)
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail=f"There was an error fetching {type(self._model)}.",
            )
