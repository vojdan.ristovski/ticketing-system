from asyncio.log import logger

from fastapi import Depends, HTTPException, status
from schemas.andon.auth import ChangePasswordSchema, TokenResponseSchema, TokenSchema
from sqlalchemy.exc import DatabaseError

from src.db.session import ScopedSession
from src.models.response import StatusResponseModel
from src.repository.users import UserRepository, get_user_repository
from src.utils import encode_token, get_password_hash, verify_password


class AuthService:
    def __init__(self, users_repository: UserRepository) -> None:
        self._repository = users_repository

    def login(self, username: str, password: str) -> TokenResponseSchema:
        try:
            with ScopedSession() as active_session:
                with active_session.begin():
                    user = self._repository.find_by_username(
                        username, session=active_session
                    )
                    if not user or not verify_password(password, user.password):
                        raise HTTPException(
                            status_code=status.HTTP_401_UNAUTHORIZED,
                            detail="Incorrect username or passsword",
                        )
                    token = encode_token(TokenSchema(email=user.email))
                    return TokenResponseSchema(access_token=token)

        except DatabaseError as e:
            logger.debug(
                "Error logging in user",
                exc_info=e,
            )
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="Incorrect username or password",
            )

    def change_password(
        self, change_password_in: ChangePasswordSchema
    ) -> StatusResponseModel:
        try:
            with ScopedSession() as active_session:
                with active_session.begin():
                    user = self._repository.find_by_username(
                        change_password_in.username, session=active_session
                    )
                    if not user or not user.id_:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail=(
                                "User with username "
                                f"{change_password_in.username} not found"
                            ),
                        )

                    if not verify_password(
                        change_password_in.old_password, user.password
                    ):
                        raise HTTPException(
                            status_code=status.HTTP_400_BAD_REQUEST,
                            detail="Incorrect username or password",
                        )

                    if (
                        change_password_in.new_password
                        == change_password_in.old_password
                    ):
                        raise HTTPException(
                            status_code=status.HTTP_400_BAD_REQUEST,
                            detail="Old and new password can not be the same",
                        )
                    if (
                        change_password_in.new_password
                        != change_password_in.repeat_password
                    ):
                        raise HTTPException(
                            status_code=status.HTTP_400_BAD_REQUEST,
                            detail="Passwords do not match",
                        )
                    new_password = get_password_hash(change_password_in.new_password)
                    self._repository.update_one_by_id(
                        user.id_, {"password": new_password}, session=active_session
                    )

                    return StatusResponseModel(
                        status=202, message="Successfully updated password"
                    )

        except DatabaseError as e:
            logger.debug(
                "Error changing password",
                exc_info=e,
            )
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail=(
                    "There was an error "
                    f"changing password for {change_password_in.username}"
                ),
            )


def get_auth_service(
    users_repository: UserRepository = Depends(get_user_repository),
):
    return AuthService(
        users_repository=users_repository,
    )
