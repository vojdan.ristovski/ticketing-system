import functools
from asyncio.log import logger
from datetime import datetime
from typing import Any, List, Optional

import pandas as pd
from fastapi import Depends, HTTPException, status
from schemas.andon.reports import AverageReport, Report, UserReport
from sqlalchemy.exc import DatabaseError

from src.db.session import ScopedSession, engine
from src.repository.lines import LineRepository, get_line_repository
from src.repository.machines import MachineRepository, get_machine_repository
from src.repository.reports import ReportsRepository, get_reports_repository
from src.repository.tickets import TicketRepository, get_ticket_repository
from src.repository.time_series import TimeSeriesRepository, get_time_series_repository
from src.schema.time_series import ReadTimeSeriesSchema


class ReportsService:
    def __init__(
        self,
        reports_repository: ReportsRepository,
        time_series_repository: TimeSeriesRepository,
        line_repository: LineRepository,
        ticket_repository: TicketRepository,
        machine_repository: MachineRepository,
    ) -> None:
        self._repository = reports_repository
        self._time_series_repository = time_series_repository
        self._line_repository = line_repository
        self._ticket_repository = ticket_repository
        self._machine_repository = machine_repository

    def get_time_series(self, line_id: int) -> List[ReadTimeSeriesSchema]:
        try:
            with ScopedSession() as active_session:
                with active_session.begin():
                    time_series = (
                        self._time_series_repository.get_time_series_by_line_id(
                            line_id, active_session
                        )
                    )
                    return [
                        ReadTimeSeriesSchema.from_orm(time_serie)
                        for time_serie in time_series
                    ]
        except DatabaseError as e:
            logger.debug(
                "Error fetching time_series",
                exc_info=e,
            )
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="There was an error fetching time_series",
            )

    def get_reports(self) -> List[Report]:
        try:
            with ScopedSession() as active_session:
                with active_session.begin():
                    reports = self._repository.get_reports_list(session=active_session)
                    return [Report.from_orm(report) for report in reports]
        except DatabaseError as e:
            logger.debug(
                "Error fetching reports",
                exc_info=e,
            )
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="There was an error fetching reports",
            )

    def get_average_time_to_handle_ticket(self, user_id: int):
        try:
            with ScopedSession() as active_session:
                with active_session.begin():
                    delta = self._repository.get_average_time_to_handle_ticket(
                        user_id, session=active_session
                    )
                    if delta:
                        return AverageReport(
                            average=delta.total_seconds(),
                        )
                    else:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"There was no report for user with id={user_id}.",
                        )
        except DatabaseError as e:
            logger.debug(
                "Error fetching average time to handle ticket for user_id %s:",
                user_id,
                exc_info=e,
            )
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail=f"There was an error fetching report for {user_id}.",
            )

    def get_average_reaction_time(self, user_id: int):
        try:
            with ScopedSession() as active_session:
                with active_session.begin():
                    delta = self._repository.get_average_reaction_time(
                        user_id, session=active_session
                    )
                    if delta:
                        return AverageReport(
                            average=delta.total_seconds(),
                        )
                    else:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"There was no report for user with id={user_id}.",
                        )
        except DatabaseError as e:
            logger.debug(
                "Error fetching average reaction time for user_id %s:",
                user_id,
                exc_info=e,
            )
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail=f"There was an error fetching report for {user_id}.",
            )

    def get_user_report(self, user_id: int) -> Optional[UserReport]:
        try:
            with ScopedSession() as active_session:
                with active_session.begin():
                    return UserReport(
                        **self._repository.get_user_report(
                            user_id, session=active_session
                        )
                    )
        except DatabaseError as e:
            logger.debug(
                "Error fetching user report for user_id %s:",
                user_id,
                exc_info=e,
            )
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail=f"There was an error fetching report for {user_id}.",
            )

    def get_line_reports(self) -> List[Any]:
        time_series = self._time_series_repository.get_dataframe(engine=engine)
        lines = self._line_repository.get_dataframe(engine=engine)
        users_tickets = self._ticket_repository.get_users_tickets_dataframe(
            engine=engine
        )
        tickets = self._ticket_repository.get_dataframe(engine=engine)

        df = pd.merge(lines, tickets, how="outer", left_on="id", right_on="line_id")
        df = df[["name", "status", "id_x", "id_y"]]
        df.rename(columns={"id_x": "line_id", "id_y": "ticket_id"}, inplace=True)

        opened_tickets = df.groupby("line_id")["ticket_id"].count()
        opened_tickets_count = pd.DataFrame(opened_tickets)
        opened_tickets_count.rename(
            columns={opened_tickets_count.columns[0]: "opened_tickets_count"},
            inplace=True,
        )

        active_tickets = df.groupby("line_id").apply(
            lambda group: group[
                (group["status"] != "REJECTED") & (group["status"] != "CLOSED")
            ]["ticket_id"].count()
        )
        active_tickets_count = pd.DataFrame(active_tickets)
        active_tickets_count.rename(
            columns={active_tickets_count.columns[0]: "active_tickets_count"},
            inplace=True,
        )

        forwarded_tickets = df.groupby("line_id").apply(
            lambda group: group[(group["status"] == "FORWARDED")]["ticket_id"].count()
        )
        forwarded_tickets_count = pd.DataFrame(forwarded_tickets)
        forwarded_tickets_count.rename(
            columns={forwarded_tickets_count.columns[0]: "forwarded_tickets_count"},
            inplace=True,
        )

        closed_tickets = df.groupby("line_id").apply(
            lambda group: group[(group["status"] == "CLOSED")]["ticket_id"].count()
        )
        closed_tickets_count = pd.DataFrame(closed_tickets)
        closed_tickets_count.rename(
            columns={closed_tickets_count.columns[0]: "closed_tickets_count"},
            inplace=True,
        )

        df = pd.merge(time_series, lines, how="outer", left_on="line_id", right_on="id")
        df = df[["name", "timestamp", "status", "id_y"]]
        df.rename(columns={"id_y": "line_id"}, inplace=True)
        df = df.loc[df["status"] != "NOT_RUNNING"]

        avg_time_from_last_ticket = (
            df.groupby("line_id")
            .apply(lambda group: group["timestamp"].diff().mean().total_seconds())
            .fillna(-1)
        )
        avg_time_from_last_ticket.rename("avg_time_from_last_ticket", inplace=True)
        total_time_since_last_incident = datetime.utcnow() - df.groupby("line_id").apply(
            lambda group: group[
                (group["status"] != "RUNNING") | (group["status"] != "NOT_RUNNING")
            ]["timestamp"].max()
        )
        total_time_since_last_incident = (
            total_time_since_last_incident.dt.total_seconds().fillna(-1)
        )
        total_time_since_last_incident.rename(
            "total_time_since_last_incident", inplace=True
        )

        df = pd.merge(tickets, lines, how="outer", left_on="line_id", right_on="id")
        df = df[["name", "status", "id_y", "closed_at", "created_at_x", "id_x"]]

        df.rename(
            columns={
                "id_y": "line_id",
                "created_at_x": "created_at",
                "id_x": "ticket_id",
            },
            inplace=True,
        )

        total_time_for_solving_problem = df.groupby("line_id").apply(
            lambda group: (group["closed_at"] - group["created_at"])
            .sum()
            .total_seconds()
        )
        total_time_for_solving_problem.rename(
            "total_time_for_solving_problem", inplace=True
        )
        mean_time_for_solving_problem = df.groupby("line_id").apply(
            lambda group: (group["closed_at"] - group["created_at"])
            .mean()
            .total_seconds()
        )
        mean_time_for_solving_problem.rename(
            "mean_time_for_solving_problem", inplace=True
        )

        df = pd.merge(
            df, users_tickets, how="outer", left_on="ticket_id", right_on="ticket_id"
        )

        average_reaction_time = (
            df.groupby("line_id")
            .apply(
                lambda group: (group["accepted_on"] - group["created_at"])
                .mean()
                .total_seconds()
            )
            .fillna(-1)
        )
        average_reaction_time.rename("average_reaction_time", inplace=True)
        total_reaction_time = (
            df.groupby("line_id")
            .apply(
                lambda group: (group["accepted_on"] - group["created_at"])
                .sum()
                .total_seconds()
            )
            .fillna(-1)
        )
        total_reaction_time.rename("total_reaction_time", inplace=True)
        line_names = lines.rename(columns={"id": "line_id"})[["line_id", "name"]]
        dfs = [
            line_names,
            avg_time_from_last_ticket,
            opened_tickets_count,
            active_tickets_count,
            forwarded_tickets_count,
            closed_tickets_count,
            total_time_since_last_incident,
            total_time_for_solving_problem,
            mean_time_for_solving_problem,
            average_reaction_time,
            total_reaction_time,
        ]
        df = functools.reduce(
            lambda left, right: pd.merge(left, right, on="line_id"), dfs
        )
        res = df.fillna(-1).to_dict("records")
        return res

    def get_machines_reports(self) -> List[Any]:
        """
        Get machines reports
        """
        lines = self._line_repository.get_dataframe(engine=engine)
        tickets = self._ticket_repository.get_dataframe(engine=engine)
        machines = self._machine_repository.get_dataframe(engine=engine)
        ticket_machines_df = (
            self._ticket_repository.get_tickets_machines_association_dataframe(
                engine=engine
            )
        )

        df = pd.merge(lines, tickets, how="outer", left_on="id", right_on="line_id")
        df = df[["name", "status", "id_x", "id_y"]]
        df.rename(columns={"id_x": "line_id", "id_y": "ticket_id"}, inplace=True)
        df = pd.merge(
            df,
            ticket_machines_df,
            how="right",
            left_on="ticket_id",
            right_on="ticket_id",
        )
        df = pd.merge(df, machines, how="outer", left_on="machine_id", right_on="id")
        df.drop(columns=["machine_id"], inplace=True)
        df.rename(
            columns={
                "name_x": "line_name",
                "name_y": "machine_name",
                "line_id_x": "line_id",
                "id": "machine_id",
            },
            inplace=True,
        )
        df.drop(columns=["line_id_y"], inplace=True)

        opened_tickets = df.groupby("machine_id")["ticket_id"].count()
        opened_tickets_count = pd.DataFrame(opened_tickets)
        opened_tickets_count.rename(
            columns={opened_tickets_count.columns[0]: "opened_tickets_count"},
            inplace=True,
        )

        active_tickets = df.groupby("machine_id").apply(
            lambda group: group[
                (group["status"] != "REJECTED") & (group["status"] != "CLOSED")
            ]["ticket_id"].count()
        )
        active_tickets_count = pd.DataFrame(active_tickets)
        active_tickets_count.rename(
            columns={active_tickets_count.columns[0]: "active_tickets_count"},
            inplace=True,
        )

        forwarded_tickets = df.groupby("machine_id").apply(
            lambda group: group[(group["status"] == "FORWARDED")]["ticket_id"].count()
        )
        forwarded_tickets_count = pd.DataFrame(forwarded_tickets)
        forwarded_tickets_count.rename(
            columns={forwarded_tickets_count.columns[0]: "forwarded_tickets_count"},
            inplace=True,
        )

        closed_tickets = df.groupby("machine_id").apply(
            lambda group: group[(group["status"] == "CLOSED")]["ticket_id"].count()
        )
        closed_tickets_count = pd.DataFrame(closed_tickets)
        closed_tickets_count.rename(
            columns={closed_tickets_count.columns[0]: "closed_tickets_count"},
            inplace=True,
        )
        line_names = lines.rename(columns={"id": "line_id", "name": "line_name"})[
            ["line_id", "line_name"]
        ]
        machine_names = machines.rename(
            columns={"id": "machine_id", "name": "machine_name"}
        )[["machine_id", "machine_name", "line_id"]]
        dfs = [
            machine_names,
            opened_tickets_count,
            active_tickets_count,
            forwarded_tickets_count,
            closed_tickets_count,
        ]
        df = functools.reduce(
            lambda left, right: pd.merge(left, right, on="machine_id"), dfs
        )
        df = pd.merge(df, line_names, how="left", left_on="line_id", right_on="line_id")
        df.rename(
            columns={"line_id_x": "line_id"},
            inplace=True,
        )
        res = df.fillna(-1).to_dict("records")
        return res


def get_reports_service(
    reports_repository: ReportsRepository = Depends(get_reports_repository),
    time_series_repository: TimeSeriesRepository = Depends(get_time_series_repository),
    line_repository: LineRepository = Depends(get_line_repository),
    ticket_repository: TicketRepository = Depends(get_ticket_repository),
    machine_repository: MachineRepository = Depends(get_machine_repository),
):
    return ReportsService(
        reports_repository=reports_repository,
        time_series_repository=time_series_repository,
        line_repository=line_repository,
        ticket_repository=ticket_repository,
        machine_repository=machine_repository,
    )
