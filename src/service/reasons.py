import logging

from fastapi import Depends, HTTPException, status
from pymongo import MongoClient
from pymongo.collection import Collection
from schemas.andon.reasons import (
    CreateReasonSchema,
    ReadReasonSchema,
    UpdateReasonSchema,
)
from sqlalchemy.exc import DatabaseError, IntegrityError

from src.config import get_config
from src.db.mongo import get_mongo_client
from src.db.session import ScopedSession
from src.models.tickets import Reason
from src.repository.reasons import ReasonRepository, get_reason_repository
from src.schema.warehouse import BaseStagingSchema
from src.service.base import BaseService

logger = logging.getLogger(__name__)


class ReasonService(
    BaseService[
        Reason,
        CreateReasonSchema,
        UpdateReasonSchema,
        ReadReasonSchema,
        ReasonRepository,
    ]
):
    _model = Reason
    _create_schema = CreateReasonSchema  # type: ignore
    _update_schema = UpdateReasonSchema  # type: ignore
    _return_schema = ReadReasonSchema  # type: ignore
    _collection = Collection

    def __init__(self, reason_repository: ReasonRepository, client: MongoClient):
        super().__init__(reason_repository)
        mongodb = client[get_config().mongo_database_name]
        collection = mongodb["reasons"]
        self._collection = collection

    def create(self, reason_in: CreateReasonSchema) -> ReadReasonSchema:
        try:
            with ScopedSession() as active_session:
                with active_session.begin():
                    reason = self._repository.insert_one(
                        Reason(**reason_in.dict(exclude_unset=True)),
                        session=active_session,
                    )
                    reason_schema = self._return_schema.from_orm(reason)
                    self._collection.insert_one(
                        BaseStagingSchema(
                            method="create_reason", document=reason_schema.dict()
                        ).dict()
                    )
                    return reason_schema
        except IntegrityError as e:
            logger.error(e)
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="Reason with this name already exists",
            )

    def update(self, reason_id: int, reason_in: UpdateReasonSchema) -> ReadReasonSchema:
        try:
            reason_dict = reason_in.dict(exclude_unset=True)
            with ScopedSession() as active_session:
                with active_session.begin():
                    persisted_reason = self._repository.find_one_by_id(
                        reason_id, session=active_session
                    )
                    if not persisted_reason:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Reason with id={reason_id} does not exist",
                        )
                    product = self._repository.update_one_by_id(
                        reason_id, reason_dict, session=active_session
                    )
                    reason_schema = self._return_schema.from_orm(product)
                    self._collection.insert_one(
                        BaseStagingSchema(
                            method="update_reason", document=reason_schema.dict()
                        ).dict()
                    )
                    return reason_schema
        except DatabaseError as e:
            logger.debug("Error while updating reason: %s", reason_id, exc_info=e)
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="There was an error updating a reason",
            )


def get_reason_service(
    reason_repository: ReasonRepository = Depends(get_reason_repository),
    client: MongoClient = Depends(get_mongo_client),
):
    return ReasonService(reason_repository, client)
