import logging
from typing import List

from fastapi import Depends, HTTPException
from pymongo import MongoClient
from pymongo.collection import Collection
from schemas.andon.base import Page
from schemas.andon.enums import ProductOrderByEnum
from schemas.andon.products import (
    CreateProductSchema,
    ReadProductSchema,
    UpdateProductSchema,
)
from sqlalchemy.exc import DatabaseError
from starlette import status

from src.config import get_config
from src.db.mongo import get_mongo_client
from src.db.session import ScopedSession
from src.models.tickets import Product
from src.repository.lines import LineRepository, get_line_repository
from src.repository.products import ProductRepository, get_product_repository
from src.schema.warehouse import BaseStagingSchema
from src.service.base import BaseService

logger = logging.getLogger(__name__)


class ProductService(
    BaseService[
        Product,
        CreateProductSchema,
        UpdateProductSchema,
        ReadProductSchema,
        ProductRepository,
    ]
):
    _model = Product
    _create_schema = CreateProductSchema  # type: ignore
    _update_schema = UpdateProductSchema  # type: ignore
    _return_schema = ReadProductSchema  # type: ignore
    _collection = Collection

    def __init__(
        self,
        _repository: ProductRepository,
        line_repository: LineRepository,
        client: MongoClient,
    ):
        super().__init__(_repository)
        self._line_repository = line_repository
        mongodb = client[get_config().mongo_database_name]
        collection = mongodb["products"]
        self._collection = collection

    def create(self, product_in: CreateProductSchema) -> ReadProductSchema:
        product_object = Product(
            **product_in.dict(exclude_unset=True, exclude={"line_id"})
        )
        try:
            with ScopedSession() as active_session:
                with active_session.begin():
                    for line_id in product_in.line_ids:
                        persisted_line = self._line_repository.find_one_by_id(
                            line_id, session=active_session
                        )
                        if not persisted_line:
                            raise HTTPException(
                                status_code=status.HTTP_400_BAD_REQUEST,
                                detail=f"Line with id={line_id} does not exist",
                            )
                    persisted_product = self._repository.insert_one(
                        product_object, session=active_session
                    )
                    if not persisted_product or not persisted_product.id_:
                        raise HTTPException(
                            status_code=status.HTTP_400_BAD_REQUEST,
                            detail="There was an error creating a product",
                        )
                    for line_id in product_in.line_ids:
                        self._line_repository.insert_product_on_line(
                            line_id,
                            persisted_product.id_,
                            session=active_session,
                        )
                    product_schema = self._return_schema.from_orm(persisted_product)
                    self._collection.insert_one(
                        BaseStagingSchema(
                            method="create_product", document=product_schema.dict()
                        ).dict()
                    )
                    return product_schema
        except DatabaseError as e:
            logger.debug("Error while creating product: %s", product_in, exc_info=e)
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="There was an error creating a product",
            )

    def update(
        self, product_id: int, product_in: UpdateProductSchema
    ) -> ReadProductSchema:
        try:
            product_dict = product_in.dict(exclude_unset=True)
            with ScopedSession() as active_session:
                with active_session.begin():
                    persisted_product = self._repository.find_one_by_id(
                        product_id, session=active_session
                    )
                    if not persisted_product:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Product with id={product_id} does not exist",
                        )
                    product = self._repository.update_one_by_id(
                        product_id, product_dict, session=active_session
                    )
                    product_schema = self._return_schema.from_orm(product)
                    self._collection.insert_one(
                        BaseStagingSchema(
                            method="update_product", document=product_schema.dict()
                        ).dict()
                    )
                    return product_schema
        except DatabaseError as e:
            logger.debug("Error while updating product: %s", product_in, exc_info=e)
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="There was an error updating a product",
            )

    def get_by_line_id(
        self, line_id: int, page: int, page_size: int, order_by: List[ProductOrderByEnum]
    ) -> Page[ReadProductSchema]:
        order_by_value = [item.value for item in order_by]
        with ScopedSession() as active_session:
            with active_session.begin():
                line = self._line_repository.find_one_by_id(
                    line_id, session=active_session
                )
                if not line:
                    raise HTTPException(
                        status_code=status.HTTP_404_NOT_FOUND,
                        detail=f"Line with id={line_id} does not exist",
                    )
                products = self._repository.find_by_line_id(
                    line_id, page, page_size, order_by_value, session=active_session
                )
                return Page(
                    content=[
                        self._return_schema.from_orm(product) for product in products
                    ],
                    total=0,
                )


def get_product_service(
    product_repository: ProductRepository = Depends(get_product_repository),
    line_repository: LineRepository = Depends(get_line_repository),
    client: MongoClient = Depends(get_mongo_client),
) -> ProductService:
    return ProductService(product_repository, line_repository, client)
