import logging

from fastapi import Depends, HTTPException, status
from pymongo import MongoClient
from pymongo.collection import Collection
from schemas.andon.zone import CreateZoneSchema, ReadZoneSchema, UpdateZoneSchema
from sqlalchemy.exc import DatabaseError, IntegrityError

from src.config import get_config
from src.db.mongo import get_mongo_client
from src.db.session import ScopedSession
from src.models.tickets import Zone
from src.repository.zones import ZoneRepository, get_zone_repository
from src.schema.warehouse import BaseStagingSchema
from src.service.base import BaseService

logger = logging.getLogger(__name__)


class ZoneService(
    BaseService[Zone, CreateZoneSchema, UpdateZoneSchema, ReadZoneSchema, ZoneRepository]
):
    _model = Zone
    _create_schema = CreateZoneSchema
    _update_schema = UpdateZoneSchema
    _return_schema = ReadZoneSchema
    _collection = Collection

    def __init__(self, zone_repository: ZoneRepository, client: MongoClient):
        super().__init__(zone_repository)
        mongodb = client[get_config().mongo_database_name]
        collection = mongodb["zones"]
        self._collection = collection

    def create(self, zone_in: CreateZoneSchema) -> ReadZoneSchema:
        try:
            with ScopedSession() as active_session:
                with active_session.begin():
                    zone_object = Zone(**zone_in.dict(exclude_unset=True))
                    if not zone_object.name:
                        raise HTTPException(
                            status_code=status.HTTP_409_CONFLICT,
                            detail=f"Zone has no name. name={zone_object.name}",
                        )

                    persisted = self._repository.find_one_by_attr(
                        "name", zone_object.name, session=active_session
                    )
                    if persisted is not None:
                        raise HTTPException(
                            status_code=status.HTTP_409_CONFLICT,
                            detail=f"Zone with name={zone_object.name} already exists",
                        )
                    zone = self._repository.insert_one(
                        zone_object, session=active_session
                    )
                    zone_schema = self._return_schema.from_orm(zone)
                    self._collection.insert_one(
                        BaseStagingSchema(
                            method="create_zone", document=zone_schema.dict()
                        ).dict()
                    )
                    return zone_schema
        except IntegrityError as e:
            raise HTTPException(status_code=status.HTTP_409_CONFLICT, detail=str(e))

    def update_one(self, zone_id: int, zone_in: UpdateZoneSchema) -> ReadZoneSchema:
        try:
            zone_dict = zone_in.dict(exclude_unset=True)
            with ScopedSession() as active_session:
                with active_session.begin():
                    persisted_with_id = self._repository.find_one_by_id(
                        zone_id, session=active_session
                    )
                    if persisted_with_id is None:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Zone with id={zone_in} does not exist",
                        )
                    persisted_with_name = self._repository.find_one_by_attr(
                        "name", zone_in.name, session=active_session
                    )
                    if (
                        persisted_with_name is not None
                        and persisted_with_name.id_ != zone_id
                    ):
                        raise HTTPException(
                            status_code=status.HTTP_409_CONFLICT,
                            detail=f"Zone with name={zone_in.name} already exists",
                        )
                    updated_zone = self._repository.update_one_by_id(
                        zone_id, zone_dict, session=active_session
                    )
                    zone_schema = self._return_schema.from_orm(updated_zone)
                    self._collection.insert_one(
                        BaseStagingSchema(
                            method="update_zone", document=zone_schema.dict()
                        ).dict()
                    )
                    return self._return_schema.from_orm(updated_zone)
        except DatabaseError as e:
            logger.debug("Error updating zone: %s %s", zone_id, zone_in, exc_info=e)
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="There was an error creating a zone",
            )


def get_zone_service(
    zone_repository: ZoneRepository = Depends(get_zone_repository),
    client: MongoClient = Depends(get_mongo_client),
):
    return ZoneService(zone_repository, client)
