import logging
from typing import List

import fastapi
from fastapi import Depends, HTTPException, status
from pymongo import MongoClient
from pymongo.collection import Collection
from schemas.andon.enums import SeriesStatusEnum
from schemas.andon.lines import CreateLineSchema, ReadLineSchema, UpdateLineSchema
from sqlalchemy.exc import DatabaseError, IntegrityError

from src.config import get_config
from src.db.mongo import get_mongo_client
from src.db.session import ScopedSession
from src.models.tickets import Line, TimeSeries
from src.repository.lines import LineRepository, get_line_repository
from src.repository.time_series import TimeSeriesRepository, get_time_series_repository
from src.repository.users import (
    UserRepository,
    UserRoleRepository,
    get_user_repository,
    get_user_role_repository,
)
from src.repository.zones import ZoneRepository, get_zone_repository
from src.schema.time_series import ReadTimeSeriesSchema
from src.schema.warehouse import BaseStagingSchema, LineStatus, LineStatusEnum
from src.service.base import BaseService

logger = logging.getLogger(__name__)


class LineService(
    BaseService[Line, CreateLineSchema, UpdateLineSchema, ReadLineSchema, LineRepository]
):
    _model = Line
    _create_schema = CreateLineSchema  # type: ignore
    _update_schema = UpdateLineSchema  # type: ignore
    _return_schema = ReadLineSchema  # type: ignore
    _collection = Collection

    def __init__(
        self,
        line_repository: LineRepository,
        zone_repository: ZoneRepository,
        time_series_repository: TimeSeriesRepository,
        user_repository: UserRepository,
        role_repository: UserRoleRepository,
        client: MongoClient,
    ):
        super().__init__(line_repository)
        self._zone_repository = zone_repository
        self._time_series_repository = time_series_repository
        self._user_repository = user_repository
        self._role_repository = role_repository
        mongodb = client[get_config().mongo_database_name]
        collection = mongodb["lines"]
        line_status_collection = mongodb["line-status"]
        self._collection = collection
        self.line_status_collection = line_status_collection

    def _find_role(self, role_id: int, session: ScopedSession):  # type: ignore
        if role_id != 0:
            role = self._user_repository.find_one_by_id(id_=role_id, session=session)
            if not role:
                raise HTTPException(
                    status_code=status.HTTP_409_CONFLICT,
                    detail=f"Role with id={role_id} doesn't exist",
                )

    def create(self, line_in: CreateLineSchema) -> ReadLineSchema:
        try:
            with ScopedSession() as _active_session:
                with _active_session.begin():
                    line_object = Line(**line_in.dict(exclude_unset=True))
                    if not line_object.name:
                        raise HTTPException(
                            status_code=status.HTTP_409_CONFLICT,
                            detail=f"Line has no name. name={line_object.name}",
                        )
                    persisted = self._repository.find_one_by_attr(
                        "name", line_object.name, session=_active_session
                    )
                    if persisted is not None:
                        raise HTTPException(
                            status_code=status.HTTP_409_CONFLICT,
                            detail=f"Line with name={line_object.name} already exists",
                        )
                    persisted_zone = self._zone_repository.find_one_by_id(
                        line_in.zone_id, session=_active_session
                    )
                    if persisted_zone is None:
                        raise HTTPException(
                            status_code=status.HTTP_409_CONFLICT,
                            detail=f"Zone with id={line_in.zone_id} doesn't exist",
                        )
                    line = self._repository.insert_one(
                        line_object, session=_active_session
                    )
                    line_schema = self._return_schema.from_orm(line)
                    self._collection.insert_one(
                        BaseStagingSchema(
                            method="create_line", document=line_schema.dict()
                        ).dict()
                    )
                    return line_schema
        except IntegrityError as e:
            raise HTTPException(status_code=status.HTTP_409_CONFLICT, detail=str(e))

    def change_line_status(
        self, line_id: int, status: SeriesStatusEnum
    ) -> ReadTimeSeriesSchema:
        try:
            with ScopedSession() as _active_session:
                with _active_session.begin():
                    time_series_object = TimeSeries(line_id=line_id, status=status.name)
                    time_series = self._time_series_repository.insert_one(
                        time_series_object, session=_active_session
                    )
                    self.line_status_collection.insert_one(
                        LineStatus(
                            method="change_line_status",
                            line_id=line_id,
                            status=LineStatusEnum.ACTIVE
                            if status == SeriesStatusEnum.RUNNING
                            else LineStatusEnum.INACTIVE,
                        ).dict()
                    )
                    return ReadTimeSeriesSchema.from_orm(time_series)
        except DatabaseError as e:
            logger.debug("Error updating line %s:", line_id, exc_info=e)
            raise HTTPException(
                status_code=fastapi.status.HTTP_400_BAD_REQUEST,
                detail="There was an error updating a line",
            )

    def update_one(self, line_id: int, line_in: UpdateLineSchema) -> ReadLineSchema:
        try:
            line_dict = line_in.dict(exclude_unset=True)
            with ScopedSession() as active_session:
                with active_session.begin():
                    if not line_in.name:
                        raise HTTPException(
                            status_code=status.HTTP_409_CONFLICT,
                            detail=f"Line has no name. name={line_in.name}",
                        )
                    persisted_by_name = self._repository.find_one_by_attr(
                        "name", line_in.name, session=active_session
                    )
                    if (
                        persisted_by_name is not None
                        and persisted_by_name.id_ != line_id
                    ):
                        raise HTTPException(
                            status_code=status.HTTP_409_CONFLICT,
                            detail=f"Line with name={line_in.name} already exists",
                        )
                    persisted_by_id = self._repository.find_one_by_id(
                        line_id, session=active_session
                    )
                    if persisted_by_id is None:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Line with id={line_id} does not exist",
                        )
                    updated_line = self._repository.update_one_by_id(
                        line_id, line_dict, session=active_session
                    )
                    line_schema = self._return_schema.from_orm(updated_line)
                    self._collection.insert_one(
                        BaseStagingSchema(
                            method="update_line", document=line_schema.dict()
                        ).dict()
                    )
                    return line_schema
        except DatabaseError as e:
            logger.debug("Error updating line %s:", line_id, exc_info=e)
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="There was an error creating a line",
            )

    def update_line_config(self, line_id: int, event_config) -> ReadLineSchema:
        try:
            with ScopedSession() as active_session:
                with active_session.begin():
                    line_in = self._repository.find_one_by_id(
                        line_id, session=active_session
                    )
                    if not line_in:
                        raise HTTPException(
                            status_code=status.HTTP_409_CONFLICT,
                            detail=f"Line with id={line_id} does not exist",
                        )
                    for key, event in event_config.items():
                        if key == "PLANNED_ACTION":
                            self._find_role(
                                event["short_action_role_id"], active_session
                            )
                            self._find_role(event["long_action_role_id"], active_session)
                        else:
                            for escalation in event.values():
                                self._find_role(escalation["role_id"], active_session)
                    line_in.event_config = event_config
                    line_in_update = UpdateLineSchema.from_orm(line_in)
                    line_dict = line_in_update.dict(exclude_unset=True)
                    updated_line = self._repository.update_one_by_id(
                        line_id, line_dict, session=active_session
                    )
                    line_schema = self._return_schema.from_orm(updated_line)
                    self._collection.insert_one(
                        BaseStagingSchema(
                            method="update_line_config", document=line_schema.dict()
                        ).dict()
                    )
                    return line_schema
        except DatabaseError as e:
            logger.debug("Error updating line %s:", line_id, exc_info=e)
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="There was an error creating a line",
            )

    def add_users(self, line_id: int, users: List[int]):
        try:
            with ScopedSession() as active_session:
                with active_session.begin():
                    persisted_line = self._repository.find_one_by_id(
                        line_id, session=active_session
                    )
                    if not persisted_line:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Line with id={line_id} not found",
                        )
                    for user_id in users:
                        persisted_user = self._user_repository.find_one_by_id(
                            id_=user_id, session=active_session
                        )
                        if not persisted_user:
                            raise HTTPException(
                                status_code=status.HTTP_404_NOT_FOUND,
                                detail=f"User with id={user_id} not found",
                            )
                        self._repository.insert_user_on_line(
                            line_id, user_id, active_session
                        )
                    line_schema = self._return_schema.from_orm(persisted_line)
                    self._collection.insert_one(
                        BaseStagingSchema(
                            method="add_users_to_line", document=line_schema.dict()
                        ).dict()
                    )
                    return line_schema
        except DatabaseError as e:
            logger.debug("Error updating line %s:", line_id, exc_info=e)
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="There was an error updating a line",
            )

    def set_users(self, line_id: int, users: List[int]):
        try:
            with ScopedSession() as active_session:
                with active_session.begin():
                    persisted_line = self._repository.find_one_by_id(
                        line_id, session=active_session
                    )
                    if not persisted_line:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Line with id={line_id} not found",
                        )
                    self._repository.remove_users_from_line(line_id, active_session)
                    for user_id in users:
                        persisted_user = self._user_repository.find_one_by_id(
                            id_=user_id, session=active_session
                        )
                        if not persisted_user:
                            raise HTTPException(
                                status_code=status.HTTP_404_NOT_FOUND,
                                detail=f"User with id={user_id} not found",
                            )
                        self._repository.insert_user_on_line(
                            line_id, user_id, active_session
                        )
                    line_schema = self._return_schema.from_orm(persisted_line)
                    self._collection.insert_one(
                        BaseStagingSchema(
                            method="set_users_on_line", document=line_schema.dict()
                        ).dict()
                    )
                    return line_schema
        except DatabaseError as e:
            logger.debug("Error updating line %s:", line_id, exc_info=e)
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="There was an error creating a line",
            )

    def delete_line(self, line_id: int) -> None:
        try:
            with ScopedSession() as active_session:
                with active_session.begin():
                    persisted_line = self._repository.find_one_by_id(
                        line_id, session=active_session
                    )
                    if not persisted_line:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Line with id={line_id} not found",
                        )
                    if len(persisted_line.machines) != 0:
                        raise HTTPException(
                            status_code=status.HTTP_409_CONFLICT,
                            detail=f"Line with id={line_id} has active machines",
                        )
                    self._repository.delete_one_by_id(line_id, active_session)
        except DatabaseError as e:
            logger.debug("Error updating line %s:", line_id, exc_info=e)
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="There was an error creating a line",
            )


def get_line_service(
    line_repository: LineRepository = Depends(get_line_repository),
    zone_repository: ZoneRepository = Depends(get_zone_repository),
    time_series_repository: TimeSeriesRepository = Depends(get_time_series_repository),
    user_repository: UserRepository = Depends(get_user_repository),
    role_repository: UserRoleRepository = Depends(get_user_role_repository),
    client: MongoClient = Depends(get_mongo_client),
):
    return LineService(
        line_repository,
        zone_repository,
        time_series_repository,
        user_repository,
        role_repository,
        client,
    )
