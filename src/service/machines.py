import logging
from typing import List

from fastapi import Depends, HTTPException, status
from pymongo import MongoClient
from pymongo.collection import Collection
from schemas.andon.base import Page
from schemas.andon.enums import MachineOrderByEnum
from schemas.andon.machines import (
    CreateMachineSchema,
    CreateMachineTypeSchema,
    ReadMachineSchema,
    ReadMachineTypeSchema,
    UpdateMachineSchema,
    UpdateMachineTypeSchema,
)
from sqlalchemy.exc import DatabaseError, IntegrityError

from src.config import get_config
from src.db.mongo import get_mongo_client
from src.db.session import ScopedSession
from src.models.tickets import Machine, MachineType
from src.repository.lines import LineRepository, get_line_repository
from src.repository.machines import (
    MachineRepository,
    MachineTypeRepository,
    get_machine_repository,
    get_machine_type_repository,
)
from src.schema.warehouse import BaseStagingSchema
from src.service.base import BaseService

logger = logging.getLogger(__name__)


class MachineTypeService(
    BaseService[
        MachineType,
        CreateMachineTypeSchema,
        UpdateMachineTypeSchema,
        ReadMachineTypeSchema,
        MachineTypeRepository,
    ]
):
    _model = MachineType
    _create_schema = CreateMachineTypeSchema
    _update_schema = UpdateMachineTypeSchema
    _return_schema = ReadMachineTypeSchema
    _collection = Collection

    def __init__(self, repository: MachineTypeRepository, client: MongoClient):
        super().__init__(repository)
        mongodb = client[get_config().mongo_database_name]
        collection = mongodb["machine_types"]
        self._collection = collection

    def create(
        self,
        create_machine_type_schema: CreateMachineTypeSchema,
    ) -> ReadMachineTypeSchema:
        try:
            machine_type_object = MachineType(
                **create_machine_type_schema.dict(exclude_unset=True)
            )
            with ScopedSession() as active_session:
                with active_session.begin():
                    inserted = self._repository.insert_one(
                        machine_type_object, session=active_session
                    )
                    machine_type_schema = self._return_schema.from_orm(inserted)
                    self._collection.insert_one(
                        BaseStagingSchema(
                            method="create_machine_type",
                            document=machine_type_schema.dict(),
                        ).dict()
                    )
                    return machine_type_schema
        except DatabaseError:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="There was an error creating a machine type",
            )

    def update_one(
        self, machine_type_id: int, machine_type_in: UpdateMachineTypeSchema
    ) -> ReadMachineTypeSchema:
        try:
            machine_type_dict = machine_type_in.dict(exclude_unset=True)
            with ScopedSession() as active_session:
                with active_session.begin():
                    _name = machine_type_in.name
                    persisted_with_name = self._repository.find_one_by_attr(
                        "name", _name, session=active_session
                    )
                    if (
                        persisted_with_name is not None
                        and persisted_with_name.id_ != machine_type_id
                    ):
                        raise HTTPException(
                            status_code=status.HTTP_409_CONFLICT,
                            detail=f"Machine type with name={_name} already exists",
                        )
                    persisted_by_id = self._repository.find_one_by_id(
                        machine_type_id, session=active_session
                    )
                    if persisted_by_id is None:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="Machine type with id={} does not exist".format(
                                machine_type_id
                            ),
                        )
                    inserted = self._repository.update_one_by_id(
                        machine_type_id, machine_type_dict, session=active_session
                    )
                    machine_type_schema = self._return_schema.from_orm(inserted)
                    self._collection.insert_one(
                        BaseStagingSchema(
                            method="update_machine_type",
                            document=machine_type_schema.dict(),
                        ).dict()
                    )
                    return machine_type_schema
        except DatabaseError:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="There was an error creating a machine type",
            )


class MachineService(
    BaseService[
        Machine,
        CreateMachineSchema,
        UpdateMachineSchema,
        ReadMachineSchema,
        MachineRepository,
    ]
):
    def __init__(
        self,
        repository: MachineRepository,
        machine_type_repository: MachineTypeRepository,
        line_repository: LineRepository,
        client: MongoClient,
    ):
        super().__init__(repository)
        self._machine_type_repository = machine_type_repository
        self._line_repository = line_repository
        mongodb = client[get_config().mongo_database_name]
        collection = mongodb["machines"]
        self._collection = collection

    _model = Machine
    _create_schema = CreateMachineSchema
    _update_schema = UpdateMachineSchema
    _return_schema = ReadMachineSchema
    _collection = Collection

    def create(
        self,
        create_machine_schema: CreateMachineSchema,
    ) -> ReadMachineSchema:
        try:
            with ScopedSession() as active_session:
                with active_session.begin():
                    machine_object = Machine(
                        **create_machine_schema.dict(
                            exclude_unset=True, exclude={"line_id"}
                        ),
                    )
                    persisted = self._repository.find_one_by_attr(
                        "name", create_machine_schema.name, session=active_session
                    )
                    if persisted is not None:
                        raise HTTPException(
                            status_code=status.HTTP_400_BAD_REQUEST,
                            detail=f"Machine with id={persisted} " f"already exist",
                        )
                    machine_type_id = create_machine_schema.machine_type_id
                    line_id = create_machine_schema.line_id
                    persisted_type = self._machine_type_repository.find_one_by_id(
                        machine_type_id, session=active_session
                    )
                    if not persisted_type:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail=(
                                f"Machine type with id={machine_type_id}"
                                "does not exist"
                            ),
                        )

                    persisted_line = self._line_repository.find_one_by_id(
                        line_id, session=active_session
                    )
                    if not persisted_line:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Production line with id={line_id} does not exist",
                        )
                    inserted = self._repository.insert_one(
                        machine_object, session=active_session
                    )
                    if not inserted or not inserted.id_:
                        raise HTTPException(
                            status_code=status.HTTP_400_BAD_REQUEST,
                            detail="There was an error creating machine",
                        )
                    self._line_repository.insert_machine_on_line(
                        line_id, inserted.id_, session=active_session
                    )
                    machine_schema = self._return_schema.from_orm(inserted)
                    self._collection.insert_one(
                        BaseStagingSchema(
                            method="create_machine", document=machine_schema.dict()
                        ).dict()
                    )
                    return machine_schema
        except IntegrityError as error:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST, detail=str(error)
            )

    def update_one(
        self,
        machine_id: int,
        machine_in: UpdateMachineSchema,
    ) -> ReadMachineSchema:
        try:
            machine_dict = machine_in.dict(exclude_unset=True)
            with ScopedSession() as active_session:
                with active_session.begin():
                    persisted_by_id = self._repository.find_one_by_id(
                        machine_id, session=active_session
                    )
                    if persisted_by_id is None:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Machine with id={machine_id} does not exist",
                        )
                    _machine_type_id = machine_in.machine_type_id
                    if _machine_type_id:
                        persisted_type = self._machine_type_repository.find_one_by_id(
                            _machine_type_id, session=active_session
                        )
                        if persisted_type is None:
                            raise HTTPException(
                                status_code=status.HTTP_404_NOT_FOUND,
                                detail="Machine type with id={} does not exist".format(
                                    _machine_type_id
                                ),
                            )
                    updated_machine = self._repository.update_one_by_id(
                        machine_id, machine_dict, session=active_session
                    )
                    machine_schema = self._return_schema.from_orm(updated_machine)
                    self._collection.insert_one(
                        BaseStagingSchema(
                            method="update_machine",
                            document=machine_schema.dict(),
                        ).dict()
                    )
                    return machine_schema
        except DatabaseError as e:
            logger.debug("Error updating machine %s", machine_id, exc_info=e)
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="There was an error creating a machine",
            )

    def get_by_line_id(
        self,
        line_id: int,
        page_number: int,
        page_size: int,
        order_by: List[MachineOrderByEnum],
    ) -> Page[ReadMachineSchema]:
        order_by_value = [item.value for item in order_by]
        try:
            with ScopedSession() as active_session:
                with active_session.begin():
                    machines = self._repository.find_by_line_id(
                        line_id,
                        page_number=page_number,
                        page_size=page_size,
                        order_by=order_by_value,
                        session=active_session,
                    )
                    if not machines:
                        # if a line can exist without a machine
                        line = self._line_repository.find_one_by_id(
                            id_=line_id, session=active_session
                        )
                        if not line:
                            raise HTTPException(
                                status_code=status.HTTP_404_NOT_FOUND,
                                detail=f"Line with id={line_id} does not exist",
                            )
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"No machines found on line {line_id}",
                        )
                    count = self._repository.count_by_line_id(
                        line_id, session=active_session
                    )
                    return Page(
                        content=[
                            self._return_schema.from_orm(machine) for machine in machines
                        ],
                        total=count,
                    )
        except DatabaseError:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="There was an error creating a machine",
            )


def get_machine_type_service(
    machine_type_repository: MachineTypeRepository = Depends(
        get_machine_type_repository
    ),
    client: MongoClient = Depends(get_mongo_client),
):
    return MachineTypeService(machine_type_repository, client)


def get_machine_service(
    machine_repository: MachineRepository = Depends(get_machine_repository),
    machine_type_repository: MachineTypeRepository = Depends(
        get_machine_type_repository
    ),
    line_repository: LineRepository = Depends(get_line_repository),
    client: MongoClient = Depends(get_mongo_client),
) -> MachineService:
    """
    Get Machine service
    """
    return MachineService(
        machine_repository, machine_type_repository, line_repository, client
    )
