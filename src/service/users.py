import logging
from typing import List, Optional

from fastapi import Depends, HTTPException, status
from pymongo import MongoClient
from pymongo.collection import Collection
from schemas.andon.base import Page
from schemas.andon.users import (
    CreateRoleSchema,
    CreateUserSchema,
    ReadRoleSchema,
    ReadUserSchema,
    UpdateRoleSchema,
    UpdateUserSchema,
)
from sqlalchemy.exc import DatabaseError, IntegrityError

from src.config import get_config
from src.db.mongo import get_mongo_client
from src.db.session import ScopedSession
from src.models.users import Role, User
from src.repository.errors import EntryNotFoundError
from src.repository.lines import LineRepository, get_line_repository
from src.repository.users import (
    UserRepository,
    UserRoleRepository,
    get_user_repository,
    get_user_role_repository,
)
from src.schema.warehouse import BaseStagingSchema
from src.service.base import BaseService
from src.utils import get_password_hash

logger = logging.getLogger(__name__)


class UserRoleService(
    BaseService[
        Role, CreateRoleSchema, UpdateRoleSchema, ReadRoleSchema, UserRoleRepository
    ]
):
    _model = Role
    _create_schema = CreateRoleSchema  # type: ignore
    _update_schema = UpdateRoleSchema  # type: ignore
    _return_schema = ReadRoleSchema  # type: ignore
    _collection = Collection

    def __init__(self, repository: UserRoleRepository, client: MongoClient):
        super().__init__(repository)
        mongodb = client[get_config().mongo_database_name]
        collection = mongodb["users_roles"]
        self._collection = collection

    def create(self, role_in: CreateRoleSchema):
        try:
            role_object = Role(**role_in.dict(exclude_unset=True))
            with ScopedSession() as active_session:
                with active_session.begin():
                    persisted_by_name = self._repository.find_one_by_attr(
                        "name", role_in.name, session=active_session
                    )
                    if persisted_by_name is not None:
                        raise HTTPException(
                            status_code=status.HTTP_409_CONFLICT,
                            detail=f"Role with name={role_in.name} already exists",
                        )
                    role = self._repository.insert_one(
                        role_object, session=active_session
                    )
                    role_schema = self._return_schema.from_orm(role)
                    self._collection.insert_one(
                        BaseStagingSchema(
                            method="create_role", document=role_schema.dict()
                        ).dict()
                    )
                    return self._return_schema.from_orm(role)

        except DatabaseError as e:
            logger.debug("Error creating role %s", role_in, exc_info=e)
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="There was an error creating a role",
            )

    def update_one(self, role_id: int, role_in: UpdateRoleSchema) -> ReadRoleSchema:
        try:
            role_dict = role_in.dict(exclude_unset=True)
            with ScopedSession() as active_session:
                with active_session.begin():
                    persisted_by_id = self._repository.find_one_by_id(
                        role_id, session=active_session
                    )
                    if persisted_by_id is None:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Role with id={role_id} does not exist",
                        )
                    updated_role = self._repository.update_one_by_id(
                        role_id, role_dict, session=active_session
                    )
                    role_schema = self._return_schema.from_orm(updated_role)
                    self._collection.insert_one(
                        BaseStagingSchema(
                            method="update_role",
                            document=role_schema.dict(),
                        ).dict()
                    )
                    return role_schema
        except DatabaseError as e:
            logger.debug("Error updating role %s", role_id, exc_info=e)
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="There was an error updating role",
            )


class UserService(
    BaseService[User, CreateUserSchema, UpdateUserSchema, ReadUserSchema, UserRepository]
):
    def __init__(
        self,
        repository: UserRepository,
        role_repository: UserRoleRepository,
        line_repository: LineRepository,
        client: MongoClient,
    ):
        super().__init__(repository)
        self.role_repository = role_repository
        self.line_repository = line_repository
        mongodb = client[get_config().mongo_database_name]
        collection = mongodb["users"]
        self._collection = collection

    _model = User
    _create_schema = CreateUserSchema  # type: ignore
    _update_schema = UpdateUserSchema  # type: ignore
    _return_schema = ReadUserSchema  # type: ignore
    _collection = Collection

    def get_by_email(self, email: str) -> Optional[ReadUserSchema]:
        """
        Get user by email from repository
        """
        try:
            with ScopedSession() as active_session:
                with active_session.begin():
                    user = self._repository.find_by_email(email, session=active_session)
                    if user is None:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"User with {email=} does not exist",
                        )
                    return self._return_schema.from_orm(user)
        except DatabaseError as e:
            logger.debug("Error getting user by email %s", email, exc_info=e)
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="There was an error getting a user",
            )

    def get_by_username(self, username: str) -> ReadUserSchema:
        """
        Get user by username from repository
        """
        try:
            with ScopedSession() as active_session:
                with active_session.begin():
                    user = self._repository.find_by_username(
                        username, session=active_session
                    )
                    if user is None:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"User with username={username} does not exist",
                        )
                    return self._return_schema.from_orm(user)
        except DatabaseError as e:
            logger.debug("Error getting user by username %s", username, exc_info=e)
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="There was an error getting a user",
            )

    def get_by_role(self, role_id: int) -> List[ReadUserSchema]:
        """
        Get user by role from repository
        """
        try:
            with ScopedSession() as active_session:
                with active_session.begin():
                    users = self._repository.find_by_role(
                        role_id, session=active_session
                    )
                    if len(users) == 0:
                        role = self.role_repository.find_one_by_id(
                            id_=role_id, session=active_session
                        )
                        if not role:
                            raise HTTPException(
                                status_code=status.HTTP_404_NOT_FOUND,
                                detail=f"Role with id={role_id} does not exist",
                            )
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"No users found with role {role_id}",
                        )
                    return [self._return_schema.from_orm(user) for user in users]
        except DatabaseError as e:
            logger.debug("Error getting users by role %s", role_id, exc_info=e)
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="There was an error getting the users",
            )

    def get_by_card_id(self, card_id: str) -> ReadUserSchema:
        try:
            with ScopedSession() as active_session:
                with active_session.begin():
                    user = self._repository.find_one_by_attr(
                        "card_id", card_id, active_session
                    )
                    if user is None:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"User with card_id={card_id} does not exist",
                        )
                    return self._return_schema.from_orm(user)
        except DatabaseError as e:
            logger.debug("Error getting user by card_id %s", card_id, exc_info=e)
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="There was an error getting a user",
            )

    def get_by_line_id(self, line_id: int, page_number: int, page_size: int) -> Page:
        try:
            with ScopedSession() as active_session:
                with active_session.begin():
                    users = self._repository.get_users_by_line(
                        line_id, page_number, page_size, session=active_session
                    )
                    if not users:
                        line = self.line_repository.find_one_by_id(
                            line_id, session=active_session
                        )
                        if not line:
                            raise HTTPException(
                                status_code=status.HTTP_404_NOT_FOUND,
                                detail=f"Line with id={line_id} not found",
                            )
                    count = self._repository.count_by_line_id(
                        line_id, session=active_session
                    )
                    return Page(
                        content=[self._return_schema.from_orm(user) for user in users],
                        total=count,
                    )
        except DatabaseError as e:
            logger.debug("Error getting users by line_id %s", line_id, exc_info=e)
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="There was an error getting users",
            )

    def get_by_line_id_and_role_id(
        self, line_id: int, role_id: int
    ) -> List[ReadUserSchema]:
        try:
            with ScopedSession() as active_session:
                with active_session.begin():
                    users = self._repository.get_users_by_role_and_line(
                        role_id, line_id, active_session
                    )
                    if not users:
                        line = self.line_repository.find_one_by_id(
                            line_id, session=active_session
                        )
                        if not line:
                            raise HTTPException(
                                status_code=status.HTTP_404_NOT_FOUND,
                                detail=f"Line with id={line_id} not found",
                            )
                        role = self.role_repository.find_one_by_id(
                            role_id, active_session
                        )
                        if not role:
                            raise HTTPException(
                                status_code=status.HTTP_409_CONFLICT,
                                detail=f"Role with id={role_id} not found",
                            )
                    return [self._return_schema.from_orm(user) for user in users]
        except DatabaseError as e:
            logger.debug(
                "Error getting users by line_id %s and role_id %s",
                line_id,
                role_id,
                exc_info=e,
            )
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="There was an error getting users",
            )

    def create(self, user_in: CreateUserSchema) -> ReadUserSchema:
        try:
            user_model = self._model(
                **user_in.dict(exclude_unset=True, exclude={"roles", "password"}),
                password=get_password_hash(user_in.password),
            )
            with ScopedSession() as active_session:
                with active_session.begin():
                    entry = self._repository.insert_one(
                        user_model, session=active_session
                    )
                    if not entry or not entry.id_:
                        raise HTTPException(
                            status_code=status.HTTP_400_BAD_REQUEST,
                            detail="There was an error creating user",
                        )
                    if user_in.roles:
                        for role_id in user_in.roles:
                            persisted_role = self.role_repository.find_one_by_id(
                                role_id, session=active_session
                            )
                            if not persisted_role:
                                raise HTTPException(
                                    status_code=status.HTTP_404_NOT_FOUND,
                                    detail=f"Role with id={role_id} is not found",
                                )
                            self._repository.insert_role_to_user(
                                entry.id_, role_id, session=active_session
                            )
                    user_schema = self._return_schema.from_orm(entry)
                    self._collection.insert_one(
                        BaseStagingSchema(
                            method="create_user", document=user_schema.dict()
                        ).dict()
                    )
                    return user_schema
        except DatabaseError as e:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail=f"Integrity error: {e.orig.diag.message_detail}",
            )

    def update(self, user_id: int, user_in: UpdateUserSchema) -> ReadUserSchema:
        try:
            with ScopedSession() as active_session:
                with active_session.begin():
                    user = self._repository.find_one_by_id(
                        user_id, session=active_session
                    )
                    if user is None:
                        raise EntryNotFoundError
                    user_dict = user_in.dict(
                        exclude_unset=True,
                        exclude={"roles"},
                    )
                    if user_in.roles:
                        self._repository.purge_user_roles(
                            user_id, session=active_session
                        )
                        for role_id in user_in.roles:
                            persisted_role = self.role_repository.find_one_by_id(
                                role_id, session=active_session
                            )
                            if not persisted_role:
                                raise HTTPException(
                                    status_code=status.HTTP_404_NOT_FOUND,
                                    detail=f"Role with id={role_id} is not found",
                                )
                            self._repository.insert_role_to_user(
                                user_id, role_id, session=active_session
                            )
                    entry = self._repository.update_one_by_id(
                        user_id, user_dict, session=active_session
                    )
                    user_schema = self._return_schema.from_orm(entry)
                    self._collection.insert_one(
                        BaseStagingSchema(
                            method="update_user", document=user_schema.dict()
                        ).dict()
                    )
                    return user_schema
        except IntegrityError as e:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail=f"Integrity error: {e.orig.diag.message_detail}",
            )
        except EntryNotFoundError:

            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
            )


def get_user_role_service(
    user_role_repository: UserRoleRepository = Depends(get_user_role_repository),
    client: MongoClient = Depends(get_mongo_client),
):
    return UserRoleService(user_role_repository, client)


def get_user_service(
    user_repository: UserRepository = Depends(get_user_repository),
    user_role_repository: UserRoleRepository = Depends(get_user_role_repository),
    line_repository: LineRepository = Depends(get_line_repository),
    client: MongoClient = Depends(get_mongo_client),
) -> UserService:
    return UserService(user_repository, user_role_repository, line_repository, client)
