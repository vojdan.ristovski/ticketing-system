import json
from typing import List
from uuid import UUID

import jwt
from fastapi import HTTPException, status
from passlib.context import CryptContext
from schemas.andon.auth import TokenSchema
from schemas.andon.events_config import EventsConfig
from schemas.andon.tickets import ReadTicketSchema
from sqlalchemy.orm import Session

from src.config import get_config
from src.models.users import User
from src.repository.users import UserRepository, get_user_repository

user_repository: UserRepository = get_user_repository()

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
settings = get_config()


class UUIDEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, UUID):
            # if the obj is uuid, we simply return the value of uuid
            return obj.hex
        return json.JSONEncoder.default(self, obj)


def verify_password(plain_password, hashed_password):
    return pwd_context.verify(plain_password, hashed_password)


def get_password_hash(password):
    return pwd_context.hash(password)


def decode_token(token: str) -> TokenSchema:
    try:
        return TokenSchema(
            **jwt.decode(
                token,
                settings.secret_key,
                algorithms=["HS256"],
            )
        )
    except jwt.ExpiredSignatureError:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Signature expired. Please log in again.",
        )


def encode_token(data: TokenSchema):
    return jwt.encode(
        data.dict(), settings.secret_key, algorithm="HS256", json_encoder=UUIDEncoder
    )


def get_role_id_from_planned_action_length(
    mapping: EventsConfig, total_planned_time: int
):
    max_action_length = mapping.PLANNED_ACTION.short_action_length_seconds
    if total_planned_time > max_action_length:
        return mapping.PLANNED_ACTION.long_action_role_id
    return mapping.PLANNED_ACTION.short_action_role_id


def get_users_from_ticket(ticket: ReadTicketSchema, session: Session):
    users: List[User] = []
    for u in ticket.handled_by:
        user = user_repository.find_one_by_id(u.user_id, session)
        if user:
            users.append(user)
    return users
