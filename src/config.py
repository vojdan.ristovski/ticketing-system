from functools import lru_cache
from typing import List

from pydantic import BaseModel, BaseSettings

# flake8: noqa
FALLBACK_SENTRY_DSN = "https://glet_e6ca53a3db3e8bd3b36952c4b86fe2a9@gitlab.com/api/v4/error_tracking/collector/31476347"


class Config(BaseSettings):
    cors_origins: List[str] = []
    host: str = "0.0.0.0"  # nosec
    env: str = "dev"
    database_url: str = "postgresql://postgres:password@db/fastapi"
    test_database_url: str = "postgresql://postgres:password@localhost/fastapi"
    sentry_dsn: str = FALLBACK_SENTRY_DSN
    celery_backend_url: str = "redis://redis:6379/0"
    celery_broker_url: str = "amqp://rabbitmq:rabbitmq@rabbitmq:5672/"
    email_from: str = "noreply@andon.masac.io"
    smtp_port = 1025
    smtp_host = "maildev"
    smtp_user = ""
    smtp_password = ""  # nosec
    public_folder = "./public/"
    secret_key: str = "secret"
    ari_endpoint: str = ""
    ari_api_key: str = ""
    starttls: bool = False
    daily_task_hour = 16
    daily_task_minute = 0
    mongo_url: str = "mongodb://mongo_user:mongo_password@mongo:27017"
    mongo_database_name: str = "aptiv-staging-db"
    etl_destination_db_url: str = ""
    etl_cron = "*/30"
    etl_mongo_db_name = "aptiv-staging-db"

    class Config:
        env_file = ".env"


class LogConfig(BaseModel):
    """Logging configuration"""

    LOGGER_NAME: str = "main_logger"
    LOG_FORMAT: str = "%(levelprefix)s | %(asctime)s | %(message)s"
    LOG_LEVEL: str = "DEBUG"

    # Logging config
    version = 1
    disable_existing_loggers = False
    formatters = {
        "default": {
            "()": "uvicorn.logging.DefaultFormatter",
            "fmt": LOG_FORMAT,
            "datefmt": "%Y-%m-%d %H:%M:%S",
        },
    }
    handlers = {
        "default": {
            "formatter": "default",
            "class": "logging.StreamHandler",
            "stream": "ext://sys.stderr",
        },
    }
    loggers = {
        "main_logger": {"handlers": ["default"], "level": LOG_LEVEL},
    }


@lru_cache()
def get_config() -> Config:
    config = Config()
    print("Loaded config %s" % config)
    return config
