from sqlalchemy.orm import declarative_base
from sqlalchemy.sql.schema import MetaData

Base = declarative_base()
meta = MetaData()
