import enum


class TicketTypeEnum(enum.IntEnum):
    MACHINE_PROBLEM = 0
    QUALITY_PROBLEM = 1
    MAINTENANCE_PROBLEM = 2
    MATERIALS_PROBLEM = 3
    PLANNED_ACTION = 4


class PriorityEnum(enum.IntEnum):
    TOTAL_HALT = 0
    EFFICIENCY_REDUCTION = 1
    POTENTIAL_HALT = 2
    NON_CRITICAL = 3
    INACTIVE = 4
    PLANNED_ACTION = 5
    PREVENTIVE_MAINTENANCE = 6


class StatusEnum(enum.IntEnum):
    OPEN = 0
    CLOSED = 1
    PENDING = 2
    FORWARDED = 3
    ON_HOLD = 4
    WAITING_FOR_APPROVAL = 5
    APPROVED = 6
    IN_PROGRESS = 7
    REJECTED = 8
    PRIORITY = 9
    MATERIAL_NOT_AVAILABLE = 10


class OrderDirection(enum.IntEnum):
    ASCENDING = 1
    DESCENDING = -1


class SeriesStatusEnum(enum.IntEnum):
    TOTAL_HALT = 0
    EFFICIENCY_REDUCTION = 1
    POTENTIAL_HALT = 2
    NON_CRITICAL = 3
    INACTIVE = 4
    PLANNED_ACTION = 5
    PREVENTIVE_MAINTENANCE = 6
    RUNNING = 7
    NOT_RUNNING = 8


class DayEnum(enum.IntEnum):
    MONDAY = 0
    TUESDAY = 1
    WEDNESDAY = 2
    THURSDAY = 3
    FRIDAY = 4
    SATURDAY = 5
    SUNDAY = 6


class TeamEnum(enum.IntEnum):
    A = 0
    B = 1
    C = 2
    N = 3


class LineOrderByEnum(enum.Enum):
    NAME = "name"
    ZONE_ID = "zone_id"


class MachineOrderByEnum(enum.Enum):
    NAME = "name"
    DESCRIPTION = "description"
    SKU = "sku"
    MACHINE_TYPE_ID = "machine_type_id"
    ORDER = "order"
    LINE_ID = "line_id"


class TicketOrderByEnum(enum.Enum):
    REASON = "reason"
    STATUS = "status"
    CORRECTIVE_ACTION = "corrective_action"
    ISSUE_DESCRIPTION = "issue_description"
    CREATED_BY = "created_by"
    PRIORITY = "priority"
    TICKET_TYPE = "ticket_type"
    MACHINES = "machines"
    COMPONENT_ID = "component_id"
    PRODUCT_ID = "product_id"
    PRODUCT = "product"
    HANDLED_BY = "handled_by"
    DATETIME_FROM = "datetime_from"
    DATETIME_TO = "datetime_to"
    APPROVED_BY = "approved_by"
    CREATED_AT = "created_at"
    UPDATED_AT = "updated_at"
    DELETED_AT = "deleted_at"
    LINE_ID = "line_id"
    ID = "id_"


class ProductOrderByEnum(enum.Enum):
    NAME = "name"
