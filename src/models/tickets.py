from typing import List

from schemas.andon.enums import (
    PriorityEnum,
    SeriesStatusEnum,
    StatusEnum,
    TicketTypeEnum,
)
from sqlalchemy import JSON, Column, DateTime, Integer, String, Text
from sqlalchemy.dialects.postgresql import ENUM
from sqlalchemy.orm import relationship
from sqlalchemy.orm.attributes import Mapped
from sqlalchemy.sql import func
from sqlalchemy.sql.schema import ForeignKey, Table

from src.models import Base, mixins
from src.models.users import (
    User,
    UsersTicketsAssociation,
    UsersTicketsForwardAssociation,
)


class Reason(mixins.BaseDbModel, Base):
    reason = Column(Text(), nullable=False, unique=True)


class Component(mixins.BaseDbModel, Base):
    component_number = Column(Text(), nullable=False, unique=True)
    description = Column(Text(), nullable=False)


class Ticket(mixins.BaseDbModel, Base):
    status = Column(
        ENUM(StatusEnum, name="statuses"),
        name="status",
        nullable=False,
        default=StatusEnum.PENDING,
    )
    corrective_action = Column(Text(), nullable=True)
    issue_description = Column(Text(), nullable=True)
    component_id = Column(
        Text(),
        ForeignKey(Component.component_number, ondelete="SET NULL"),
        nullable=True,
    )
    component: Mapped[Component] = relationship(Component, lazy="joined")
    reason_id = Column(
        Integer, ForeignKey("reasons.id", ondelete="SET NULL"), nullable=True
    )
    reason: Mapped[Reason] = relationship("Reason", lazy="joined")
    created_by_id = Column(
        Integer, ForeignKey(User.id_, ondelete="SET NULL"), nullable=True
    )
    closed_by_id = Column(
        Integer, ForeignKey(User.id_, ondelete="SET NULL"), nullable=True
    )
    closed_at = Column(DateTime(), nullable=True)
    product_id = Column(
        Integer, ForeignKey("products.id", ondelete="SET NULL"), nullable=True
    )
    product: Mapped["Product"] = relationship("Product", back_populates="tickets")
    line_id = Column(Integer, ForeignKey("lines.id", ondelete="SET NULL"), nullable=True)
    datetime_from = Column(DateTime(), nullable=True)
    datetime_to = Column(DateTime(), nullable=True)
    approved_by_id = Column(
        Integer, ForeignKey(User.id_, ondelete="SET NULL"), nullable=True
    )
    line: Mapped["Line"] = relationship("Line", lazy="joined")
    created_by: Mapped[User] = relationship(
        User,
        lazy="joined",
        back_populates="created_tickets",
        primaryjoin="Ticket.created_by_id==User.id_",
    )
    priority = Column(
        ENUM(PriorityEnum, name="priorities"),
        name="priority",
        nullable=False,
    )
    ticket_type = Column(
        ENUM(TicketTypeEnum, name="ticket_types"),
        name="ticket_type",
        nullable=False,
    )
    handled_by: List[UsersTicketsAssociation] = relationship(
        UsersTicketsAssociation,
        lazy="select",
        uselist=True,
        order_by="users_tickets_association.c.accepted_on",
        viewonly=True,
    )
    forwarded_to: List[UsersTicketsForwardAssociation] = relationship(
        UsersTicketsForwardAssociation,
        lazy="select",
        uselist=True,
        order_by="users_tickets_forward_association.c.forwarded_on",
        viewonly=True,
    )
    machines = relationship(
        "Machine",
        secondary="tickets_machines_association",
        lazy="joined",
        sync_backref="tickets",
        secondaryjoin="tickets_machines_association.c.machine_id==Machine.id_",
    )


class MachineType(mixins.BaseDbModel, Base):
    name = Column(String(255), nullable=False, unique=True)


class Zone(mixins.BaseDbModel, Base):
    name = Column(String(255), nullable=False, unique=True)
    lines: Mapped[List["Line"]] = relationship(
        "Line",
        back_populates="zone",
        primaryjoin="and_(Zone.id_==Line.zone_id, Line.deleted==False)",
    )


class Line(mixins.BaseDbModel, Base):
    name = Column(String(255), nullable=False)
    zone_id = Column(Integer, ForeignKey(Zone.id_, ondelete="SET NULL"), nullable=True)
    password = Column(String(255), nullable=False, default="Aptiv703")
    event_config = Column(JSON, nullable=True, default="")
    zone: Mapped[Zone] = relationship(
        "Zone",
        back_populates="lines",
        primaryjoin="and_(Line.zone_id==Zone.id_, Zone.deleted==False)",
    )
    machines: Mapped[List["Machine"]] = relationship(
        "Machine",
        primaryjoin="and_(Line.id_==Machine.line_id, Machine.deleted==False)",
        back_populates="line",
    )
    products: Mapped[List["Product"]] = relationship(
        "Product",
        secondary="products_lines_association",
        lazy="select",
        back_populates="lines",
        secondaryjoin="and_("
        "products_lines_association.c.product_id==Product.id_, "
        "Product.deleted==False"
        ")",
    )
    users: Mapped[List[User]] = relationship(
        "User",
        secondary="users_lines_association",
        lazy="select",
        secondaryjoin="and_("
        "users_lines_association.c.user_id==User.id_, "
        "User.deleted==False"
        ")",
    )


class Machine(mixins.BaseDbModel, Base):
    name = Column(String(255), nullable=False)
    description = Column(String(255), nullable=True)
    sku = Column(String(255), nullable=True)
    machine_type_id = Column(
        Integer, ForeignKey(MachineType.id_, ondelete="CASCADE"), nullable=False
    )
    order = Column(Integer, nullable=True)
    machine_type: Mapped[MachineType] = relationship(
        MachineType,
        lazy="joined",
        primaryjoin="and_("
        "Machine.machine_type_id==MachineType.id_, "
        "MachineType.deleted==False"
        ")",
    )
    line_id = Column(Integer, ForeignKey(Line.id_, ondelete="SET NULL"), nullable=True)
    image = Column(String(255), nullable=True)
    line: Mapped[Line] = relationship(
        Line,
        back_populates="machines",
        primaryjoin="and_(Machine.line_id==Line.id_, Line.deleted==False)",
    )
    tickets: Mapped[List[Ticket]] = relationship(
        "Ticket",
        secondary="tickets_machines_association",
        lazy="joined",
        back_populates="machines",
        secondaryjoin="tickets_machines_association.c.ticket_id==Ticket.id_",
    )


class Product(mixins.BaseDbModel, Base):
    name = Column(String(255), nullable=False)
    lines: Mapped[Line] = relationship(
        "Line",
        secondary="products_lines_association",
        lazy="select",
        back_populates="products",
        secondaryjoin="and_("
        "products_lines_association.c.line_id==Line.id_, "
        "Line.deleted==False"
        ")",
    )
    tickets: Mapped[List[Ticket]] = relationship(
        "Ticket",
        back_populates="product",
    )


# associations

tickets_machines_association = Table(
    "tickets_machines_association",
    Base.metadata,
    Column(
        "ticket_id",
        ForeignKey(Ticket.id_, ondelete="CASCADE"),
        primary_key=True,
    ),
    Column(
        "machine_id",
        ForeignKey(Machine.id_, ondelete="CASCADE"),
        primary_key=True,
    ),
)

products_lines_association = Table(
    "products_lines_association",
    Base.metadata,
    Column(
        "product_id",
        ForeignKey(Product.id_, ondelete="CASCADE"),
        primary_key=True,
    ),
    Column(
        "line_id",
        ForeignKey(Line.id_, ondelete="CASCADE"),
        primary_key=True,
    ),
)

users_lines_association = Table(
    "users_lines_association",
    Base.metadata,
    Column(
        "user_id",
        ForeignKey(User.id_, ondelete="CASCADE"),
        primary_key=True,
    ),
    Column(
        "line_id",
        ForeignKey(Line.id_, ondelete="CASCADE"),
        primary_key=True,
    ),
)


class TimeSeries(mixins.BaseDbModel, Base):
    line_id = Column(Integer, ForeignKey(Line.id_, ondelete="SET NULL"), nullable=False)
    status = Column(
        ENUM(SeriesStatusEnum, name="statuses"),
        name="status",
        nullable=False,
        default=SeriesStatusEnum.RUNNING,
    )
    timestamp = Column(DateTime(), server_default=func.now(), index=True)
    line: Mapped[Line] = relationship(
        Line,
        primaryjoin="and_(TimeSeries.line_id==Line.id_, Line.deleted==False)",
    )
