import factory.fuzzy

from src.db.session import ScopedSession
from src.models.tickets import Line, Machine, MachineType, Ticket, Zone


class MachineTypeFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = MachineType
        sqlalchemy_session = ScopedSession()

    name = factory.Faker()


class ZoneFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = Zone
        sqlalchemy_session = ScopedSession()

    name = factory.Faker("text")


class LineFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = Line
        sqlalchemy_session = ScopedSession()

    name = factory.Faker("text")
    zone = factory.SubFactory(ZoneFactory)


class MachineFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = Machine
        sqlalchemy_session = ScopedSession()

    name = factory.Faker("text")
    description = factory.Faker("text")
    sku = factory.Faker("ean")
    machine_type = factory.SubFactory(MachineTypeFactory)
    line = factory.SubFactory(LineFactory)


class TicketFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = Ticket
        sqlalchemy_session = ScopedSession()

    reason = factory.Faker("text")
    status = factory.fuzzy.FuzzyChoice()
    corrective_action = factory.Faker("text")
    issue_description = factory.Faker("text")
    closed_at = factory.Faker("past_datetime")
    priority = factory.Faker("word", ext_word_list=["low", "medium", "high"])

    @factory.post_generation
    def machines(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            for machine in extracted:
                self.machines.append(machine)
