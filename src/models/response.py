from pydantic import BaseModel


class StatusResponseModel(BaseModel):
    status: int
    message: str
