from schemas.andon.enums import DayEnum, TeamEnum
from sqlalchemy import Boolean, Column, DateTime, Enum, Identity, Integer, String, func
from sqlalchemy.orm import relationship
from sqlalchemy.sql.schema import ForeignKey, Table
from sqlalchemy.sql.sqltypes import ARRAY

from src.models import Base, mixins


class Role(mixins.BaseDbModel, Base):
    """
    Role model
    """

    name = Column(String(255), nullable=False)


class User(mixins.BaseDbModel, Base):
    """
    User model
    """

    roles = relationship(
        Role, secondary="users_roles_association", uselist=True, lazy="joined"
    )
    responded_tickets = relationship(
        "Ticket",
        secondary="users_tickets_association",
        lazy="select",
        secondaryjoin="and_("
        "users_tickets_association.c.ticket_id==Ticket.id_, "
        "Ticket.deleted==False"
        ")",
        viewonly=True,
        uselist=True,
    )
    created_tickets = relationship(
        "Ticket",
        lazy="select",
        back_populates="created_by",
        primaryjoin="and_("
        "Ticket.created_by_id==User.id_, "
        "Ticket.deleted==False"
        ")",
        viewonly=True,
    )
    first_name = Column(String(25), nullable=False)
    last_name = Column(String(25), nullable=False)
    email = Column(String(50), nullable=True, unique=True)
    username = Column(String(50), nullable=False, unique=True)
    password = Column(String(255), nullable=True)
    is_admin = Column(Boolean, default=False)

    # TODO: Check constraints with BA
    phone_number = Column(String(11), nullable=True)
    card_id = Column(String(255), nullable=True, unique=True)
    work_hours_from = Column(String(), nullable=True)
    work_hours_to = Column(String(), nullable=True)
    work_days = Column(
        ARRAY(Enum(DayEnum, name="work_days", create_constraint=False)),
        nullable=True,
    )
    team = Column(
        Enum(TeamEnum, name="team", create_constraint=False),
        nullable=True,
    )

    def __eq__(self, other):
        return (
            isinstance(other, User)
            and self.first_name == other.first_name
            and self.last_name == other.last_name
            and self.email == other.email
            and self.username == other.username
            and self.password == other.password
            and self.is_admin == other.is_admin
            and self.phone_number == other.phone_number
            and self.card_id == other.card_id
            and self.work_hours_from == other.work_hours_from
            and self.work_hours_to == other.work_hours_to
            and self.work_days == other.work_days
        )


users_roles_association = Table(
    "users_roles_association",
    Base.metadata,
    Column("user_id", ForeignKey(User.id_, ondelete="CASCADE"), primary_key=True),
    Column("role_id", ForeignKey(Role.id_, ondelete="CASCADE"), primary_key=True),
)

# indicates the timeline of people that worked on tickets
users_tickets_association = Table(
    "users_tickets_association",
    Base.metadata,
    Column("user_id", ForeignKey(User.id_, ondelete="CASCADE"), primary_key=True),
    Column("ticket_id", ForeignKey("tickets.id", ondelete="CASCADE"), primary_key=True),
    Column("id", Integer, Identity(), primary_key=True),
    Column("accepted_on", DateTime(), server_default=func.now(), nullable=False),
    Column("forwarded_on", DateTime(), nullable=True),
)

users_tickets_forward_association = Table(
    "users_tickets_forward_association",
    Base.metadata,
    Column("from_user_id", ForeignKey(User.id_, ondelete="CASCADE"), primary_key=True),
    Column("to_user_id", ForeignKey(User.id_, ondelete="CASCADE"), primary_key=True),
    Column("ticket_id", ForeignKey("tickets.id", ondelete="CASCADE"), primary_key=True),
    Column("forwarded_on", DateTime(), server_default=func.now(), nullable=True),
)


class UsersTicketsAssociation(Base):
    """
    Association table between users and tickets
    """

    __tablename__ = "users_tickets_association"
    __table_args__ = {"extend_existing": True}

    id_ = Column("id", Integer, Identity(), primary_key=True)

    user_id = Column(
        Integer,
        ForeignKey("users.id", ondelete="CASCADE"),
        primary_key=True,
        nullable=False,
    )
    ticket_id = Column(
        Integer,
        ForeignKey("tickets.id", ondelete="CASCADE"),
        primary_key=True,
        nullable=False,
    )
    accepted_on = Column(DateTime(), server_default=func.now(), nullable=False)
    forwarded_on = Column(DateTime(), nullable=True)

    user: User = relationship(User)
    ticket = relationship("Ticket", back_populates="handled_by")


class UsersTicketsForwardAssociation(Base):
    __tablename__ = "users_tickets_forward_association"
    __table_args__ = {"extend_existing": True}

    from_user_id = Column(
        ForeignKey(User.id_, ondelete="CASCADE"), primary_key=True, nullable=False
    )
    to_user_id = Column(
        ForeignKey(User.id_, ondelete="CASCADE"), primary_key=True, nullable=False
    )
    ticket_id = Column(
        ForeignKey("tickets.id", ondelete="CASCADE"), primary_key=True, nullable=False
    )
    forwarded_on = Column(DateTime(), server_default=func.now(), nullable=False)
    from_user: User = relationship(User, foreign_keys=[from_user_id])
    to_user: User = relationship(User, foreign_keys=[to_user_id])
    ticket = relationship("Ticket", back_populates="forwarded_to")
