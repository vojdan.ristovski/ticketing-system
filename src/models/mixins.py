from sqlalchemy import Boolean, Column, DateTime, Identity, Integer, inspect
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.orm import Mapped, declarative_mixin
from sqlalchemy.sql import func
from sqlalchemy.sql.expression import false


@declarative_mixin
class BaseDbModel:
    id_ = Column("id", Integer, Identity(), primary_key=True)
    created_at = Column(DateTime, server_default=func.now())
    updated_at = Column(DateTime, server_default=func.now(), onupdate=func.now())
    deleted_at = Column(DateTime, nullable=True)
    deleted = Column(Boolean, default=false())

    def _asdict(self):
        return {c.key: getattr(self, c.key) for c in inspect(self).mapper.column_attrs}

    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.id_})>"

    @declared_attr
    def __tablename__(cls) -> Mapped[str]:
        return f"{cls.__name__.lower()}s"
