from fastapi import APIRouter, Depends, status
from schemas.andon.base import Page
from schemas.andon.zone import CreateZoneSchema, ReadZoneSchema, UpdateZoneSchema

from src.models.response import StatusResponseModel
from src.service.zones import ZoneService, get_zone_service

router = APIRouter(prefix="/zones", tags=["Zones"])


@router.get("/", response_model=Page[ReadZoneSchema])
async def get_zones(
    page: int = 0,
    page_size: int = 10,
    zone_service: ZoneService = Depends(get_zone_service),
):
    return zone_service.list_entries(page, page_size)


@router.get("/{zone_id}", response_model=ReadZoneSchema)
async def get_zone_by_id(
    zone_id: int,
    zone_service: ZoneService = Depends(get_zone_service),
):
    return zone_service.get_by_id(zone_id)


@router.patch("/{zone_id}", response_model=ReadZoneSchema)
async def update_zone_by_id(
    zone_id: int,
    zone_in: UpdateZoneSchema,
    zone_service: ZoneService = Depends(get_zone_service),
):
    return zone_service.update_one(zone_id, zone_in)


@router.post("/", response_model=ReadZoneSchema, status_code=status.HTTP_201_CREATED)
async def create_zone(
    zone_in: CreateZoneSchema,
    zone_service: ZoneService = Depends(get_zone_service),
):
    return zone_service.create(zone_in)


@router.delete(
    "/{zone_id}",
    response_model=StatusResponseModel,
    status_code=status.HTTP_202_ACCEPTED,
)
async def delete_zone(
    zone_id: int,
    zone_service: ZoneService = Depends(get_zone_service),
):
    zone_service.delete(zone_id)
    return StatusResponseModel(
        status=202, message=f"Zone with id={zone_id} has been deleted"
    )
