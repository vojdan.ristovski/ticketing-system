import os
from typing import List, Optional

from fastapi import APIRouter, Body, Depends, File, Query, status
from schemas.andon.base import Page
from schemas.andon.enums import MachineOrderByEnum, StatusEnum, TicketOrderByEnum
from schemas.andon.machines import (
    CreateMachineSchema,
    CreateMachineTypeSchema,
    ReadMachineSchema,
    ReadMachineTypeSchema,
    UpdateMachineSchema,
    UpdateMachineTypeSchema,
)
from schemas.andon.tickets import ReadTicketSchema

from src.config import get_config
from src.models.enums import OrderDirection
from src.models.response import StatusResponseModel
from src.service.machines import (
    MachineService,
    MachineTypeService,
    get_machine_service,
    get_machine_type_service,
)
from src.service.tickets import TicketService, get_ticket_service

router = APIRouter(prefix="/machines")


@router.get("/types", response_model=Page[ReadMachineTypeSchema], tags=["Machine Types"])
async def get_machine_types(
    page_number: int = 0,
    page_size: int = 10,
    machine_type_service: MachineTypeService = Depends(get_machine_type_service),
):
    return machine_type_service.list_entries(page_number, page_size)


@router.post(
    "/types",
    response_model=ReadMachineTypeSchema,
    status_code=status.HTTP_201_CREATED,
    tags=["Machine Types"],
)
async def create_machine_type(
    machine_type_in: CreateMachineTypeSchema,
    machine_type_service: MachineTypeService = Depends(get_machine_type_service),
):
    return machine_type_service.create(machine_type_in)


@router.patch(
    "/types/{machine_type_id}",
    response_model=ReadMachineTypeSchema,
    status_code=status.HTTP_201_CREATED,
    tags=["Machine Types"],
)
async def update_machine_type(
    machine_type_id: int,
    machine_type_in: UpdateMachineTypeSchema,
    machine_type_service: MachineTypeService = Depends(get_machine_type_service),
):
    return machine_type_service.update_one(machine_type_id, machine_type_in)


@router.get(
    "/types/{machine_type_id}",
    response_model=ReadMachineTypeSchema,
    tags=["Machine Types"],
)
async def get_machine_type_by_id(
    machine_type_id: int,
    machine_type_service: MachineTypeService = Depends(get_machine_type_service),
):
    return machine_type_service.get_by_id(machine_type_id)


@router.get("/", response_model=Page[ReadMachineSchema], tags=["Machines"])
async def get_machines(
    page_number: int = 0,
    page_size: int = 10,
    order_by: List[MachineOrderByEnum] = Query([MachineOrderByEnum.NAME]),
    machine_service: MachineService = Depends(get_machine_service),
):
    return machine_service.list_entries(page_number, page_size, order_by)  # type: ignore


@router.post(
    "/",
    response_model=ReadMachineSchema,
    status_code=status.HTTP_201_CREATED,
    tags=["Machines"],
)
async def create_machine(
    machine_in: CreateMachineSchema,
    machine_service: MachineService = Depends(get_machine_service),
):
    return machine_service.create(machine_in)


@router.get("/{machine_id}", response_model=ReadMachineSchema, tags=["Machines"])
async def get_machine_by_id(
    machine_id: int,
    machine_service: MachineService = Depends(get_machine_service),
):
    return machine_service.get_by_id(machine_id)


@router.patch("/{machine_id}", response_model=ReadMachineSchema, tags=["Machines"])
async def update_machine(
    machine_id: int,
    machine_in: UpdateMachineSchema,
    machine_service: MachineService = Depends(get_machine_service),
):
    return machine_service.update_one(machine_id, machine_in)


@router.delete(
    "/{machine_id}",
    response_model=StatusResponseModel,
    status_code=status.HTTP_202_ACCEPTED,
    tags=["Machines"],
)
async def delete_machine_by_id(
    machine_id: int,
    machine_service: MachineService = Depends(get_machine_service),
):
    machine_service.delete(machine_id)
    return StatusResponseModel(
        status=202, message=f"Machine with id={machine_id} has been deleted"
    )


@router.get(
    "/{machine_id}/tickets", response_model=Page[ReadTicketSchema], tags=["Machines"]
)
async def get_machine_tickets(
    machine_id: int,
    page_number: int = 0,
    page_size: int = 10,
    sort_by: List[TicketOrderByEnum] = Query([TicketOrderByEnum.CREATED_AT]),
    order: OrderDirection = Query(OrderDirection.ASCENDING),
    status: Optional[List[StatusEnum]] = Query(None),
    ticket_service: TicketService = Depends(get_ticket_service),
):
    return ticket_service.find_by_machine_id(
        machine_id, page_number, page_size, status, order_by=sort_by, order=order
    )


@router.post(
    "/{machine_id}/image",
)
async def upload_machine_image(
    machine_id: int,
    image: bytes = File(...),
    filename: str = Body(...),
    machine_service: MachineService = Depends(get_machine_service),
):
    if not os.path.exists(get_config().public_folder):
        os.mkdir(get_config().public_folder)

    with open(get_config().public_folder + filename, "wb") as f:
        f.write(image)

    return machine_service.update_one(machine_id, UpdateMachineSchema(image=filename))
