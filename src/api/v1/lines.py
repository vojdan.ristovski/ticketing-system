from datetime import datetime
from typing import List, Optional

from fastapi import APIRouter, Depends, Query, status
from schemas.andon.base import Page
from schemas.andon.enums import (
    LineOrderByEnum,
    MachineOrderByEnum,
    ProductOrderByEnum,
    SeriesStatusEnum,
    StatusEnum,
    TicketOrderByEnum,
    TicketTypeEnum,
)
from schemas.andon.lines import CreateLineSchema, ReadLineSchema, UpdateLineSchema
from schemas.andon.machines import ReadMachineSchema
from schemas.andon.products import ReadProductSchema
from schemas.andon.tickets import ReadTicketSchema
from schemas.andon.users import ReadUserSchema

from src.models.enums import OrderDirection
from src.models.response import StatusResponseModel
from src.schema.time_series import ReadTimeSeriesSchema
from src.service.lines import LineService, get_line_service
from src.service.machines import MachineService, get_machine_service
from src.service.products import ProductService, get_product_service
from src.service.tickets import TicketService, get_ticket_service
from src.service.users import UserService, get_user_service

router = APIRouter(prefix="/lines", tags=["Lines"])


@router.get("/", response_model=Page[ReadLineSchema])
async def get_lines(
    page: int = 0,
    page_size: int = 10,
    order_by: List[LineOrderByEnum] = Query([LineOrderByEnum.NAME]),
    line_service: LineService = Depends(get_line_service),
):
    return line_service.list_entries(page, page_size, order_by)  # type: ignore


@router.get("/{line_id}", response_model=ReadLineSchema)
async def get_line_by_id(
    line_id: int,
    line_service: LineService = Depends(get_line_service),
):
    return line_service.get_by_id(line_id)


@router.get("/{line_id}/machines", response_model=Page[ReadMachineSchema])
async def get_machines_by_line_id(
    line_id: int,
    page: int = 0,
    page_size: int = 10,
    order_by: List[MachineOrderByEnum] = Query([MachineOrderByEnum.ORDER]),
    machine_service: MachineService = Depends(get_machine_service),
):
    return machine_service.get_by_line_id(line_id, page, page_size, order_by)


@router.get("/{line_id}/products", response_model=Page[ReadProductSchema])
async def get_products_by_line_id(
    line_id: int,
    page: int = 0,
    page_size: int = 10,
    order_by: List[ProductOrderByEnum] = Query([ProductOrderByEnum.NAME]),
    product_service: ProductService = Depends(get_product_service),
):
    return product_service.get_by_line_id(line_id, page, page_size, order_by)


@router.get("/{line_id}/tickets", response_model=Page[ReadTicketSchema])
async def get_tickets_by_line_id(
    line_id: int,
    page: int = 0,
    page_size: int = 10,
    sort_by: List[TicketOrderByEnum] = Query([TicketOrderByEnum.CREATED_AT]),
    order: OrderDirection = Query(OrderDirection.ASCENDING),
    status: Optional[List[StatusEnum]] = Query(None),
    type: Optional[List[TicketTypeEnum]] = Query(None),
    reason_id: Optional[int] = Query(None),
    start_date: Optional[datetime] = Query(None),
    end_date: Optional[datetime] = Query(None),
    ticket_service: TicketService = Depends(get_ticket_service),
):
    return ticket_service.get_by_line_id(
        line_id,
        page,
        page_size,
        status,
        type,
        reason_id,
        order_by=sort_by,
        order=order,
        start_date=start_date,
        end_date=end_date,
    )


@router.patch("/{line_id}", response_model=ReadLineSchema)
async def patch_line_by_id(
    line_id: int,
    line_in: UpdateLineSchema,
    line_service: LineService = Depends(get_line_service),
):
    return line_service.update_one(line_id, line_in)


@router.post("/", response_model=ReadLineSchema, status_code=status.HTTP_201_CREATED)
async def create_line(
    line_in: CreateLineSchema,
    line_service: LineService = Depends(get_line_service),
):
    return line_service.create(line_in)


@router.delete(
    "/{line_id}",
    status_code=status.HTTP_202_ACCEPTED,
    response_model=StatusResponseModel,
)
async def delete_line_by_id(
    line_id: int,
    line_service: LineService = Depends(get_line_service),
):
    line_service.delete_line(line_id)
    return StatusResponseModel(
        status=202, message=f"Line with id={line_id} has been deleted"
    )


@router.post("/{line_id}/inactivate", response_model=ReadTimeSeriesSchema)
async def inactivate_line(
    line_id: int,
    line_service: LineService = Depends(get_line_service),
):
    return line_service.change_line_status(line_id, SeriesStatusEnum.NOT_RUNNING)


@router.post("/{line_id}/activate", response_model=ReadTimeSeriesSchema)
async def activate_line(
    line_id: int,
    line_service: LineService = Depends(get_line_service),
):
    return line_service.change_line_status(line_id, SeriesStatusEnum.RUNNING)


@router.get("/{line_id}/users", response_model=Page[ReadUserSchema])
async def get_users_by_line_id(
    line_id: int,
    page: int = 0,
    page_size: int = 10,
    user_service: UserService = Depends(get_user_service),
):
    return user_service.get_by_line_id(line_id, page, page_size)


@router.get("/{line_id}/users/{role_id}", response_model=List[ReadUserSchema])
async def get_users_by_line_id_and_role_id(
    line_id: int,
    role_id: int,
    user_service: UserService = Depends(get_user_service),
):
    return user_service.get_by_line_id_and_role_id(line_id, role_id)


@router.put("/{line_id}/add_users", response_model=ReadLineSchema)
async def add_users_to_line(
    line_id: int,
    users: List[int],
    line_service: LineService = Depends(get_line_service),
):
    return line_service.add_users(line_id, users)


@router.post("/{line_id}/set_users", response_model=ReadLineSchema)
async def set_users_to_line(
    line_id: int,
    users: List[int],
    line_service: LineService = Depends(get_line_service),
):
    return line_service.set_users(line_id, users)
