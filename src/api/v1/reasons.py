from fastapi import APIRouter, Depends, status
from schemas.andon.base import Page
from schemas.andon.reasons import (
    CreateReasonSchema,
    ReadReasonSchema,
    UpdateReasonSchema,
)

from src.models.response import StatusResponseModel
from src.service.reasons import ReasonService, get_reason_service

router = APIRouter(prefix="/reasons", tags=["Reasons"])


@router.get("/", response_model=Page[ReadReasonSchema])
async def get_reasons(
    page: int = 0,
    page_size: int = 10,
    reason_service: ReasonService = Depends(get_reason_service),
):
    return reason_service.list_entries(page, page_size)


@router.get("/{reason_id}", response_model=ReadReasonSchema)
async def get_reason_by_id(
    reason_id: int,
    reason_service: ReasonService = Depends(get_reason_service),
):
    return reason_service.get_by_id(reason_id)


@router.post("/", response_model=ReadReasonSchema, status_code=status.HTTP_201_CREATED)
async def create_reason(
    reason_in: CreateReasonSchema,
    reason_service: ReasonService = Depends(get_reason_service),
):
    return reason_service.create(reason_in)


@router.delete(
    "/{reason_id}",
    response_model=StatusResponseModel,
    status_code=status.HTTP_202_ACCEPTED,
)
async def delete_reason(
    reason_id: int, reason_service: ReasonService = Depends(get_reason_service)
):
    reason_service.delete(reason_id)
    return StatusResponseModel(
        status=202, message=f"Reason with id={reason_id} has been deleted"
    )


@router.patch(
    "/{reason_id}", response_model=ReadReasonSchema, status_code=status.HTTP_202_ACCEPTED
)
async def update_reason(
    reason_id: int,
    reason_in: UpdateReasonSchema,
    reason_service: ReasonService = Depends(get_reason_service),
):
    return reason_service.update(reason_id, reason_in)
