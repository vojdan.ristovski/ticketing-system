from fastapi import APIRouter, Depends
from fastapi.encoders import jsonable_encoder
from schemas.andon.events_config import EventsConfig
from schemas.andon.lines import ReadLineSchema

from src.service.lines import LineService, get_line_service

router = APIRouter(prefix="/config", tags=["Config"])


@router.post("/", response_model=ReadLineSchema)
async def update_events_config(
    line_id: int,
    events_config: EventsConfig,
    line_service: LineService = Depends(get_line_service),
):
    json_events_config = jsonable_encoder(events_config)
    return line_service.update_line_config(
        line_id=line_id, event_config=json_events_config
    )
