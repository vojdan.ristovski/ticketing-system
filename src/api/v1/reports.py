from typing import Any, List

from fastapi import APIRouter, Depends
from schemas.andon.reports import AverageReport, LineReport, UserReport

from src.schema.time_series import ReadTimeSeriesSchema
from src.service.reports import ReportsService, get_reports_service

router = APIRouter(prefix="/reports", tags=["Reports"])


@router.get("/", response_model=List)
def get_reports(report_service: ReportsService = Depends(get_reports_service)):
    return report_service.get_reports()


@router.get("/average-reaction-time/{user_id}", response_model=AverageReport)
def get_average_reaction_time(
    user_id: int,
    ticket_service: ReportsService = Depends(get_reports_service),
):
    return ticket_service.get_average_reaction_time(user_id)


@router.get("/average-time-to-handle-ticket/{user_id}", response_model=AverageReport)
def get_average_time_to_handle_ticket(
    user_id: int,
    ticket_service: ReportsService = Depends(get_reports_service),
):
    return ticket_service.get_average_time_to_handle_ticket(user_id)


@router.get("/user_report/{user_id}", response_model=UserReport)
def get_user_report(
    user_id: int,
    ticket_service: ReportsService = Depends(get_reports_service),
):
    return ticket_service.get_user_report(user_id)


@router.get("/machine_reports", response_model=List[Any])
def get_machine_reports(
    report_service: ReportsService = Depends(get_reports_service),
):
    return report_service.get_machines_reports()


@router.get("/line_reports", response_model=List[LineReport])
def get_line_reports(
    report_service: ReportsService = Depends(get_reports_service),
):
    return report_service.get_line_reports()


@router.get("/time-series/{line_id}", response_model=List[ReadTimeSeriesSchema])
def get_time_series(
    line_id: int, report_service: ReportsService = Depends(get_reports_service)
):
    return report_service.get_time_series(line_id)
