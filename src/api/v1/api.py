from fastapi import APIRouter

from src.api.v1 import (
    auth,
    components,
    events_config,
    lines,
    machines,
    products,
    reasons,
    reports,
    tickets,
    users,
    zones,
)

router = APIRouter()
router.include_router(zones.router)
router.include_router(lines.router)
router.include_router(machines.router)
router.include_router(products.router)
router.include_router(tickets.router)
router.include_router(users.router)
router.include_router(reports.router)
router.include_router(events_config.router)
router.include_router(components.router)
router.include_router(reasons.router)
router.include_router(auth.router)
