from fastapi import APIRouter, Depends
from fastapi.security import OAuth2PasswordRequestForm
from schemas.andon.auth import ChangePasswordSchema, TokenResponseSchema

from src.models.response import StatusResponseModel
from src.service.auth import AuthService, get_auth_service

router = APIRouter(prefix="/auth", tags=["Authentication"])


@router.post("/login", response_model=TokenResponseSchema)
async def login(
    form_data: OAuth2PasswordRequestForm = Depends(),
    auth_service: AuthService = Depends(get_auth_service),
):
    return auth_service.login(form_data.username, form_data.password)


@router.post("/change-password", response_model=StatusResponseModel)
async def change_password(
    change_password_in: ChangePasswordSchema,
    auth_service: AuthService = Depends(get_auth_service),
):
    return auth_service.change_password(change_password_in)
