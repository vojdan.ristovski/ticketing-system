from fastapi import APIRouter, Depends, status
from schemas.andon.base import Page
from schemas.andon.component import CreateComponentSchema, ReadComponentSchema

from src.models.response import StatusResponseModel
from src.service.components import ComponentService, get_component_service

router = APIRouter(prefix="/components", tags=["Components"])


@router.get("/", response_model=Page[ReadComponentSchema])
async def get_components(
    page: int = 0,
    page_size: int = 10,
    component_service: ComponentService = Depends(get_component_service),
):
    return component_service.list_entries(page, page_size)


@router.get("/{component_id}", response_model=ReadComponentSchema)
async def get_component_by_id(
    component_id: int,
    component_service: ComponentService = Depends(get_component_service),
):
    return component_service.get_by_id(component_id)


@router.post(
    "/", response_model=ReadComponentSchema, status_code=status.HTTP_201_CREATED
)
async def create_component(
    component_in: CreateComponentSchema,
    component_service: ComponentService = Depends(get_component_service),
):
    return component_service.create(component_in)


@router.delete(
    "/{component_id}",
    response_model=StatusResponseModel,
    status_code=status.HTTP_202_ACCEPTED,
)
async def delete_component(
    component_id: int,
    component_service: ComponentService = Depends(get_component_service),
):
    component_service.delete(component_id)
    return StatusResponseModel(
        status=202, message=f"Component with id={component_id} has been deleted"
    )
