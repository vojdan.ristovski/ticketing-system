from typing import List

from fastapi import APIRouter, Depends, status
from pydantic.networks import EmailStr
from schemas.andon.base import Page
from schemas.andon.tickets import ReadTicketSchema
from schemas.andon.users import (
    CreateRoleSchema,
    CreateUserSchema,
    ReadRoleSchema,
    ReadUserSchema,
    UpdateRoleSchema,
    UpdateUserSchema,
)

from src.models.response import StatusResponseModel
from src.service.tickets import TicketService, get_ticket_service
from src.service.users import (
    UserRoleService,
    UserService,
    get_user_role_service,
    get_user_service,
)

router = APIRouter(prefix="/users")


@router.get("/roles", response_model=Page[ReadRoleSchema], tags=["User Roles"])
async def get_user_roles(
    page_number: int = 0,
    page_size: int = 10,
    user_role_service: UserRoleService = Depends(get_user_role_service),
):
    return user_role_service.list_entries(page_number, page_size)


@router.get("/roles/{role_id}", response_model=ReadRoleSchema, tags=["User Roles"])
async def get_user_role(
    role_id: int, user_role_service: UserRoleService = Depends(get_user_role_service)
):
    return user_role_service.get_by_id(role_id)


@router.post("/roles", response_model=ReadRoleSchema, tags=["User Roles"])
async def create_user_role(
    role_in: CreateRoleSchema,
    user_role_service: UserRoleService = Depends(get_user_role_service),
):
    return user_role_service.create(role_in)


@router.patch("/roles/{role_id}", response_model=ReadRoleSchema, tags=["User Roles"])
async def update_user_role(
    role_id: int,
    role_in: UpdateRoleSchema,
    user_role_service: UserRoleService = Depends(get_user_role_service),
):
    return user_role_service.update_one(role_id, role_in)


@router.get("/", response_model=Page[ReadUserSchema], tags=["Users"])
async def get_users(
    page: int = 0,
    page_size: int = 10,
    user_service: UserService = Depends(get_user_service),
):
    return user_service.list_entries(page, page_size)


@router.get("/{user_id}", response_model=ReadUserSchema, tags=["Users"])
async def get_user_by_id(
    user_id: int,
    user_service: UserService = Depends(get_user_service),
):
    return user_service.get_by_id(user_id)


@router.get("/e/{email}", response_model=ReadUserSchema, tags=["Users"])
async def get_user_by_email(
    email: EmailStr,
    user_service: UserService = Depends(get_user_service),
):
    return user_service.get_by_email(email)


@router.get("/u/{username}", response_model=ReadUserSchema, tags=["Users"])
async def get_user_by_username(
    username: str,
    user_service: UserService = Depends(get_user_service),
):
    return user_service.get_by_username(username)


@router.get("/r/{role}", response_model=List[ReadUserSchema], tags=["Users"])
async def get_user_by_role(
    role: int,
    user_service: UserService = Depends(get_user_service),
):
    return user_service.get_by_role(role)


@router.post(
    "/",
    response_model=ReadUserSchema,
    status_code=status.HTTP_201_CREATED,
    tags=["Users"],
)
async def create_user(
    user_in: CreateUserSchema,
    user_service: UserService = Depends(get_user_service),
):
    return user_service.create(user_in)


@router.patch("/{user_id}", response_model=ReadUserSchema, tags=["Users"])
async def update_user(
    user_id: int,
    user_in: UpdateUserSchema,
    user_service: UserService = Depends(get_user_service),
):
    return user_service.update(user_id, user_in)


@router.delete(
    "/{user_id}",
    response_model=StatusResponseModel,
    status_code=status.HTTP_202_ACCEPTED,
    tags=["Users"],
)
async def delete_user_by_id(
    user_id: int,
    user_service: UserService = Depends(get_user_service),
):
    user_service.delete(user_id)
    return StatusResponseModel(
        status=202, message=f"User with id={user_id} has been deleted"
    )


@router.get("/c/{card_id}", response_model=ReadUserSchema, tags=["Users"])
def get_user_by_card_id(
    card_id: str,
    user_service: UserService = Depends(get_user_service),
):
    return user_service.get_by_card_id(card_id)


@router.get(
    "/{user_id}/tickets",
    response_model=Page[ReadTicketSchema],
    tags=["Users"],
)
async def get_tickets_by_user_id(
    user_id: int,
    page: int = 0,
    page_size: int = 10,
    ticket_service: TicketService = Depends(get_ticket_service),
):
    return ticket_service.find_by_user_id(user_id, page, page_size)
