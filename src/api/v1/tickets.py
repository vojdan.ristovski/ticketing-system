from datetime import datetime
from typing import List, Optional

from fastapi import APIRouter, Body, Depends, Query, status
from schemas.andon.base import Page
from schemas.andon.enums import TicketOrderByEnum
from schemas.andon.tickets import (
    CreateTicketSchema,
    ReadTicketSchema,
    UpdateTicketSchema,
)

from src.models.response import StatusResponseModel
from src.schema.tickets import QueryTicketSchema
from src.service.tickets import TicketService, get_ticket_service

router = APIRouter(prefix="/tickets", tags=["Tickets"])


@router.get(
    "/export", status_code=status.HTTP_202_ACCEPTED, response_model=StatusResponseModel
)
def export(
    ticket_service: TicketService = Depends(get_ticket_service),
    user_id: int = Query(...),
):
    ticket_service.export_to_csv(user_id)
    return StatusResponseModel(status=202, message="Export queued")


@router.get("/", response_model=Page[ReadTicketSchema])
async def get_tickets(
    page: int = 0,
    page_size: int = 10,
    query: QueryTicketSchema = Depends(QueryTicketSchema),
    order_by: List[TicketOrderByEnum] = Query([TicketOrderByEnum.CREATED_AT]),
    ticket_service: TicketService = Depends(get_ticket_service),
):
    return ticket_service.list_entries(
        page, page_size, order_by=order_by, query=query  # type: ignore
    )


@router.post("/", response_model=ReadTicketSchema, status_code=status.HTTP_201_CREATED)
async def create_ticket(
    ticket_in: CreateTicketSchema,
    ticket_service: TicketService = Depends(get_ticket_service),
):
    return ticket_service.create(ticket_in)


@router.get("/{ticket_id}", response_model=ReadTicketSchema)
async def get_ticket_by_id(
    ticket_id: int,
    ticket_service: TicketService = Depends(get_ticket_service),
):
    ticket = ticket_service.get_by_id(ticket_id)
    return ticket


@router.patch("/{ticket_id}", response_model=ReadTicketSchema)
async def update_ticket(
    ticket_id: int,
    ticket_in: UpdateTicketSchema,
    card_id: Optional[str] = None,
    ticket_service: TicketService = Depends(get_ticket_service),
):
    ticket = ticket_service.update(ticket_id, ticket_in, card_id)
    return ticket


@router.post("/{ticket_id}/close", response_model=ReadTicketSchema)
async def close_ticket(
    ticket_id: int,
    card_id: str = Body(...),
    corrective_action: Optional[str] = Body(default=None),
    issue_description: Optional[str] = Body(default=None),
    ticket_service: TicketService = Depends(get_ticket_service),
):
    ticket = ticket_service.close(
        ticket_id=ticket_id,
        card_id=card_id,
        corrective_action=corrective_action,
        issue_description=issue_description,
    )
    return ticket


@router.post("/{ticket_id}/accept", response_model=ReadTicketSchema)
async def accept_ticket(
    ticket_id: int,
    card_id: str = Body(..., embed=True),
    ticket_service: TicketService = Depends(get_ticket_service),
):
    ticket = ticket_service.accept(ticket_id, card_id)
    return ticket


@router.post("/{ticket_id}/forward", response_model=ReadTicketSchema)
async def forward_ticket(
    ticket_id: int,
    from_card_id: str = Body(..., embed=True),
    to_card_id: str = Body(..., embed=True),
    ticket_service: TicketService = Depends(get_ticket_service),
):
    ticket = ticket_service.forward(ticket_id, from_card_id, to_card_id)
    return ticket


@router.post("/{ticket_id}/forward_to_on_call_engineer", response_model=ReadTicketSchema)
async def forward_ticket_to_on_call_engineer(
    ticket_id: int,
    from_card_id: str = Body(..., embed=True),
    ticket_service: TicketService = Depends(get_ticket_service),
):
    ticket = ticket_service.forward_to_on_call_engineer(ticket_id, from_card_id)
    return ticket


@router.post("/{ticket_id}/prioritize", response_model=ReadTicketSchema)
async def prioritize_ticket(
    ticket_id: int,
    card_id: str = Body(..., embed=True),
    ticket_service: TicketService = Depends(get_ticket_service),
):
    ticket = ticket_service.prioritize(ticket_id, card_id)
    return ticket


@router.post("/{ticket_id}/pause", response_model=ReadTicketSchema)
async def pause_ticket(
    ticket_id: int,
    card_id: str = Body(..., embed=True),
    ticket_service: TicketService = Depends(get_ticket_service),
):
    ticket = ticket_service.pause(ticket_id, card_id)
    return ticket


@router.post("/{ticket_id}/material_not_available", response_model=ReadTicketSchema)
async def material_not_available(
    ticket_id: int,
    card_id: str = Body(..., embed=True),
    ticket_service: TicketService = Depends(get_ticket_service),
):
    ticket = ticket_service.material_not_available(ticket_id, card_id)
    return ticket


@router.post("/{ticket_id}/approve_planned_action", response_model=ReadTicketSchema)
async def approve_planned_action(
    ticket_id: int,
    card_id: str = Body(..., embed=True),
    datetime_from: datetime = Body(...),
    datetime_to: datetime = Body(...),
    ticket_service: TicketService = Depends(get_ticket_service),
):
    ticket = ticket_service.approve_planned_action(
        ticket_id, card_id, datetime_from, datetime_to
    )
    return ticket


@router.post("/{ticket_id}/reject_planned_action", response_model=ReadTicketSchema)
async def reject_planned_action(
    ticket_id: int,
    card_id: str = Body(..., embed=True),
    ticket_service: TicketService = Depends(get_ticket_service),
):
    ticket = ticket_service.reject_planned_action(ticket_id, card_id)
    return ticket
