from fastapi import APIRouter, Depends, status
from schemas.andon.base import Page
from schemas.andon.products import (
    CreateProductSchema,
    ReadProductSchema,
    UpdateProductSchema,
)

from src.models.response import StatusResponseModel
from src.service.products import ProductService, get_product_service

router = APIRouter(prefix="/products", tags=["Products"])


@router.get("/", response_model=Page[ReadProductSchema])
async def get_products(
    page_number: int = 0,
    page_size: int = 10,
    product_service: ProductService = Depends(get_product_service),
):
    return product_service.list_entries(page_number, page_size)


@router.post("/", response_model=ReadProductSchema, status_code=status.HTTP_201_CREATED)
async def create_product(
    product_in: CreateProductSchema,
    product_service: ProductService = Depends(get_product_service),
):
    return product_service.create(product_in)


@router.get("/{product_id}", response_model=ReadProductSchema)
async def get_product_by_id(
    product_id: int,
    product_service: ProductService = Depends(get_product_service),
):
    return product_service.get_by_id(product_id)


@router.delete(
    "/{product_id}",
    response_model=StatusResponseModel,
    status_code=status.HTTP_202_ACCEPTED,
)
async def delete_product_by_id(
    product_id: int,
    product_service: ProductService = Depends(get_product_service),
):
    product_service.delete(product_id)
    return StatusResponseModel(
        status=202, message=f"Product with id={product_id} has been deleted"
    )


@router.patch("/{product_id}", response_model=ReadProductSchema)
async def update_product_by_id(
    product_id: int,
    product_in: UpdateProductSchema,
    product_service: ProductService = Depends(get_product_service),
):
    return product_service.update(product_id, product_in)
