from typing import List, Optional

from sqlalchemy import and_, delete, func, insert, select
from sqlalchemy.orm import Session

from src.models.tickets import users_lines_association
from src.models.users import Role, User, users_roles_association
from src.repository.base import BaseRepository


class UserRoleRepository(BaseRepository[Role]):
    _table = Role


class UserRepository(BaseRepository[User]):
    """
    Users repository
    """

    _table = User

    def find_by_email(self, email: str, session: Session) -> Optional[User]:
        return self.find_one_by_attr("email", email, session=session)

    def find_by_username(self, username: str, session: Session) -> Optional[User]:
        return self.find_one_by_attr("username", username, session=session)

    def insert_role_to_user(self, user_id: int, role_id: int, session: Session) -> None:
        statement = insert(users_roles_association).values(
            user_id=user_id, role_id=role_id
        )
        session.execute(statement)
        return None

    def purge_user_roles(self, user_id: int, session: Session) -> None:
        statement = delete(users_roles_association).where(
            users_roles_association.c.user_id == user_id
        )
        session.execute(statement)
        return None

    def find_by_role(self, role_id: int, session: Session) -> List[User]:
        statement = select(self._table).where(
            self._table.id_.in_(
                select([users_roles_association.c.user_id]).where(
                    users_roles_association.c.role_id == role_id
                )
            )
        )
        user = session.execute(statement).scalars().unique().all()
        return user

    def get_users_by_line(
        self, line_id: int, page_number: int, page_size: int, session: Session
    ) -> List[User]:
        statement = select(self._table).where(
            self._table.id_.in_(
                select([users_lines_association.c.user_id]).where(
                    users_lines_association.c.line_id == line_id
                )
            )
        )
        user = session.execute(statement).scalars().unique().all()
        return user

    def get_users_by_role_and_line(
        self, role_id: int, line_id: int, session: Session
    ) -> List[User]:
        statement = select(self._table).where(
            and_(
                self._table.id_.in_(
                    select([users_lines_association.c.user_id]).where(
                        users_lines_association.c.line_id == line_id
                    )
                ),
                self._table.id_.in_(
                    select([users_roles_association.c.user_id]).where(
                        users_roles_association.c.role_id == role_id
                    )
                ),
            )
        )
        users = session.execute(statement).scalars().unique().all()
        return users

    def count_by_line_id(self, line_id: int, session: Session) -> int:
        """
        Count all machines by line id
        """
        statement = select(func.count(self._table.id_)).where(
            self._table.id_.in_(
                select([users_lines_association.c.user_id]).where(
                    users_lines_association.c.line_id == line_id
                )
            )
        )
        count = session.execute(statement).scalar()
        return count or 0


def get_user_role_repository() -> UserRoleRepository:
    """
    Get user repository
    """
    return UserRoleRepository()


def get_user_repository() -> UserRepository:
    """
    Get user repository
    """
    return UserRepository()
