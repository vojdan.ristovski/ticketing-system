from typing import List, Optional

from sqlalchemy import and_, func, insert, select
from sqlalchemy.orm import Session

from src.models.tickets import Machine, MachineType, tickets_machines_association
from src.repository.base import BaseRepository


class MachineTypeRepository(BaseRepository[MachineType]):
    """
    Machines repository
    """

    _table = MachineType


class MachineRepository(BaseRepository[Machine]):
    """
    Machines repository
    """

    _table = Machine

    def insert_ticket_on_machine(
        self,
        ticket_id: int,
        machine_id: int,
        session: Session,
    ):
        """
        Create a new many to many association
        """
        stmt = insert(
            tickets_machines_association,
        ).values(ticket_id=ticket_id, machine_id=machine_id)
        session.execute(stmt)
        return None

    def get_machines_by_ticket_id(
        self,
        ticket_id: int,
        session: Session,
    ) -> List[Machine]:
        statement = select(self._table).where(
            self._table.id_.in_(
                select([tickets_machines_association.c.machine_id]).where(
                    tickets_machines_association.c.ticket_id == ticket_id
                )
            )
        )
        entries = session.execute(statement).scalars().unique().all()
        return entries

    def find_by_line_id(
        self,
        line_id: int,
        page_number: int,
        page_size: int,
        order_by: List[str],
        session: Session,
    ) -> List[Machine]:
        """
        Find all machines by line id
        """
        statement = (
            select(self._table)
            .where(
                and_(
                    self._table.deleted.isnot(True),
                    self._table.line_id == line_id,
                )
            )
            .order_by(*[getattr(self._table, o) for o in order_by])
            .offset(page_number * page_size)
            .limit(page_size)
        )
        entries = session.execute(statement).unique().scalars().all()
        return entries

    def count_by_line_id(self, line_id: int, session: Session) -> Optional[int]:
        """
        Count all machines by line id
        """
        statement = select(func.count(self._table.id_)).where(
            and_(
                self._table.deleted.isnot(True),
                self._table.line_id == line_id,
            )
        )
        count = session.execute(statement).scalar()
        return count


def get_machine_type_repository() -> MachineTypeRepository:
    """
    Get Machine repository
    """
    return MachineTypeRepository()


def get_machine_repository() -> MachineRepository:
    """
    Get Machine repository
    """
    return MachineRepository()
