from src.models.tickets import Component
from src.repository.base import BaseRepository


class ComponentRepository(BaseRepository[Component]):
    """
    Components repository
    """

    _table = Component


def get_component_repository():
    return ComponentRepository()
