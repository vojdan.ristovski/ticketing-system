from src.models.tickets import Zone
from src.repository.base import BaseRepository


class ZoneRepository(BaseRepository[Zone]):
    """
    Zone repository
    """

    _table = Zone


def get_zone_repository() -> ZoneRepository:
    """
    Get Zone repository
    """
    return ZoneRepository()
