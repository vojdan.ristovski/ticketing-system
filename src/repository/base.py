import abc
from datetime import datetime
from typing import Any, Generic, Iterable, List, Optional, Tuple, Type, TypeVar

import pandas
from sqlalchemy import and_, func
from sqlalchemy.dialects.postgresql import insert
from sqlalchemy.orm import Session
from sqlalchemy.sql.expression import select, update

from src.models.enums import OrderDirection
from src.models.mixins import BaseDbModel

MODEL = TypeVar("MODEL", bound=BaseDbModel)


class BaseRepository(
    Generic[MODEL],
    metaclass=abc.ABCMeta,
):
    """
    Generic repository methods.
    """

    @property
    @abc.abstractmethod
    def _table(self) -> Type[MODEL]:
        ...

    def get_dataframe(self, engine):
        return pandas.read_sql_table(self._table.__tablename__, con=engine)

    def find_many(
        self,
        page_number: int,
        page_size: int,
        session: Session,
        order_by: Optional[List[str]] = None,
        query: dict = {},
    ) -> List[MODEL]:
        """
        List all entries in the table.
        """
        if order_by is None:
            order_by = ["id_"]
        statement = (
            select(self._table)
            .where(
                self._table.deleted.isnot(True),
                *[
                    getattr(self._table, k) == v
                    for k, v in query.items()
                    if v is not None
                ],
            )
            .order_by(*[getattr(self._table, item) for item in order_by])
            .offset(page_number * page_size)
            .limit(page_size)
        )
        entries = session.execute(statement).unique().scalars().all()
        return entries

    def find_one_by_id(self, id_: int, session: Session) -> Optional[MODEL]:
        """
        Get a single entry by id.
        """
        statement = (
            select(self._table)
            .where(and_(self._table.id_ == id_, self._table.deleted.isnot(True)))
            .limit(1)
        )
        return session.execute(statement).unique().scalar_one_or_none()

    def find_many_by_ids(self, ids: List[int], session: Session) -> List[MODEL]:
        """
        Get many entries by id.
        """
        statement = select(self._table).where(self._table.id_.in_(ids))
        entries = session.execute(statement).unique().scalars().all()
        return entries

    def insert_one(self, insert_object: MODEL, session: Session) -> MODEL:
        """
        Create a new entry.
        """
        session.add(insert_object)
        session.flush()
        session.refresh(insert_object)
        return insert_object

    def update_one_by_id(
        self,
        update_id: int,
        update_dict: dict,
        session: Session,
        upsert: bool = False,
    ) -> Optional[MODEL]:
        """
        Update or upsert an entry.
        """
        if upsert:
            # Upsert entry if upsert flag set to true
            insert_table = insert(self._table).values(**update_dict)
            statement = insert_table.on_conflict_do_update(
                constraint="id_",
                set_=update_dict,
            )
        else:
            statement = (
                update(self._table)
                .where(self._table.id_ == update_id)
                .values(**update_dict)
            )
        session.execute(statement)
        return self.find_one_by_id(update_id, session)

    def delete_one_by_id(self, id_: int, session: Session) -> None:
        """
        Soft delete an entry.
        """
        statement = (
            update(self._table)
            .values(
                deleted_at=datetime.utcnow(),
                deleted=True,
            )
            .where(self._table.id_ == id_)
        )
        session.execute(statement)

    def find_one_by_attr(
        self, attribute: str, value: str, session: Session
    ) -> Optional[MODEL]:
        """
        Get a single entry by attribute.
        """
        entry = session.execute(
            select(self._table).where(getattr(self._table, attribute) == value).limit(1)
        )
        return entry.unique().scalar_one_or_none()

    def find_many_by_attrs(
        self,
        page_number: int,
        page_size: int,
        *attrs: Tuple[str, Any],
        order_by: Optional[List[str]] = None,
        order: Optional[OrderDirection] = None,
        start_date: Optional[datetime] = None,
        end_date: Optional[datetime] = None,
        session: Session,
    ) -> List[MODEL]:
        """
        Get a multiple entries by attribute.
        """
        if order_by is None:
            order_by = ["id_"]
        if order is None:
            order = OrderDirection.ASCENDING
        start_date = start_date or datetime.min
        end_date = end_date or datetime.max
        statement = (
            select(self._table)
            .where(
                and_(
                    *[
                        getattr(self._table, attribute) == value
                        if not isinstance(value, Iterable)
                        else getattr(self._table, attribute).in_(value)
                        for (attribute, value) in attrs
                    ],
                    self._table.deleted.isnot(True),
                    self._table.created_at.between(start_date, end_date),
                )
            )
            .order_by(
                *[
                    (getattr(self._table, item).desc())
                    if order == OrderDirection.DESCENDING
                    else (getattr(self._table, item))
                    for item in order_by
                ]
            )
            .offset(page_number * page_size)
            .limit(page_size)
        )
        entry = session.execute(statement)
        return entry.unique().scalars().all()

    def count(self, session: Session):
        statement = select(func.count(self._table.id_)).where(
            self._table.deleted.isnot(True)
        )
        return session.execute(statement).scalar()

    def count_by_attrs(
        self,
        *attrs: Tuple[str, Any],
        start_date: Optional[datetime] = None,
        end_date: Optional[datetime] = None,
        session: Session,
    ):
        start_date = start_date or datetime.min
        end_date = end_date or datetime.max
        statement = select(func.count(self._table.id_)).where(
            self._table.deleted.isnot(True),
            *[
                getattr(self._table, attribute) == value
                if not isinstance(value, Iterable)
                else getattr(self._table, attribute).in_(value)
                for (attribute, value) in attrs
            ],
            self._table.created_at.between(start_date, end_date),
        )
        total = session.execute(statement).scalar()
        return total or 0
