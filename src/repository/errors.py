class EntryNotFoundError(Exception):
    """Raised when entity was not found in database."""
