from datetime import timedelta
from typing import Optional

from schemas.andon.enums import StatusEnum
from sqlalchemy import and_, case, func, or_
from sqlalchemy.orm import Session
from sqlalchemy.sql.expression import select

from src.models.tickets import Line, Ticket
from src.models.users import User, UsersTicketsAssociation


class ReportsRepository:
    def get_average_time_to_handle_ticket(
        self, user_id: int, session: Session
    ) -> Optional[timedelta]:
        """
        Get average time to handle ticket for a user.
        The average time is calculated for the finished tickets only.

        A finished ticket for a user is defined by either being forwarded or closed.

        The average is calculated from the time the ticket was accepted to the time the
        ticket was closed or forwarded.
        """
        case_statement = case(  # type: ignore
            [
                (
                    UsersTicketsAssociation.forwarded_on is not None,
                    UsersTicketsAssociation.forwarded_on,
                ),
            ],
            else_=Ticket.closed_at,
        )

        statement = (
            select([func.avg(case_statement - UsersTicketsAssociation.accepted_on)])
            .where(
                and_(
                    UsersTicketsAssociation.user_id == user_id,
                    or_(
                        UsersTicketsAssociation.forwarded_on is not None,
                        Ticket.closed_at is not None,
                    ),
                )
            )
            .join(
                Ticket,
                UsersTicketsAssociation.ticket_id == Ticket.id_,
            )
        )

        return session.execute(statement).one()[0]

    def get_average_reaction_time(
        self, user_id: int, session: Session
    ) -> Optional[timedelta]:
        """
        Get average reaction time for a user.
        The reaction time is the time between
        the ticket was created and the ticket was accepted.
        """
        statement = (
            select([func.avg(UsersTicketsAssociation.accepted_on - Ticket.created_at)])
            .join(
                Ticket,
                Ticket.id_ == UsersTicketsAssociation.ticket_id,
            )
            .where(UsersTicketsAssociation.user_id == user_id)
        )

        return session.execute(statement).one()[0]

    def get_user_report(self, user_id: int, session: Session):
        """
        Returns the number of accepted unclosed tickets for a user,
        the number of closed tickets and the number of forwarded tickets.
        """

        statement = (
            select([func.count(UsersTicketsAssociation.accepted_on)])
            .where(
                and_(
                    UsersTicketsAssociation.user_id == user_id,
                )
            )
            .join(
                Ticket,
                UsersTicketsAssociation.ticket_id == Ticket.id_,
            )
        )
        accepted_tickets = session.execute(statement).one()[0]
        statement = (
            select([func.count(UsersTicketsAssociation.forwarded_on)])
            .where(
                and_(
                    UsersTicketsAssociation.user_id == user_id,
                    UsersTicketsAssociation.forwarded_on is not None,
                )
            )
            .join(
                Ticket,
                UsersTicketsAssociation.ticket_id == Ticket.id_,
            )
        )
        forwarded_tickets = session.execute(statement).one()[0]
        statement = (
            select([func.count(UsersTicketsAssociation.user_id)])
            .where(
                and_(
                    UsersTicketsAssociation.user_id == user_id,
                    Ticket.closed_at is not None,
                )
            )
            .join(
                Ticket,
                UsersTicketsAssociation.ticket_id == Ticket.id_,
            )
        )
        closed_tickets = session.execute(statement).one()[0]
        return {
            "accepted_unclosed_tickets": accepted_tickets,
            "forwarded_tickets": forwarded_tickets,
            "closed_tickets": closed_tickets,
        }

    def get_reports_list(self, session: Session):
        case_statement = case(  # type: ignore
            [
                (
                    UsersTicketsAssociation.forwarded_on is not None,
                    UsersTicketsAssociation.forwarded_on,
                ),
            ],
            else_=Ticket.closed_at,
        )

        average_time_to_handle_ticket = (
            select(
                [
                    func.avg(case_statement - UsersTicketsAssociation.accepted_on).label(
                        "avg_time_to_handle_ticket"
                    ),
                    UsersTicketsAssociation.user_id,
                ]
            )
            .where(
                and_(
                    or_(
                        UsersTicketsAssociation.forwarded_on is not None,
                        Ticket.closed_at is not None,
                    ),
                )
            )
            .join(
                Ticket,
                UsersTicketsAssociation.ticket_id == Ticket.id_,
            )
            .group_by(UsersTicketsAssociation.user_id)
            .subquery()
        )

        average_reaction_time = (
            select(
                [
                    func.avg(
                        UsersTicketsAssociation.accepted_on - Ticket.created_at
                    ).label("avg_reaction_time"),
                    UsersTicketsAssociation.user_id,
                ]
            )
            .join(
                Ticket,
                Ticket.id_ == UsersTicketsAssociation.ticket_id,
            )
            .group_by(UsersTicketsAssociation.user_id)
            .subquery()
        )

        accepted_unclosed_tickets_count = (
            select(
                [
                    func.count(UsersTicketsAssociation.accepted_on).label(
                        "accepted_unclosed_tickets"
                    ),
                    UsersTicketsAssociation.user_id,
                ]
            )
            .join(
                Ticket,
                UsersTicketsAssociation.ticket_id == Ticket.id_,
            )
            .group_by(UsersTicketsAssociation.user_id)
            .subquery()
        )

        forwarded_tickets_count = (
            select(
                [
                    func.count(UsersTicketsAssociation.forwarded_on).label(
                        "forwarded_tickets"
                    ),
                    UsersTicketsAssociation.user_id,
                ]
            )
            .where(
                and_(
                    UsersTicketsAssociation.forwarded_on is not None,
                )
            )
            .join(
                Ticket,
                UsersTicketsAssociation.ticket_id == Ticket.id_,
            )
            .group_by(UsersTicketsAssociation.user_id)
            .subquery()
        )

        closed_tickets_count = (
            select(
                [
                    func.count(UsersTicketsAssociation.user_id).label("closed_tickets"),
                    UsersTicketsAssociation.user_id,
                ]
            )
            .where(
                and_(
                    Ticket.closed_at is not None,
                )
            )
            .join(
                Ticket,
                UsersTicketsAssociation.ticket_id == Ticket.id_,
            )
            .group_by(UsersTicketsAssociation.user_id)
            .subquery()
        )

        statement = (
            select(
                [
                    User.id_,
                    User.first_name,
                    User.last_name,
                    average_time_to_handle_ticket.c.avg_time_to_handle_ticket,
                    average_reaction_time.c.avg_reaction_time,
                    accepted_unclosed_tickets_count.c.accepted_unclosed_tickets,
                    forwarded_tickets_count.c.forwarded_tickets,
                    closed_tickets_count.c.closed_tickets,
                ]
            )
            .join(UsersTicketsAssociation)
            .outerjoin(
                average_time_to_handle_ticket,
                average_time_to_handle_ticket.c.user_id == User.id_,
            )
            .outerjoin(
                average_reaction_time, average_reaction_time.c.user_id == User.id_
            )
            .outerjoin(
                accepted_unclosed_tickets_count,
                accepted_unclosed_tickets_count.c.user_id == User.id_,
            )
            .outerjoin(
                forwarded_tickets_count, forwarded_tickets_count.c.user_id == User.id_
            )
            .outerjoin(closed_tickets_count, closed_tickets_count.c.user_id == User.id_)
        )

        return session.execute(statement).unique().all()

    def get_line_reports_list(self, session: Session):

        opened_tickets_count = (
            select([func.count(Ticket.id_).label("opened_tickets"), Ticket.line_id])
            .group_by(Ticket.line_id)
            .subquery()
        )

        active_tickets_count = (
            select([func.count(Ticket.id_).label("active_tickets"), Ticket.line_id])
            .where(
                Ticket.status.in_(
                    [
                        StatusEnum.PENDING,
                        StatusEnum.APPROVED,
                        StatusEnum.MATERIAL_NOT_AVAILABLE,
                        StatusEnum.WAITING_FOR_APPROVAL,
                        StatusEnum.PRIORITY,
                        StatusEnum.ON_HOLD,
                        StatusEnum.OPEN,
                        StatusEnum.IN_PROGRESS,
                    ]
                )
            )
            .group_by(Ticket.line_id)
            .subquery()
        )

        forwarded_tickets_count = (
            select([func.count(Ticket.id_).label("forwarded_tickets"), Ticket.line_id])
            .where(Ticket.status == StatusEnum.FORWARDED)
            .group_by(Ticket.line_id)
            .subquery()
        )

        closed_tickets_counts = (
            select([func.count(Ticket.id_).label("closed_tickets"), Ticket.line_id])
            .where(Ticket.status.in_([StatusEnum.CLOSED, StatusEnum.REJECTED]))
            .group_by(Ticket.line_id)
            .subquery()
        )

        time_from_last_ticket = (
            select(
                [
                    (func.now() - func.max(Ticket.created_at)).label(
                        "time_from_last_ticket"
                    ),
                    Ticket.line_id,
                ]
            )
            .group_by(Ticket.line_id)
            .subquery()
        )

        case_statement = case(  # type: ignore
            [
                (
                    func.count(Ticket.id_) < 2,
                    2,
                ),
            ],
            else_=func.count(Ticket.id_),
        )

        avg_time_for_problem_to_occur = (
            select(
                [
                    (
                        (func.max(Ticket.created_at) - func.min(Ticket.created_at))
                        / (case_statement - 1)
                    ).label("avg_time_for_problem_to_occur"),
                    Ticket.line_id,
                ]
            )
            .group_by(Ticket.line_id)
            .subquery()
        )

        statement = (
            select(
                [
                    opened_tickets_count.c.opened_tickets,
                    active_tickets_count.c.active_tickets,
                    forwarded_tickets_count.c.forwarded_tickets,
                    closed_tickets_counts.c.closed_tickets,
                    time_from_last_ticket.c.time_from_last_ticket,
                    avg_time_for_problem_to_occur.c.avg_time_for_problem_to_occur,
                    Line.id_,
                    Line.name,
                ]
            )
            .outerjoin(
                opened_tickets_count,
                opened_tickets_count.c.line_id == Line.id_,
            )
            .outerjoin(active_tickets_count, active_tickets_count.c.line_id == Line.id_)
            .outerjoin(
                forwarded_tickets_count,
                forwarded_tickets_count.c.line_id == Line.id_,
            )
            .outerjoin(
                closed_tickets_counts, closed_tickets_counts.c.line_id == Line.id_
            )
            .outerjoin(
                time_from_last_ticket, time_from_last_ticket.c.line_id == Line.id_
            )
            .outerjoin(
                avg_time_for_problem_to_occur,
                avg_time_for_problem_to_occur.c.line_id == Line.id_,
            )
        )
        return session.execute(statement).unique().all()


def get_reports_repository() -> ReportsRepository:
    return ReportsRepository()
