from typing import List

from sqlalchemy import and_, select
from sqlalchemy.orm import Session

from src.models.tickets import Machine, Product, products_lines_association
from src.repository.base import BaseRepository


class ProductRepository(BaseRepository[Product]):
    """
    Product repository
    """

    _table = Product

    def find_by_line_id(
        self,
        line_id: int,
        page_number: int,
        page_size: int,
        order_by: List[str],
        session: Session,
    ) -> List[Machine]:
        """
        Find all machines by line id
        """
        statement = (
            select(self._table, products_lines_association.c.get("product_id"))
            .where(
                and_(
                    products_lines_association.c.get("line_id") == line_id,
                    self._table.deleted.isnot(True),
                )
            )
            .order_by(*[getattr(self._table, o) for o in order_by])
            .offset(page_number * page_size)
            .limit(page_size)
            .join(self._table)
        )
        entries = session.execute(statement).unique().scalars().all()
        return entries


def get_product_repository() -> ProductRepository:
    return ProductRepository()
