from src.models.tickets import Reason
from src.repository.base import BaseRepository


class ReasonRepository(BaseRepository[Reason]):
    """
    Reasons repository
    """

    _table = Reason


def get_reason_repository():
    return ReasonRepository()
