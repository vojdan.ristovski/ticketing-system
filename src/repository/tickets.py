from enum import Enum
from typing import List, Optional

import pandas
from schemas.andon.enums import StatusEnum
from sqlalchemy import and_, func, insert, select, update
from sqlalchemy.orm import Session, aliased

from src.models.enums import OrderDirection
from src.models.tickets import (
    Line,
    Machine,
    Product,
    Reason,
    Ticket,
    tickets_machines_association,
)
from src.models.users import (
    User,
    users_tickets_association,
    users_tickets_forward_association,
)
from src.repository.base import BaseRepository


class TicketRepository(BaseRepository[Ticket]):
    """
    Tickets repository
    """

    _table = Ticket

    def insert_forwarded_to_on_ticket(
        self, from_user_id: int, to_user_id: int, ticket_id: int, session: Session
    ):
        stmt = insert(users_tickets_forward_association).values(
            from_user_id=from_user_id, to_user_id=to_user_id, ticket_id=ticket_id
        )
        session.execute(stmt)
        return None

    def insert_handled_by_on_ticket(
        self, user_id: int, ticket_id: int, session: Session
    ):
        stmt = insert(
            users_tickets_association,
        ).values(user_id=user_id, ticket_id=ticket_id)
        session.execute(stmt)
        return None

    def update_handled_by_on_ticket(
        self, user_id: int, ticket_id: int, update_dict: dict, session: Session
    ):
        stmt = (
            update(
                users_tickets_association,
            )
            .where(
                and_(
                    users_tickets_association.c.user_id == user_id,
                    users_tickets_association.c.ticket_id == ticket_id,
                )
            )
            .values(**update_dict)
        )
        session.execute(stmt)
        return None

    def find_by_machine_id(
        self,
        machine_id: int,
        session: Session,
        page: int = 0,
        page_size: int = 10,
        status: Optional[List[StatusEnum]] = None,
        order_by: Optional[List[Enum]] = None,
        order: Optional[OrderDirection] = None,
    ):
        if status is None:
            status = []
        if order_by is None:
            order_by = ["id_"]
        if order is None:
            order = OrderDirection.ASCENDING
        statement = (
            select(self._table)
            .join(
                tickets_machines_association,
                self._table.id_ == tickets_machines_association.c.ticket_id,
            )
            .where(
                and_(
                    tickets_machines_association.c.machine_id == machine_id,
                    self._table.status.in_(status),
                )
            )
            .order_by(
                *[
                    (getattr(self._table, item).desc())
                    if order == OrderDirection.DESCENDING
                    else (getattr(self._table, item))
                    for item in order_by
                ]
            )
            .offset(page * page_size)
            .limit(page_size)
        )
        entries = session.execute(statement).unique().scalars().all()
        return entries

    def find_by_user_id(
        self,
        user_id: int,
        session: Session,
        page: int = 0,
        page_size: int = 10,
    ):
        statement = (
            select(self._table)
            .where(users_tickets_association.c.user_id == user_id)
            .offset(page * page_size)
            .limit(page_size)
            .join(
                users_tickets_association,
                self._table.id_ == users_tickets_association.c.ticket_id,
            )
        )
        entries = session.execute(statement).unique().scalars().all()
        return entries

    def count_by_machine_id(self, machine_id: int, session: Session):
        statement = (
            select(func.count(self._table.id_))
            .where(tickets_machines_association.c.machine_id == machine_id)
            .join(
                tickets_machines_association,
                self._table.id_ == tickets_machines_association.c.ticket_id,
            )
        )
        count = session.execute(statement).one()[0]
        return count

    def count_by_user_id(self, user_id: int, session: Session):
        statement = (
            select(func.count(self._table.id_))
            .where(users_tickets_association.c.user_id == user_id)
            .join(
                users_tickets_association,
                self._table.id_ == users_tickets_association.c.ticket_id,
            )
        )
        count = session.execute(statement).one()[0]
        return count

    def get_users_tickets_dataframe(self, engine):
        return pandas.read_sql_table("users_tickets_association", con=engine)

    def get_tickets_machines_association_dataframe(self, engine):
        return pandas.read_sql_table("tickets_machines_association", con=engine)

    def get_pd_dataframe(self, engine):
        approved_alias = aliased(User)
        closed_alias = aliased(User)
        reason_alias = aliased(Reason)

        statement = (
            select(
                self._table.id_,
                self._table.created_at,
                self._table.updated_at,
                Line.name.label("Line name"),
                Machine.name.label("Machines"),
                self._table.status,
                self._table.ticket_type,
                self._table.datetime_from,
                self._table.datetime_to,
                func.concat(
                    User.card_id, " ", User.first_name, " ", User.last_name
                ).label("Created by user"),
                func.concat(
                    approved_alias.card_id,
                    " ",
                    approved_alias.first_name,
                    " ",
                    approved_alias.last_name,
                ).label("Approved by user"),
                func.concat(
                    closed_alias.card_id,
                    " ",
                    closed_alias.first_name,
                    " ",
                    closed_alias.last_name,
                ).label("Closed by user"),
                self._table.closed_at,
                Product.name.label("Product name"),
                self._table.corrective_action,
                self._table.issue_description,
                self._table.component_id,
                reason_alias.reason,
            )
            .join(
                target=User,
                onclause=User.id_ == self._table.created_by_id,
            )
            .join(
                target=approved_alias,
                onclause=approved_alias.id_ == self._table.approved_by_id,
                isouter=True,
            )
            .join(
                target=closed_alias,
                onclause=closed_alias.id_ == self._table.closed_by_id,
                isouter=True,
            )
            .join(
                target=reason_alias,
                onclause=reason_alias.id_ == self._table.reason_id,
                isouter=True,
            )
            .where(
                and_(
                    self._table.deleted is False,
                    Machine.id_ == tickets_machines_association.c.machine_id,
                    self._table.id_ == tickets_machines_association.c.ticket_id,
                    self._table.line_id == Line.id_,
                    self._table.product_id == Product.id_,
                    self._table.created_by_id == User.id_,
                )
            )
            .distinct()
        )

        df = pandas.read_sql(statement, con=engine)
        return df

    def get_unclosed_tickets(self, session: Session) -> List[Ticket]:
        statement = select(self._table).where(self._table.status != StatusEnum.CLOSED)
        entries = session.execute(statement).scalars().unique().all()
        return entries


def get_ticket_repository() -> TicketRepository:
    """
    Get Ticket repository
    """
    return TicketRepository()
