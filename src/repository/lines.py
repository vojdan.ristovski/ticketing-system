from sqlalchemy import delete, insert, update
from sqlalchemy.orm import Session

from src.models.tickets import (
    Line,
    Machine,
    products_lines_association,
    users_lines_association,
)
from src.repository.base import BaseRepository


class LineRepository(BaseRepository[Line]):
    """
    Lines repository
    """

    _table = Line
    _machines_table = Machine

    def insert_machine_on_line(
        self, line_id: int, machine_id: int, session: Session
    ) -> None:
        statement = (
            update(self._machines_table)
            .where(self._machines_table.id_ == machine_id)
            .values(line_id=line_id)
        )
        session.execute(statement)

    def insert_product_on_line(
        self, line_id: int, product_id: int, session: Session
    ) -> None:
        statement = insert(products_lines_association).values(
            product_id=product_id, line_id=line_id
        )
        session.execute(statement)

    def insert_user_on_line(self, line_id: int, user_id: int, session: Session) -> None:
        statement = insert(users_lines_association).values(
            user_id=user_id, line_id=line_id
        )
        session.execute(statement)

    def remove_users_from_line(self, line_id: int, session: Session):
        statement = delete(users_lines_association).where(
            users_lines_association.c.line_id == line_id
        )
        session.execute(statement)


def get_line_repository() -> LineRepository:
    """
    Get Line repository
    """
    return LineRepository()
