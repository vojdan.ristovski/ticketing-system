from sqlalchemy import select
from sqlalchemy.orm import Session

from src.models.tickets import TimeSeries
from src.repository.base import BaseRepository


class TimeSeriesRepository(BaseRepository[TimeSeries]):
    """
    Components repository
    """

    _table = TimeSeries

    def get_time_series_by_line_id(self, line_id: int, session: Session):
        statement = select(self._table).where(self._table.line_id == line_id)
        entries = session.execute(statement).unique().scalars().all()
        return entries


def get_time_series_repository():
    return TimeSeriesRepository()
