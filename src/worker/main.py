from celery import Celery

from src.config import get_config

config = get_config()


def get_worker():
    celery_worker = Celery(
        "tasks",
        broker=config.celery_broker_url,
        backend=config.celery_backend_url,
        task_serializer="pickle",
        accept_content=["application/json", "application/x-python-serialize"],
        include=["src.worker.tasks", "src.worker.emails", "src.worker.etl"],
    )

    return celery_worker


app = get_worker()
app.conf.task_routes = {
    "src.worker.etl.*": {"queue": "etl"},
}
