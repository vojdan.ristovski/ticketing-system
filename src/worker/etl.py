from datetime import datetime
from hashlib import sha256

import numpy as np
import pandas as pd
from celery.schedules import crontab
from pymongo import MongoClient
from pymongo.collection import Collection
from sqlalchemy import create_engine

from src.config import get_config
from src.worker.main import app

config = get_config()

mongo_url: str = config.mongo_url
mongo_db_name: str = config.etl_mongo_db_name

destination_db_connection_sting: str = config.etl_destination_db_url


@app.on_after_finalize.connect
def setup_periodic_tasks(sender, **kwargs):
    print("Setting up periodic ETL tasks...")
    sender.add_periodic_task(
        crontab(minute=config.etl_cron),
        add_users_to_staging_db.s(),
    )
    sender.add_periodic_task(
        crontab(minute=config.etl_cron),
        add_lines_to_staging_db.s(),
    )
    sender.add_periodic_task(
        crontab(minute=config.etl_cron),
        add_machines_to_staging_db.s(),
    )
    sender.add_periodic_task(
        crontab(minute=config.etl_cron),
        add_statuses_to_staging_db.s(),
    )
    sender.add_periodic_task(
        crontab(minute=config.etl_cron),
        add_tickets_to_staging_db.s(),
    )
    sender.add_periodic_task(
        crontab(minute=config.etl_cron),
        add_ticket_types_to_staging_db.s(),
    )
    sender.add_periodic_task(
        crontab(minute=config.etl_cron),
        add_priorities_to_staging_db.s(),
    )
    print("Setting up periodic ETL tasks done!")


@app.task(queue="etl")
def add_users_to_staging_db():
    client = MongoClient(mongo_url)
    destination_db_engine = create_engine(
        destination_db_connection_sting, pool_pre_ping=True
    )
    db = client[mongo_db_name]
    users_collection: Collection = db["users"]
    users_cursor = users_collection.find()
    users_df = pd.DataFrame(
        [
            {
                **user_document["document"],
                "timestamp": user_document["timestamp"],
            }
            for user_document in users_cursor
        ]
    )
    users_df["valid_from"] = users_df["timestamp"]
    users_df["source_id"] = users_df["id_"]
    users_df["team"] = users_df["team"].map(
        {
            0: "A",
            1: "B",
            2: "C",
            3: "N",
        }
    )
    users_df["hash"] = users_df.apply(
        lambda row: sha256(
            (row["first_name"] + row["last_name"] + str(row["team"])).encode()
        ).hexdigest(),
        axis=1,
    )

    users_df.sort_values(by=["timestamp"], inplace=True)
    users_df["valid_to"] = users_df.groupby(["source_id"])["timestamp"].shift(-1)
    users_df["valid_to"] = users_df["valid_to"].fillna(datetime.max)

    users_df = users_df[
        [
            "source_id",
            "valid_from",
            "valid_to",
            "first_name",
            "last_name",
            "team",
            "hash",
        ]
    ]
    users_df.index.set_names("unique_id", inplace=True)

    users_df.to_sql(
        name="users", con=destination_db_engine, if_exists="replace", index=True
    )

    print("add_users_to_staging_db done")

    return "add_users_to_staging_db"


@app.task(queue="etl")
def add_lines_to_staging_db():
    client = MongoClient(mongo_url)
    destination_db_engine = create_engine(
        destination_db_connection_sting, pool_pre_ping=True
    )
    db = client[mongo_db_name]
    lines_collection: Collection = db["lines"]
    lines_cursor = lines_collection.find()
    lines_df = pd.DataFrame(
        [
            {
                **line_document["document"],
                "timestamp": line_document["timestamp"],
            }
            for line_document in lines_cursor
        ]
    )

    lines_df["type"] = lines_df["name"].apply(
        lambda x: "SMT" if "smt" in x.lower() or "offline" in x.lower() else "CELL"
    )

    lines_df["valid_from"] = lines_df["timestamp"]
    lines_df["source_id"] = lines_df["id_"]
    lines_df["hash"] = lines_df.apply(
        lambda row: sha256((row["name"] + str(row["zone_id"])).encode()).hexdigest(),
        axis=1,
    )

    lines_df.sort_values(by=["timestamp"], inplace=True)
    lines_df["valid_to"] = lines_df.groupby(["source_id"])["timestamp"].shift(-1)
    lines_df["valid_to"] = lines_df["valid_to"].fillna(datetime.max)

    lines_df = lines_df[
        ["source_id", "valid_from", "valid_to", "name", "zone_id", "hash", "type"]
    ]
    lines_df.index.set_names("unique_id", inplace=True)

    lines_df.to_sql(
        name="lines", con=destination_db_engine, if_exists="replace", index=True
    )


@app.task(queue="etl")
def add_machines_to_staging_db():
    client = MongoClient(mongo_url)
    destination_db_engine = create_engine(
        destination_db_connection_sting, pool_pre_ping=True
    )
    db = client[mongo_db_name]
    machines_collection: Collection = db["machines"]
    machines_cursor = machines_collection.find()
    machines_df = pd.DataFrame(
        [machine_document["document"] for machine_document in machines_cursor]
    )

    machines_df = machines_df.drop_duplicates(subset=["id_"])

    machines_df["machine_type_id"] = machines_df["machine_type"].apply(
        lambda row: row["id_"]
    )
    machines_df["machine_type_name"] = machines_df["machine_type"].apply(
        lambda row: row["name"]
    )
    machines_df["line_id"] = machines_df["line"].apply(lambda row: row["id_"])
    machines_df["line_name"] = machines_df["line"].apply(lambda row: row["name"])

    machines_df.set_index("id_", inplace=True)
    machines_df.index.set_names("id", inplace=True)

    machines_df = machines_df[
        [
            "created_at",
            "updated_at",
            "deleted_at",
            "deleted",
            "name",
            "order",
            "description",
            "sku",
            "machine_type_id",
            "machine_type_name",
            "line_id",
            "line_name",
        ]
    ]

    machines_df.to_sql(
        name="machines", con=destination_db_engine, if_exists="replace", index=True
    )


@app.task(queue="etl")
def add_statuses_to_staging_db():
    destination_db_engine = create_engine(
        destination_db_connection_sting, pool_pre_ping=True
    )
    statuses_df = pd.DataFrame(
        [
            {"id": 0, "name": "Open"},
            {"id": 1, "name": "Closed"},
            {"id": 2, "name": "Pending"},
            {"id": 3, "name": "Forwarded"},
            {"id": 4, "name": "On Hold"},
            {"id": 5, "name": "Waiting for Approval"},
            {"id": 6, "name": "Approved"},
            {"id": 7, "name": "In Progress"},
            {"id": 8, "name": "Rejected"},
            {"id": 9, "name": "Priority"},
            {"id": 10, "name": "Material Not Available"},
        ]
    )

    statuses_df.index = statuses_df["id"]
    statuses_df.drop(columns=["id"], inplace=True)
    statuses_df.to_sql(
        name="statuses",
        con=destination_db_engine,
        if_exists="replace",
        index=True,
    )


@app.task(queue="etl")
def add_tickets_to_staging_db():
    client = MongoClient(mongo_url)
    destination_db_engine = create_engine(
        destination_db_connection_sting, pool_pre_ping=True
    )
    db = client[mongo_db_name]
    tickets_collection: Collection = db["tickets"]
    tickets_cursor = tickets_collection.find()
    tickets_df = pd.DataFrame(
        [
            {
                **ticket_document["document"],
                "active_from": ticket_document["timestamp"],
                "user": ticket_document["user"] if "user" in ticket_document else None,
            }
            for ticket_document in tickets_cursor
        ]
    )

    tickets_df["hash"] = tickets_df.apply(
        lambda row: sha256(
            (str(row["status"]) + str(row["id_"]) + str(row["active_from"])).encode()
        ).hexdigest(),
        axis=1,
    )

    tickets_df["machine_id"] = tickets_df["machines"].apply(
        lambda row: row[0]["id_"] if len(row) > 0 else None
    )
    tickets_df["machine_id"] = tickets_df["machine_id"].astype("Int64")

    tickets_df.set_index(["id_", "status"], append=True, inplace=True)

    def create_dim_date(date: datetime):
        return {
            "day_of_week": date.weekday(),
            "day_of_month": date.day,
            "month_of_year": date.month,
            "year": date.year,
            "is_weekend": date.weekday() in [5, 6],
            "is_national_holiday": False,  # TODO: Get holidays from API
        }

    def create_dim_time(time: datetime):
        return {
            "hour": time.hour,
            "minute": time.minute,
            "second": time.second,
            "shift": 0
            if time.hour < 8
            else 1
            if time.hour < 16
            else 2,  # TODO: redefine this
        }

    dates_df = pd.DataFrame(
        columns=[
            "day_of_week",
            "day_of_month",
            "month_of_year",
            "year",
            "is_weekend",
            "is_national_holiday",
        ]
    )

    times_df = pd.DataFrame(
        columns=[
            "hour",
            "shift",
        ]
    )

    def set_or_create_dim_date(date: datetime):
        dateint = date.year * 10000 + date.month * 100 + date.day
        if np.isnan(dateint):
            return None
        if dateint not in dates_df.index:
            dates_df.loc[dateint] = create_dim_date(date)
        return dateint

    def set_or_create_dim_time(date: datetime):
        dateint = date.hour
        if np.isnan(dateint):
            return None
        if dateint not in times_df.index:
            times_df.loc[dateint] = create_dim_time(date)
        return dateint

    tickets_df["date_from_id"] = tickets_df["active_from"].apply(set_or_create_dim_date)

    tickets_df["time_from_id"] = tickets_df["active_from"].apply(set_or_create_dim_time)

    tickets_df["line_id"] = tickets_df["line"].apply(lambda row: row["id_"])

    tickets_df["priority_id"] = tickets_df["priority"]

    tickets_df.sort_values(by=["active_from"], inplace=True)

    tickets_df["active_to"] = tickets_df.groupby(["id_"])["active_from"].shift(-1)

    tickets_df["total_duration"] = (
        tickets_df["active_to"] - tickets_df["created_at"]
    ).dt.total_seconds()

    tickets_df["duration"] = tickets_df["active_to"] - tickets_df["active_from"]
    tickets_df["duration"] = tickets_df["duration"].dt.total_seconds() * 1000
    tickets_df["duration"] = tickets_df["duration"].fillna(-1000)
    tickets_df["duration"] = tickets_df["duration"].astype(int)
    tickets_df["duration"] = tickets_df["duration"] / 1000
    tickets_df["duration"] = tickets_df["duration"].astype(int)
    tickets_df["duration"] = tickets_df["duration"].replace(-1, pd.NA)

    tickets_df["active_to"] = tickets_df["active_to"].fillna(datetime.max)

    tickets_df["date_to_id"] = tickets_df["active_to"].apply(set_or_create_dim_date)

    tickets_df["time_to_id"] = tickets_df["active_to"].apply(set_or_create_dim_time)

    tickets_df["user_id"] = tickets_df.apply(
        lambda row: row["user"]["id_"] if row["user"] else None, axis=1
    )
    tickets_df["user_id"] = tickets_df["user_id"].astype("Int64")

    tickets_df["is_active"] = tickets_df["active_to"] == datetime.max

    tickets_df.index.set_names(["unique_id", "ticket_id", "status_id"], inplace=True)
    tickets_df = tickets_df[
        [
            "corrective_action",
            "issue_description",
            "ticket_type",
            "date_from_id",
            "date_to_id",
            "time_from_id",
            "time_to_id",
            "is_active",
            "line_id",
            "machine_id",
            "priority_id",
            "created_at",
            "user_id",
            "active_from",
            "active_to",
            "duration",
            "total_duration",
        ]
    ]
    tickets_df.loc[tickets_df["issue_description"] == "", "issue_description"] = None
    tickets_df.loc[tickets_df["corrective_action"] == "", "corrective_action"] = None
    tickets_df.to_sql(
        name="tickets",
        con=destination_db_engine,
        if_exists="replace",
        index=True,
        index_label=["unique_id", "ticket_id", "status_id"],
    )


@app.task(queue="etl")
def add_ticket_types_to_staging_db():
    destination_db_engine = create_engine(
        destination_db_connection_sting, pool_pre_ping=True
    )
    ticket_types_df = pd.DataFrame(
        [
            {"id": 0, "name": "Machine Problem"},
            {"id": 1, "name": "Quality Problem"},
            {"id": 2, "name": "Maintenance Problem"},
            {"id": 3, "name": "Materials Problem"},
            {"id": 4, "name": "Planned Action"},
        ],
        columns=["id", "name"],
    )
    ticket_types_df.set_index("id", inplace=True)

    ticket_types_df.to_sql(
        name="ticket_types",
        con=destination_db_engine,
        if_exists="replace",
        index=True,
    )


@app.task(queue="etl")
def add_priorities_to_staging_db():
    destination_db_engine = create_engine(
        destination_db_connection_sting, pool_pre_ping=True
    )
    priority_df = pd.DataFrame(
        [
            {"id": 0, "name": "Total Halt"},
            {"id": 1, "name": "Efficiency Reduction"},
            {"id": 2, "name": "Potential Halt"},
            {"id": 3, "name": "Non-Critical"},
            {"id": 4, "name": "Inactive"},
            {"id": 5, "name": "Planned Action"},
            {"id": 6, "name": "Preventive Maintenance"},
        ],
        columns=["id", "name"],
    )

    priority_df.set_index("id", inplace=True)

    priority_df.to_sql(
        name="priorities",
        con=destination_db_engine,
        if_exists="replace",
        index=True,
    )
