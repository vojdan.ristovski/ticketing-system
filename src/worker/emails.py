import logging
import smtplib
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from fastapi import File

from src.config import get_config
from src.worker.main import app

config = get_config()
logger = logging.getLogger(__name__)


def _server_send_email(to: str, message):
    with smtplib.SMTP(config.smtp_host, config.smtp_port) as server:
        if config.starttls:
            server.starttls()
        if config.smtp_user and config.smtp_password:
            server.login(config.smtp_user, config.smtp_password)
        server.sendmail(config.email_from, to, message.as_string())
        logger.info(f"Sent email to {to}")


@app.task()
def send_email(to: str, subject: str, raw_body: str):
    message = MIMEMultipart()
    message["From"] = config.email_from
    message["To"] = to
    message["Subject"] = subject
    message.attach(MIMEText(raw_body, "plain"))
    logger.info(f"Sending email to {to}")
    _server_send_email(to, message)
    return True


@app.task()
def send_email_with_attachment(
    to: str, subject: str, raw_body: str, attachment: File, filename: str  # type: ignore
):
    message = MIMEMultipart()
    message["From"] = config.email_from
    message["To"] = to
    message["Subject"] = subject
    message.attach(MIMEText(raw_body, "plain"))
    mime_attachment = MIMEApplication(attachment)
    mime_attachment.add_header("Content-Disposition", "attachment", filename=filename)
    message.attach(mime_attachment)

    _server_send_email(to, message)
