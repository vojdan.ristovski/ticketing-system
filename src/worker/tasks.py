"""
In theory the following flow should be achieved.

Materials:
    - status changed to not available: call the user who
    created the ticket & send email to predefined group
    - status changed to closed: send email to predefined group

Machine problem:
    - status changed to forwarded: call level 2 support team
        - if forwarding is in range [8:00, 16:00] call, otherwise send email
    - if status changed to priority: call again & send email
    - status changed to closed: send email to predefined group

Quality problem:
    - status changed to forwarded: call level 2 support team
        - if forwarding is in range [8:00, 16:00] call, otherwise send email
    - if status changed to priority: call again & send email
    - status changed to closed: send email to predefined group

Preventive maintenance:
    - /
    - status changed to closed: send email to predefined group

Planned action:
    - if length < 30 mins send email to predefined group
    - else send email to predefined group
    - status changed to approved/rejected send email to creator
     (if approved send the time)
    - status changed to closed: send email to predefined group
"""

import logging
import random
from collections import defaultdict
from datetime import datetime
from time import sleep
from typing import Any, List, Tuple, TypeVar

import pytz
import requests  # type: ignore
from celery.schedules import crontab
from schemas.andon.enums import (
    PriorityEnum,
    SeriesStatusEnum,
    StatusEnum,
    TicketTypeEnum,
)
from schemas.andon.events_config import Escalation
from schemas.andon.tickets import ReadTicketSchema
from schemas.andon.users import ReadUserSchema

from src.config import get_config
from src.const import (
    TRIGGER_APPROVED_TICKET_TASKNAME,
    TRIGGER_CLOSED_TICKET_TASKNAME,
    TRIGGER_EXPORT_TICKETS,
    TRIGGER_FORWARDED_TICKET_TASKNAME,
    TRIGGER_MATERIAL_NOT_AVAILABLE_TASKNAME,
    TRIGGER_NEW_TICKET_TASKNAME,
    TRIGGER_PERIODIC,
    TRIGGER_PRIORITIZED_TICKET_TASKNAME,
)
from src.db.session import ScopedSession, engine
from src.models.mixins import BaseDbModel
from src.models.tickets import Ticket, TimeSeries
from src.models.users import User
from src.repository.lines import LineRepository, get_line_repository
from src.repository.tickets import TicketRepository, get_ticket_repository
from src.repository.time_series import (  # type: ignore
    TimeSeriesRepository,
    get_time_series_repository,
)
from src.repository.users import (
    UserRepository,
    UserRoleRepository,
    get_user_repository,
    get_user_role_repository,
)
from src.utils import get_role_id_from_planned_action_length, get_users_from_ticket
from src.worker.emails import send_email, send_email_with_attachment
from src.worker.main import app

logger = logging.getLogger(__name__)

MODEL = TypeVar("MODEL", bound=BaseDbModel)

config = get_config()
ticket_repository: TicketRepository = get_ticket_repository()
user_repository: UserRepository = get_user_repository()
line_repository: LineRepository = get_line_repository()
role_repository: UserRoleRepository = get_user_role_repository()
time_series_repository: TimeSeriesRepository = get_time_series_repository()

strings = {
    TicketTypeEnum.MACHINE_PROBLEM: "Проблем со машина",
    TicketTypeEnum.MAINTENANCE_PROBLEM: "Превентивно одржување",
    TicketTypeEnum.MATERIALS_PROBLEM: "Проблем со материјали",
    TicketTypeEnum.PLANNED_ACTION: "Планирана акција",
    TicketTypeEnum.QUALITY_PROBLEM: "Проблем со квалитет",
}

ari_endpoint = config.ari_endpoint
ari_api_key = config.ari_api_key

priority_sounds = {
    PriorityEnum.POTENTIAL_HALT: "sound:POTENTIAL_HALT",
    PriorityEnum.TOTAL_HALT: "sound:TOTAL_HALT",
    PriorityEnum.EFFICIENCY_REDUCTION: "sound:EFFICIENCY_REDUCTION",
    PriorityEnum.NON_CRITICAL: "sound:NON_CRITICAL",
    PriorityEnum.INACTIVE: "sound:INACTIVE",
    PriorityEnum.PLANNED_ACTION: "sound:PLANNED_ACTION",
    PriorityEnum.PREVENTIVE_MAINTENANCE: "sound:PREVENTIVE_MAINTENANCE",
}

problem_sounds = {
    TicketTypeEnum.MACHINE_PROBLEM: "sound:MACHINE_PROBLEM",
    TicketTypeEnum.MAINTENANCE_PROBLEM: "sound:MAINTENANCE_PROBLEM",
    TicketTypeEnum.MATERIALS_PROBLEM: "sound:MATERIALS_PROBLEM",
    TicketTypeEnum.PLANNED_ACTION: "sound:PLANNED_ACTION",
    TicketTypeEnum.QUALITY_PROBLEM: "sound:QUALITY_PROBLEM",
}


@app.task(name=TRIGGER_PERIODIC)
def trigger_periodic():
    logger.info("Running periodic task")
    with ScopedSession() as active_session:
        users_dict = defaultdict(list)
        unclosed_tickets = ticket_repository.get_unclosed_tickets(session=active_session)
        for ticket in unclosed_tickets:
            ticket = ReadTicketSchema.from_orm(ticket)
            if not ticket.line.event_config or not ticket.line.id_:
                continue
            event_config = ticket.line.event_config.get_event_config(
                ticket_type=ticket.ticket_type
            )
            daily_role_id = event_config.daily.role_id
            users = user_repository.get_users_by_role_and_line(
                role_id=daily_role_id, line_id=ticket.line.id_, session=active_session
            )
            for user in users:
                user = ReadUserSchema.from_orm(user)
                users_dict[user].append(ticket)

        for user, tickets in users_dict.items():
            send_daily_email(user, tickets)


@app.on_after_finalize.connect
def setup_periodic_tasks(sender, **kwargs):
    logger.info("Setting up periodic tasks...")
    sender.add_periodic_task(
        crontab(hour=config.daily_task_hour, minute=config.daily_task_minute),
        trigger_periodic.s(),
    )
    logger.info("Setup done.")


@app.task()
def send_daily_email(user: User, tickets: List[Ticket]):
    subject = "Дневен извештај за тикети"

    body = "Отворени тикети: \n\n"

    for ticket in tickets:
        ticket_schema = ReadTicketSchema.from_orm(ticket)
        body = (
            f"Приоритет: {PriorityEnum(ticket_schema.priority).name} \n"
            f"Тип на проблем: {strings.get(ticket_schema.ticket_type)} \n"
        )
        if ticket_schema.ticket_type == TicketTypeEnum.MACHINE_PROBLEM:
            if len(ticket_schema.machines) > 0:
                body += "Машина: "
                for machine in ticket_schema.machines:
                    body += f"{machine.name} "

        body += "\n" f"Статус: {StatusEnum(ticket_schema.status).name} \n\n\n"

    if user.email:
        send_email.delay(
            to=user.email,
            subject=subject,
            raw_body=body,
        )


@app.task
def send_email_to_escalation_group(
    initial_ticket: ReadTicketSchema,
    escalation: Escalation,
    ignored_statuses=[StatusEnum.CLOSED],
    type=None,
):
    with ScopedSession() as active_session:
        ticket_obj = ticket_repository.find_one_by_id(
            id_=initial_ticket.id_, session=active_session
        )
        if not ticket_obj:
            return

        ticket = ReadTicketSchema.from_orm(ticket_obj)

        if ticket.status in ignored_statuses:
            return
        escalation_group_name = ""
        if type:
            escalation_group_name = f"[{type}] "
        if ticket.line and not ticket.line.id_:
            return
        ticket_type = str(TicketTypeEnum(ticket.ticket_type).name)
        escalation_group_name += f"{ticket_type} {escalation.get_delay_as_str()}"
        subject = f"{escalation_group_name}"

        body = (
            f"Приоритет: {PriorityEnum(ticket.priority).name} \n"
            f"Тип на проблем: {strings.get(ticket.ticket_type)} \n"
        )

        if ticket.ticket_type == TicketTypeEnum.MACHINE_PROBLEM and ticket.machines:
            body += "Машини: " + "".join(machine.name for machine in ticket.machines)

        body += "\n" f"Статус: {StatusEnum(ticket.status).name} \n"

        users = user_repository.get_users_by_role_and_line(
            escalation.role_id, ticket.line.id_, active_session
        )
        _send_email_and_call_users(users, subject, body, ticket)


def send_planned_action_email(ticket: ReadTicketSchema):
    if not ticket.datetime_from or not ticket.datetime_to:
        return
    with ScopedSession() as active_session:
        subject = "Ново барање за планирана акција"

        mapping = ticket.line.event_config
        total_planned_time = ticket.datetime_to - ticket.datetime_from
        role_id = get_role_id_from_planned_action_length(
            mapping, total_planned_time.seconds  # type: ignore
        )

        body = (
            f"Приоритет: {PriorityEnum(ticket.priority).name} \n"
            f"Тип на проблем: {strings.get(ticket.ticket_type)} \n"
            f"Статус: {StatusEnum(ticket.status).name} \n"
            f"Барањето е во времетрање од {total_planned_time} \n"
        )

        users = user_repository.get_users_by_role_and_line(
            role_id, ticket.line.id_, active_session
        )

        _send_email_and_call_users(users, subject, body, ticket)


@app.task()
def call_user(phone_number: str, ticket: ReadTicketSchema):
    caller_id = f"P{ticket.priority.value + 1}_{ticket.line.name}"
    channel_id = random.randint(1, 100_000)  # nosec
    query = {
        "endpoint": "Local/{endpoint}@from-internal".format(
            endpoint=str(phone_number).split(".")[0]
        ),
        "app": "hello-world",
        "callerId": caller_id,
        "timeout": 30,
        "api_key": ari_api_key,
        "channelId": channel_id,
    }

    print(f"Calling {phone_number}")

    try:
        response = requests.post(ari_endpoint + "channels", params=query)
        print(response.json())

        if response.status_code < 300:
            query = {
                "media": ",".join(
                    [
                        priority_sounds[ticket.priority],
                        problem_sounds[ticket.ticket_type],
                    ]
                ),
                "api_key": ari_api_key,
                "lang": "mk",
            }

            start = 0
            while (
                requests.post(
                    ari_endpoint + f"channels/{channel_id}/play",
                    params=query,
                ).status_code
                >= 300
            ) and start < 60:
                sleep(1)
                start += 1

            requests.post(
                ari_endpoint + f"channels/{channel_id}/play",
                params=query,
            )
            requests.post(
                ari_endpoint + f"channels/{channel_id}/play",
                params=query,
            )

            sleep(30)
            requests.delete(
                ari_endpoint + f"channels/{channel_id}",
                params={"api_key": ari_api_key},
            )
            print(f"Call to {phone_number} finished")

    except Exception as e:
        logger.info(e)


@app.task()
def change_time_series_status(ticket: ReadTicketSchema):
    with ScopedSession() as active_session:
        query: List[Tuple[str, Any]] = [
            ("line_id", ticket.line.id_),
            (
                "status",
                [
                    StatusEnum.OPEN,
                    StatusEnum.FORWARDED,
                    StatusEnum.PENDING,
                    StatusEnum.ON_HOLD,
                    StatusEnum.MATERIAL_NOT_AVAILABLE,
                    StatusEnum.WAITING_FOR_APPROVAL,
                    StatusEnum.PRIORITY,
                    StatusEnum.APPROVED,
                ],
            ),
        ]
        tickets: List[Ticket] = ticket_repository.find_many_by_attrs(
            0, 10000, *query, session=active_session
        )
        if not tickets:
            time_series_object = TimeSeries(
                line_id=ticket.line.id_,
                status=SeriesStatusEnum.RUNNING,
            )
            time_series_repository.insert_one(time_series_object, active_session)


@app.task(name=TRIGGER_APPROVED_TICKET_TASKNAME)
def trigger_approved_ticket(ticket: ReadTicketSchema):
    with ScopedSession() as active_session:
        if (
            not ticket.line.event_config
            or not ticket.line.id_
            or not ticket.datetime_from
            or not ticket.datetime_to
        ):
            return
        if ticket.ticket_type == TicketTypeEnum.PLANNED_ACTION:
            send_planned_action_email(ticket)
            return

        users = get_users_from_ticket(ticket, active_session)

        subject = (
            f"[ANDON SYSTEM Ticket #{ticket.id_}]"
            f" {strings.get(ticket.ticket_type)} на линија {ticket.line.name}"
        )

        body = (
            f"Одобрено {strings.get(ticket.ticket_type, '').lower()}\n"
            f"Од {ticket.datetime_from.astimezone(pytz.timezone('Europe/Skopje'))}\n"
            f"До {ticket.datetime_to.astimezone(pytz.timezone('Europe/Skopje'))}"
        )
        _send_email_to_users(users, subject, body)


@app.task(name=TRIGGER_MATERIAL_NOT_AVAILABLE_TASKNAME)
def trigger_material_not_available(ticket: ReadTicketSchema):
    with ScopedSession() as active_session:
        if not ticket.line.event_config or not ticket.line.id_:
            return
        mapping = ticket.line.event_config.MATERIALS_PROBLEM
        subject = (
            f"[ANDON SYSTEM Ticket #{ticket.id_}]"
            f" {strings.get(ticket.ticket_type)} на линија {ticket.line.name}"
        )

        body = "Материјалот не е достапен"

        users = user_repository.get_users_by_role_and_line(
            mapping.material_unavailable_escalation.role_id,
            ticket.line.id_,
            active_session,
        )
        _send_email_to_users(users, subject, body)


@app.task(name=TRIGGER_CLOSED_TICKET_TASKNAME)
def trigger_closed_ticket(ticket: ReadTicketSchema):
    with ScopedSession() as active_session:
        if not ticket.line.event_config or not ticket.line.id_:
            return
        if ticket.ticket_type == TicketTypeEnum.PLANNED_ACTION:
            send_planned_action_email(ticket)
            return

        users = get_users_from_ticket(ticket, active_session)

        subject = (
            f"[ANDON SYSTEM Ticket #{ticket.id_}]"
            f" {strings.get(ticket.ticket_type)} на линија {ticket.line.name}"
        )

        description = ticket.reason or "" + " ".join(
            [machine.name for machine in ticket.machines]
        )

        active_since = (
            ticket.created_at.astimezone(pytz.timezone("Europe/Skopje"))
            if ticket.created_at
            else "ERR"
        )
        closed_at = (
            (ticket.closed_at or datetime.now()).astimezone(
                pytz.timezone("Europe/Skopje")
            )
            if ticket.created_at
            else "ERR"
        )
        total_delay = (ticket.closed_at or datetime.now()) - (
            ticket.created_at or datetime.now()
        )

        body = (
            f"На линија {ticket.line.name} е разрешен"
            f" {strings.get(ticket.ticket_type, '').lower()}:"
            f" {description}\n\n\n"
            f"Проблемот е активен од {active_since}"
            f" ({pytz.timezone('Europe/Skopje')}) до {closed_at}\n"
            f"Вкупно време потребно за разрешување {total_delay}"
        )
        _send_email_to_users(users, subject, body)
        change_time_series_status.delay(ticket=ticket)


@app.task(name=TRIGGER_FORWARDED_TICKET_TASKNAME)
def trigger_forwarded_ticket(ticket: ReadTicketSchema, from_user: User, to_user: User):
    ticket_type = str(TicketTypeEnum(ticket.ticket_type).name)
    subject = f"[{ticket_type}] Препраќање"

    body = (
        f"Препратено од {from_user.card_id} до {to_user.card_id}"
        f"Приоритет: {PriorityEnum(ticket.priority).name} \n"
        f"Тип на проблем: {strings.get(ticket.ticket_type)} \n"
    )

    if ticket.ticket_type == TicketTypeEnum.MACHINE_PROBLEM:
        body += "Машини: " + "".join(machine.name for machine in ticket.machines)

    body += "\n" f"Статус: {StatusEnum(ticket.status).name} \n"

    if to_user.email:
        send_email.delay(
            to=to_user.email,
            subject=subject,
            raw_body=body,
        )

    if to_user.phone_number:
        call_user.delay(
            phone_number=to_user.phone_number,
            ticket=ticket,
        )


@app.task(name=TRIGGER_PRIORITIZED_TICKET_TASKNAME)
def trigger_prioritized_ticket(ticket: ReadTicketSchema):
    with ScopedSession() as active_session:
        if not ticket.line.event_config or not ticket.line.id_:
            return
        if ticket.ticket_type == TicketTypeEnum.PLANNED_ACTION:
            send_planned_action_email(ticket)
            return

        users = get_users_from_ticket(ticket, active_session)

        subject = (
            f"[ANDON SYSTEM Ticket #{ticket.id_}]"
            f" Приоретизиран {strings.get(ticket.ticket_type)}"
            f" на линија {ticket.line.name}"
        )

        description = ticket.reason or "" + " ".join(
            [machine.name for machine in ticket.machines]
        )

        active_since = (
            ticket.created_at.astimezone(pytz.timezone("Europe/Skopje"))
            if ticket.created_at
            else "ERR"
        )

        body = (
            f"На линија {ticket.line.name} има"
            f" {strings.get(ticket.ticket_type, '').lower()}:"
            f" {description}\n\n\n"
            f"Проблемот е активен од {active_since} ({pytz.timezone('Europe/Skopje')})"
        )
        _send_email_to_users(users, subject, body)


@app.task(name=TRIGGER_NEW_TICKET_TASKNAME)
def trigger_new_ticket(ticket: ReadTicketSchema):
    if not ticket.id_ or not ticket.line.event_config:
        return

    mapping = ticket.line.event_config.get_event_config(ticket_type=ticket.ticket_type)

    if ticket.ticket_type == TicketTypeEnum.PLANNED_ACTION:
        send_planned_action_email(ticket)
        return

    send_email_to_escalation_group.delay(ticket, mapping.initial_escalation)
    # 2. Schedule call to senior technician in 10 minutes
    send_email_to_escalation_group.apply_async(
        (
            ticket,
            mapping.escalation_1,
            [
                StatusEnum.CLOSED,
                StatusEnum.PENDING,
                StatusEnum.FORWARDED,
                StatusEnum.ON_HOLD,
                StatusEnum.WAITING_FOR_APPROVAL,
                StatusEnum.APPROVED,
                StatusEnum.IN_PROGRESS,
                StatusEnum.REJECTED,
                StatusEnum.MATERIAL_NOT_AVAILABLE,
            ],
        ),
        countdown=(mapping.escalation_1.seconds_delay),
    )
    # Schedule escalations 2, 3, 4, 5
    send_email_to_escalation_group.apply_async(
        (ticket, mapping.escalation_2),
        countdown=(mapping.escalation_2.seconds_delay),
    )
    send_email_to_escalation_group.apply_async(
        (ticket, mapping.escalation_3),
        countdown=(mapping.escalation_3.seconds_delay),
    )
    send_email_to_escalation_group.apply_async(
        (ticket, mapping.escalation_4),
        countdown=(mapping.escalation_4.seconds_delay),
    )
    send_email_to_escalation_group.apply_async(
        (ticket, mapping.escalation_5),
        countdown=(mapping.escalation_5.seconds_delay),
    )


@app.task(name=TRIGGER_EXPORT_TICKETS)
def trigger_ticket_export(user_email: str):
    df = TicketRepository().get_pd_dataframe(engine=engine)

    df["Machines"] = df.groupby("id")["Machines"].transform(lambda x: ",".join(x))
    df = df.drop_duplicates()

    df["status"] = df["status"].apply(lambda x: StatusEnum(x).name)
    df["ticket_type"] = df["ticket_type"].apply(lambda x: TicketTypeEnum(x).name)

    df = df.sort_values(by=["id"])

    body = bytes(df.to_csv(), encoding="utf-8")

    to = user_email
    date = str(datetime.now())
    subject = f"[Andon System] CSV Export of Tickets {date}"

    send_email_with_attachment.delay(
        to=to,
        subject=subject,
        raw_body="Во прилог побараниот експорт на тикети",
        attachment=body,
        filename="tickets_export.csv",
    )


def _send_email_to_users(users: List[User], subject, body) -> None:
    for user in users:
        if user.email:
            send_email.delay(
                to=user.email,
                subject=subject,
                raw_body=body,
            )


def _send_email_and_call_users(
    users: List[User],
    subject,
    body,
    ticket: ReadTicketSchema,
) -> None:
    for user in users:
        if user.email:
            send_email.delay(
                to=user.email,
                subject=subject,
                raw_body=body,
            )
    phone_numbers = {user.phone_number for user in users if user.phone_number}
    for phone_number in phone_numbers:
        call_user.delay(
            phone_number=phone_number,
            ticket=ticket,
        )
