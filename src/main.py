import logging
from logging.config import dictConfig

import sentry_sdk
import uvicorn
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from sentry_sdk.integrations.asgi import SentryAsgiMiddleware

from src.api.v1.api import router as api_v1_router
from src.config import Config, LogConfig, get_config

# logging.basicConfig(
#     level=logging.INFO, format="%(asctime)s | %(levelname)s | %(message)s"
# )

config: Config = get_config()
dictConfig(LogConfig().dict())
logger: logging.Logger = logging.getLogger("main_logger")
logger.propagate = False

app = FastAPI(
    title="Aptiv Andon API",
    servers=[],
)

app.logger = logger  # type: ignore

if config.env == "prod":
    sentry_sdk.init(dsn=config.sentry_dsn)
    app.add_middleware(SentryAsgiMiddleware)
    print(SentryAsgiMiddleware)

app.add_middleware(
    CORSMiddleware,
    allow_origins=config.cors_origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
app.include_router(api_v1_router, prefix="/api/v1")


if __name__ == "__main__":
    # pyre-ignore[6]:
    uvicorn.run(app, host=config.host or "127.0.0.1", port=8000)
