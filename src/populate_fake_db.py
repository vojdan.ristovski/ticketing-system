import random

from src.models.factories import ticket_factory

if __name__ == "__main__":
    session = ticket_factory.create_session()()
    machines = ticket_factory.MachineFactory.create_batch(500)
    session.add_all(machines)

    tickets = [
        ticket_factory.TicketFactory.create(
            machines=random.sample(machines, random.randint(1, 100))  # nosec
        )
        for _ in range(10000)
    ]
    session.add_all(tickets)

    session.commit()
