import json

from sqlalchemy import text

from src.db.session import ScopedSession
from src.models.tickets import (
    Component,
    Line,
    Machine,
    MachineType,
    Product,
    Reason,
    Zone,
    products_lines_association,
)
from src.models.users import Role, User, users_roles_association

if __name__ == "__main__":

    with open("seed.json") as file:
        seed_data = json.loads(file.read())
        zone_objects = [Zone(**data) for data in seed_data["zones"]]
        line_objects = [Line(**data) for data in seed_data["lines"]]
        machine_type_objects = [
            MachineType(**data) for data in seed_data["machine_types"]
        ]
        machine_objects = [Machine(**data) for data in seed_data["machines"]]
        product_objects = [Product(**data) for data in seed_data["products"]]
        products_lines_associations = seed_data["products_lines_associations"]
        for data in seed_data["users"]:
            del data["roles"]
        user_objects = [User(**data) for data in seed_data["users"]]
        role_objects = [Role(**data) for data in seed_data["roles"]]
        users_roles_associations = seed_data["users_roles_association"]
        component_objects = [Component(**data) for data in seed_data["components"]]
        reason_objects = [Reason(**data) for data in seed_data["reasons"]]

    with ScopedSession() as session:
        with session.begin():
            session.add_all(zone_objects)
            session.add_all(line_objects)
            session.add_all(machine_type_objects)
            session.add_all(machine_objects)
            session.add_all(product_objects)
            session.flush()
            session.execute(
                products_lines_association.insert(), products_lines_associations
            )
            session.add_all(user_objects)
            session.add_all(role_objects)
            session.flush()
            session.execute(users_roles_association.insert(), users_roles_associations)
            session.add_all(component_objects)
            session.add_all(reason_objects)

            for table in [
                "zones",
                "lines",
                "machinetypes",
                "machines",
                "products",
                "users",
                "roles",
                "components",
                "reasons",
            ]:
                session.execute(
                    text(
                        f"""
SELECT setval(
        pg_get_serial_sequence('{table}', 'id'),
        COALESCE(MAX(id), 1) + 1
    )
FROM {table};
                """
                    )
                )

    print("Seed data loaded")
