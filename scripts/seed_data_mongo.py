import json
from datetime import datetime

import pandas as pd
from pymongo import MongoClient
from pymongo.collection import Collection
from schemas.andon.lines import ReadLineSchema
from schemas.andon.zone import ReadZoneSchema
from sqlalchemy import create_engine
from sqlalchemy.engine import Engine

from src.config import Config, get_config
from src.db.mongo import get_mongo_client
from src.schema.warehouse import BaseStagingSchema

config: Config = get_config()

engine: Engine = create_engine(config.database_url)
lines_df = pd.read_sql_table("lines", engine)
zones_df = pd.read_sql_table("zones", engine)


client: MongoClient = get_mongo_client()
db = client["staging-db"]
zones: Collection = db["zones"]
lines: Collection = db["lines"]
with open("seed.json") as file:
    seed_data = json.loads(file.read())
    zone_objects = [
        BaseStagingSchema(
            method="add_zones",
            document=ReadZoneSchema(
                **data, created_at=datetime.now(), updated_at=datetime.now()
            ).dict(),
        ).dict()
        for data in seed_data["zones"]
    ]
    line_objects = [
        BaseStagingSchema(
            method="add_lines",
            document=ReadLineSchema(
                **data,
                created_at=datetime.now(),
                updated_at=datetime.now(),
                password="Aptiv703",
            ).dict(),
        ).dict()
        for data in seed_data["lines"]
    ]

with client.start_session() as session:
    with session.start_transaction():
        zones.insert_many(zone_objects)
        lines.insert_many(line_objects)
