FROM python:3.10 as requirements-stage

WORKDIR /tmp



RUN pip install --upgrade pip
RUN pip install --no-cache-dir poetry

COPY ./pyproject.toml ./poetry.lock* /tmp/

RUN poetry export -f requirements.txt --output requirements.txt --without-hashes



FROM python:3.10

WORKDIR /code

COPY --from=requirements-stage /tmp/requirements.txt /code/requirements.txt

RUN mkdir ~/.ssh
RUN ssh-keyscan gitlab.com > ~/.ssh/known_hosts

ARG USERNAME
ARG PASSWORD
RUN echo "machine gitlab.com\n" \
         "    login ${USERNAME}\n" \
         "    password ${PASSWORD}" > /root/.netrc
RUN chown root ~/.netrc
RUN chmod 0600 ~/.netrc

RUN pip install --upgrade pip
RUN pip install --no-cache-dir --upgrade -r /code/requirements.txt && \
    pip install --no-cache-dir --upgrade uvicorn


COPY ./src /code/src
COPY ./alembic /code/alembic
COPY ./alembic.ini /code/alembic.ini
COPY ./initial_config.json /code/initial_config.json
COPY ./scripts/seed_data.py /code/seed_data.py
COPY ./scripts/seed_data_mongo.py /code/seed_data_mongo.py
COPY ./scripts/seed.json /code/seed.json

EXPOSE 8000

CMD ["uvicorn", "src.main:app", "--proxy-headers", "--host", "0.0.0.0", "--port", "8000"]
