"""Change closed_at to datetime

Revision ID: b64fa93b2501
Revises: ab343736f113
Create Date: 2022-02-21 09:52:42.592173

"""
import sqlalchemy as sa

from alembic import op

# revision identifiers, used by Alembic.
revision = "b64fa93b2501"
down_revision = "ab343736f113"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column("tickets", "closed_at", type_=sa.DateTime(), nullable=True)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column("tickets", "closed_at", type_=sa.Date(), nullable=True)
    # ### end Alembic commands ###
