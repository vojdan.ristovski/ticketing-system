"""Add priority to status enum

Revision ID: fb85b0573ce4
Revises: b64fa93b2501
Create Date: 2022-02-22 21:59:09.729697

"""
from alembic import op

# revision identifiers, used by Alembic.
revision = "fb85b0573ce4"
down_revision = "b64fa93b2501"
branch_labels = None
depends_on = None


def upgrade():
    op.execute("ALTER TYPE statuses ADD VALUE IF NOT EXISTS 'PRIORITY'")
    # ### end Alembic commands ###


def downgrade():
    pass
    # ### end Alembic commands ###
