"""merge master onto timeseries

Revision ID: 5ed33201c297
Revises: 5a5fd945be71, 32655da01a9b
Create Date: 2022-04-27 09:52:50.098064

"""
import sqlalchemy as sa

from alembic import op

# revision identifiers, used by Alembic.
revision = "5ed33201c297"
down_revision = ("5a5fd945be71", "32655da01a9b")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
