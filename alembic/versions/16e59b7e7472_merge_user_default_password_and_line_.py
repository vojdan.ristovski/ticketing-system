"""Merge user default password and line config

Revision ID: 16e59b7e7472
Revises: 9864c1322027, f48a8e838fdd
Create Date: 2022-06-02 22:18:14.887232

"""
import sqlalchemy as sa

from alembic import op

# revision identifiers, used by Alembic.
revision = "16e59b7e7472"
down_revision = ("9864c1322027", "f48a8e838fdd")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
