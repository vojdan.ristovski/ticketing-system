"""Add lines config

Revision ID: 9864c1322027
Revises: 5ed33201c297
Create Date: 2022-05-03 12:23:12.851931

"""
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

from alembic import op

# revision identifiers, used by Alembic.
revision = "9864c1322027"
down_revision = "5ed33201c297"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table(
        "users_lines_association",
        sa.Column("user_id", sa.Integer(), nullable=False),
        sa.Column("line_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(["line_id"], ["lines.id"], ondelete="CASCADE"),
        sa.ForeignKeyConstraint(["user_id"], ["users.id"], ondelete="CASCADE"),
        sa.PrimaryKeyConstraint("user_id", "line_id"),
    )
    op.add_column("lines", sa.Column("event_config", sa.JSON(), nullable=True))

    statement = """
    UPDATE lines SET event_config = '{
        "MACHINE_PROBLEM": {
            "initial_escalation": { "role_id": 1, "seconds_delay": 0 },
            "escalation_1": { "role_id": 2, "seconds_delay": 600 },
            "escalation_2": { "role_id": 3, "seconds_delay": 3600 },
            "escalation_3": { "role_id": 4, "seconds_delay": 7200 },
            "escalation_4": { "role_id": 5, "seconds_delay": 14400 },
            "escalation_5": { "role_id": 6, "seconds_delay": 28800 },
            "forwarded_escalation": { "role_id": 0, "seconds_delay": 0 },
            "closed_escalation": { "role_id": 0, "seconds_delay": 0 },
            "daily": { "role_id": 7, "hour": 0, "minute": 0 }
        },
        "QUALITY_PROBLEM": {
            "initial_escalation": { "role_id": 8, "seconds_delay": 0 },
            "escalation_1": { "role_id": 9, "seconds_delay": 600 },
            "escalation_2": { "role_id": 10, "seconds_delay": 3600 },
            "escalation_3": { "role_id": 11, "seconds_delay": 7200 },
            "escalation_4": { "role_id": 12, "seconds_delay": 14400 },
            "escalation_5": { "role_id": 13, "seconds_delay": 28800 },
            "forwarded_escalation": { "role_id": 0, "seconds_delay": 0 },
            "closed_escalation": { "role_id": 0, "seconds_delay": 0 },
            "daily": { "role_id": 14, "hour": 0, "minute": 0 }
        },
        "MAINTENANCE_PROBLEM": {
            "initial_escalation": { "role_id": 15, "seconds_delay": 0 },
            "escalation_1": { "role_id": 0, "seconds_delay": 600 },
            "escalation_2": { "role_id": 0, "seconds_delay": 3600 },
            "escalation_3": { "role_id": 0, "seconds_delay": 7200 },
            "escalation_4": { "role_id": 0, "seconds_delay": 14400 },
            "escalation_5": { "role_id": 0, "seconds_delay": 28800 },
            "forwarded_escalation": { "role_id": 0, "seconds_delay": 0 },
            "closed_escalation": { "role_id": 0, "seconds_delay": 0 },
            "daily": { "role_id": 0, "hour": 0, "minute": 0 }
        },
        "MATERIALS_PROBLEM": {
            "initial_escalation": { "role_id": 16, "seconds_delay": 0 },
            "escalation_1": { "role_id": 0, "seconds_delay": 600 },
            "escalation_2": { "role_id": 21, "seconds_delay": 3600 },
            "escalation_3": { "role_id": 0, "seconds_delay": 7200 },
            "escalation_4": { "role_id": 0, "seconds_delay": 14400 },
            "escalation_5": { "role_id": 0, "seconds_delay": 28800 },
            "confirmation_escalation": { "role_id": 22, "seconds_delay": 0 },
            "material_unavailable_escalation": { "role_id": 0, "seconds_delay": 0 },
            "daily": { "role_id": 20, "hour": 0, "minute": 0 }
        },
        "PLANNED_ACTION": {
            "short_action_role_id": 0,
            "long_action_role_id": 0,
            "short_action_length_seconds": 1800
        }
    }' WHERE 1 = 1;"""

    op.execute(statement)

    op.alter_column(
        "lines",
        "event_config",
        existing_type=postgresql.JSON(astext_type=sa.Text()),
        nullable=False,
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column("lines", "event_config")
    op.drop_table("users_lines_association")
    # ### end Alembic commands ###
